<?php


Route::group(['prefix' => 'builders', 'middleware' => ['web', 'auth'], 'namespace' => 'UserAccount\Builder'], function() {

    Route::get('/profile/', 'ProfileBuilderController@index')->name('builders.profile');
    Route::get('/profile/{any}', 'ProfileBuilderController@index');

});
