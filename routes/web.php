<?php
/**
 * Authentication routes
 *
 * All the routes here handles registration, login, password reset etc
 */
Route::get('/join', 'TalentJoinController@show')->name('talent.join-show');
Route::post('/join', 'TalentJoinController@store')->middleware('phone-number-setter')->name('talent.join');
Route::get('/join/plans', 'TalentJoinController@selectPlan')->name('join.plan-selection')->middleware('auth');
Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@loginProcess')->name('login-process');
Route::post('/login', 'LoginController@loginProcess')->name('login-process');

/**
 * Password rest
 */
Route::get('/forgot-password', 'PasswordResetController@forgotPassword')->name('auth.forgot-password');
Route::post('/forgot-password', 'PasswordResetController@forgotPasswordPost')->name('auth.forgot-password-post');
Route::get('reset-password/{token}', 'PasswordResetController@resetPassword')->name('auth.reset-password');
Route::post('reset-password/{any}', 'PasswordResetController@setNewPassword')->name('auth.reset-password-process');
Route::get('/logout', 'UserAccount\LogoutController@logout')->name('account.logout');


/**
 * User account routes
 */
Route::group(['prefix' => 'account', 'middleware' => ['auth'], 'namespace' => 'UserAccount'], function() {
    Route::get('/', 'AccountController@home')->name('account.home');
    Route::get('/home', 'AccountDashboardController@index')->middleware('account-verify')->name('account.dashboard');
    Route::get('/profile', 'ProfileController@profile')->name('account.profile');
    Route::get('/profile/media', 'ProfileMediaController@index')->name('account.profile.media');
    Route::post('/profile/media', 'ProfileMediaController@store')->name('account.profile.media.store');
    Route::delete('/profile/media/{id}', 'ProfileMediaController@delete');
    Route::get('/profile/avatar', 'ProfileController@addAvatar')->name('account.profile.avatar');
    Route::get('/profile/settings', 'AccountSettingsController@profileSettings')->name('account.profile.settings');
    Route::get('/profile/settings/subscriptions', 'AccountSettingsController@subscriptions')->name('account.profile.settings.subscriptions');
    Route::post('/profile/password', 'AccountPasswordController@setNewPassword')->name('account.profile.password');

    Route::post('/profile/set-location', 'UserLocationController@store');

    Route::get('/packages/{id}/change', 'BillingPackage@changePackage')->name('account.change.package');
    Route::get('/packages/{id}/pricing', 'BillingPackage@getPricing');
    Route::get('/packages/{id}/pricing', 'BillingPackage@getPricing');
    Route::post('/packages/order-subscription', 'BillingPackage@orderSubscription');

    Route::get('/billing/{txf}/verify', 'BillingTransactionVerify@check');

    Route::get('/applications', 'ApplicationManagementController@index')->name('account.applications');


    Route::group(['prefix' => '/educations'], function() {

        Route::get('/', 'EducationManagementController@index');
        Route::post('/', 'EducationManagementController@store');
        Route::post('/{id}', 'EducationManagementController@update');
        Route::delete('/{id}', 'EducationManagementController@delete');

    });

    Route::group(['prefix' => '/productions'], function() {

        Route::get('/', 'ProductionDataController@index');
        Route::post('/invite', 'ProductionDataController@store');

    });


    /**
     * Account endpoints for ajax result
     */
    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {

        Route::get('/profile', 'ProfileManagementController@showProfile')->name('account.api.show.profile');
        Route::post('/profile', 'ProfileManagementController@updateProfile')->name('account.api.profile');


        Route::get('/profile/medias', 'ProfileManagementController@getMedias')->name('account.api.show.medias');

        Route::get('/profile/appearance', 'AppearanceController@index')->name('account.api.appearance');
        Route::post('/profile/appearance', 'AppearanceController@store')->name('account.api.appearance.store');


        // account/api/profile/settings
        Route::group(['prefix' => 'profile/settings'], function() {

            Route::get('/', 'ProfileSettingsController@getProfileInfo');
        });

    });
});


Route::group(['prefix' => 'creator', 'namespace' => 'Creator'], function() {

    Route::get('/join', 'CreatorJoinController@index')->name('creator.join');
    Route::post('/join', 'CreatorJoinController@store')->name('creator.join.store')->middleware('phone-number-setter');

    Route::get('/company-profile-update', 'CreatorCompanyProfileUpdate@index')
                ->name('creator.company.profile-update');
    Route::post('/company-profile-update', 'CreatorCompanyProfileUpdate@store');

    Route::group(['prefix' => 'projects', 'namespace' => 'Project', 'middleware' => ['auth', 'account-verify']], function() {
        Route::get('/', 'CastingProjectController@index')->name('casting.projects.index');
        Route::get('/post', 'ProjectController@index')->name('creator.projects.new');
        Route::post('/{id}', 'ProjectController@store')->name('creator.projects.post');
        Route::get('/{id}/applications', 'ProjectApplicationsController@index')->name('creator.projects.applications');
        Route::post('/{id}/basins/{applicationId}', 'ProjectBasinController@store');
        Route::get('/{id}/applications-feeds', 'ProjectApplicationsController@feeds');
        Route::post('/{id}/roles', 'ProjectController@storeRole')->name('creator.projects.store.role');
        Route::get('/{id}/roles', 'ProjectController@getRoles')->name('creator.projects.get.roles');
        Route::delete('/{id}/roles/{roleId}', 'ProjectController@deleteRole')->name('creator.projects.delete.role');
        Route::get('/{id}/locations', 'ProjectController@getLocations')->name('creator.projects.get.locations');
        Route::get('/{id}/{status}', 'ProjectController@show')->name('creator.projects.show');
        Route::post('/{id}/attach-country', 'ProjectController@attachCountry');
        Route::post('/{id}/attach-city', 'ProjectController@attachCity');
        Route::post('/{id}/locations', 'ProjectController@addLocation')->name('creator.projects.add.location');
        Route::post('/{id}/locations-with-dates', 'ProjectController@addLocationWithDates')->name('creator.projects.add.location.with.dates');
        Route::get('/{id}/locations-with-dates/{locationId}', 'ProjectController@getLocationDates')->name('creator.projects.get.location.dates');
        Route::delete('/{id}/locations/{location}', 'ProjectController@removeLocation')->name('creator.projects.remove.location');
        Route::delete('/{id}/locations-with-dates/{locationId}/{datePId}', 'ProjectController@removeLocationDate');

        /**
         * Applying for application
         */

        });


    Route::group(['prefix' => 'notes', 'namespace' => 'Note', 'middleware' => ['auth']], function() {
        Route::get('/{userId}', 'NewNoteController@show');
        Route::post('/{userId}', 'NewNoteController@store');
        Route::delete('/{userId}', 'NewNoteController@delete');
    });
});

Route::group(['prefix' => 'casting-calls', 'namespace' => 'Core'], function() {

    Route::get('/', 'CastingCallsController@index')->name('casting-calls');
    Route::get('/feeds', 'CastingCallsController@getAll');
    Route::get('/feeds/{projectId}', 'CastingCallsController@show');
    Route::get('/{id}', 'SingleCastingCallController@index')->name('single.casting-call');
    Route::post('/{projectId}/draft-apply', 'ApplyToProjectRoleController@draftStore');
    Route::post('/{projectId}/apply', 'ApplyToProjectRoleController@store');
    Route::post('/{projectId}/apply/{applicationId}/media/{mediaTypeId}', 'ApplyToProjectRoleAddMediaController@store');

});

Route::group(['prefix' => 'talents', 'namespace' => 'Core'], function() {

    Route::get('/', 'TalentController@index')->name('talents');
    Route::get('/feeds', 'TalentFeedController@index')->name('talent.feed');
    Route::get('/feeds/{userId}', 'TalentFeedController@show')->name('talent.feed.show');

});

Route::get('/geo/search', 'Core\HomePageController@geo');

Route::get('/', 'Core\HomePageController@home')->name('home');
Route::get('/performers/how-it-works', 'Core\HomePageController@performHowItWorks')->name('performer.how-it-works');
Route::get('/creators/how-it-works', 'Core\CreatorPublicController@creatorHowItWorks')->name('creator.how-it-works');

Route::group(['prefix' => 'utilities', 'namespace' => 'Utilities'], function() {

    Route::get('/body-sizes', 'BodySizeController@index');
    Route::get('/eye-colors', 'EyeColorController@index');
    Route::get('/hair-colors', 'HairColorController@index');
    Route::get('/appearance', 'AppearanceController@index')->middleware('auth');

    Route::get('/skills', 'ProfileSkillController@index');
    Route::post('/skills', 'ProfileSkillController@store');


});

Route::group(['prefix' => 'graph', 'namespace' => 'Graph', 'as' => 'graph.'], function() {

    Route::get('/skills', 'SkillsController@index')->name('skills');

});

Route::get('/partnership', 'Core\PartnershipController@index')->name('partnership');
Route::get('/services/terms', 'Core\ServciesController@terms')->name('terms-of-services');
Route::get('/about', 'Core\AboutController@index')->name('about');

Route::get('/verify', 'Core\VerificationController@index')->middleware('auth')->name('verify');
Route::get('/verify/activation', 'Core\VerificationController@activate')->middleware('auth')->name('verify.activate');

Route::get('/u/{username}', 'Core\PublicProfileController@index')->name('public.profile');
Route::get('/u/{username}/media', 'Core\PublicProfileController@media')->name('public.profile.media');
Route::get('/u/{username}/appearance', 'Core\PublicProfileController@appearance')->name('public.profile.appearance');

