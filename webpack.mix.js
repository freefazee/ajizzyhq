const mix = require('laravel-mix');

mix.js('resources/js/account/profile-form.js', 'public/assets/ajizzy/js/account')
    .js('resources/js/account/settings.js', 'public/assets/ajizzy/js/account')
    .js('resources/js/account/photo-avatars.js', 'public/assets/ajizzy/js/account')
    .js('resources/js/project/post.js', 'public/assets/ajizzy/js/project')
    .js('resources/js/billing/order-subscription.js', 'public/assets/ajizzy/js/account')
    .js('resources/js/project/single/casting-call-single.js', 'public/assets/ajizzy/js/project')
    .js('resources/js/project/calls/casting-calls.js', 'public/assets/ajizzy/js/project')
    .js('resources/js/account/creator-company-profile-update.js', 'public/assets/ajizzy/js/account')
    .js('resources/js/talents/index.js', 'public/assets/ajizzy/js/talents')
    .js('resources/js/talents/public.js', 'public/assets/ajizzy/js/talents')
    .js('resources/js/talents/applications.js', 'public/assets/ajizzy/js/talents')
    .js('resources/js/profile-builder/builder.js', 'public/assets/ajizzy/js/account');
