<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['pivot', 'created_at', 'updated_at', 'valid_mime_types'];

    public function roles()
    {
        return $this->belongsToMany(ProjectCastingRole::class);
    }
}
