<?php

namespace App;

use App\Events\ApplicationApproved;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Application extends Model
{
    public const STATUS_NEW = 'new';
    public const RED_BASIN = 'red';
    public const GREEN_BASIN = 'green';
    public const YELLOW_BASIN = 'yellow';

    public const APPLICATION_REJECTED = 'Rejected';
    public const APPLICATION_ACCEPTED = 'Accepted';
    public const APPLICATION_MOVED_TO_REVIEW = 'In-Review';

    const INAVLID_DATE = 'INAVLID_DATE';


    use Uuids;

    public $incrementing = false;

    protected $table  = 'applications';

    protected $guarded = ['id'];


    /**
     * The person that applied to this application
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function role()
    {
        return $this->belongsTo(ProjectCastingRole::class, 'project_casting_role_id');
    }

    public function medias()
    {
        return $this->hasMany(ApplicationMedia::class);
    }

    public function scopeDraft($query)
    {
        $query->where('draft', 1);
    }

    public function scopeValid($query)
    {
        $query->where('status', '!=' , 'draft')
                ->where('draft', 0);
    }

    public function scopeForUser($query, User $user)
    {
        $query->where($this->table.'.user_id', $user->id);
    }


    public function scopeNew($query)
    {
        $query->where($this->table.'.status', static::STATUS_NEW);
    }

    public static function createApplication(User $user, Project $project, ProjectCastingRole $role)
    {
        $check = $project->applications()
            ->where('applications.user_id', $user->id)
            ->where('project_casting_role_id', $role->id)
            ->first();

        if($check)
        {
            $check->status = static::STATUS_NEW;
            $check->draft = 0;
            $check->save();
            return $check;
        }

        return $project->applications()
            ->create([
                'user_id' => $user->id,
                'project_casting_role_id' => $role->id,
                'status' => static::STATUS_NEW,
                'draft' => 0
            ]);

    }

    public static function createDraft(User $user, Project $project, ProjectCastingRole $role)
    {
        $check = $project->applications()
                    ->where('applications.user_id', $user->id)
                    ->where('project_casting_role_id', $role->id)
                    ->first();

        if($check) return $check;

        return $project->applications()
            ->create([
                'user_id' => $user->id,
                'project_casting_role_id' => $role->id,
                'status' => 'draft',
                'draft' => 1
            ]);

    }

    public static function getUserMedias(Application $application)
    {
        $out = [];
        $data = $application->medias()->select('id','media_type_id', 'created_at', 'filename')->get();
        foreach($data as $item) {
            $out[$item->media_type_id] = ['id' => $item->id, 'media_type_id' => $item->media_type_id, 'date' => $item->created_at->toFormattedDateString()];
        }
        return $out;
    }

    public static function getUserAppliedRoleIds(Project $project, $user)
    {
        if(!$user) return collect();
        return $project->applications()
            ->forUser($user)
            ->new()->get()
            ->pluck('project_casting_role_id');
    }

    public function createSingleMedia($mediaTypeId, array $data)
    {
        if(!$mediaTypeId) return null;
        $media = $this->medias()->where('media_type_id', $mediaTypeId )->first();

        $data['media_type_id'] = $mediaTypeId;

        if($media)
            return $media->update($data);

        return $this->medias()->create($data);
    }

    public function setRedBasin()
    {
        $this->update(['basin' => static::RED_BASIN]);

        return $this;
    }

    public function setGreenBasin()
    {
        $this->update(['basin' => static::GREEN_BASIN]);

        event(new ApplicationApproved($this));

        return $this;
    }

    public function setYellowBasin()
    {
        $this->update(['basin' => static::YELLOW_BASIN]);

        return $this;
    }


    /**
     * @param $basin
     *
     * @return $this|\App\Application
     */
    public function setBasin($basin)
    {
        $basin = strtolower($basin);

        if($basin === static::RED_BASIN) return $this->setRedBasin();
        if($basin === static::GREEN_BASIN) return $this->setGreenBasin();
        if($basin === static::YELLOW_BASIN) return $this->setYellowBasin();

        return $this;

    }

    /**
     * @return string
     */
    public function getBasinMessage()
    {
        if($this->basin === static::RED_BASIN) return static::APPLICATION_REJECTED;
        if($this->basin === static::GREEN_BASIN)  return static::APPLICATION_ACCEPTED;
        if($this->basin === static::YELLOW_BASIN)  return static::APPLICATION_MOVED_TO_REVIEW;

        return 'New';
    }

    /**
     * @return string
     */
    public function getBasinClass()
    {
        if($this->basin === static::RED_BASIN) return 'danger';
        if($this->basin === static::GREEN_BASIN)  return 'success';
        if($this->basin === static::YELLOW_BASIN)  return 'warning';

        return 'dark';
    }

    public function getFormattedCreatedAttribute($val)
    {
        return $this->created_at ? Carbon::parse($this->created_at)->toFormattedDateString() : static::INAVLID_DATE;
    }


    public static function removeUserApplication(Application $application) {

        if($application->medias)
        {
            foreach ($application->medias as $media)
            {
                Storage::disk(config('ajizzy.application-media-disk'))->delete($media->filename);
                $media->delete();
            }
        }

        $application->delete();

        return true;
    }

    public static function  statusList()
    {
        return [
            static::RED_BASIN => static::APPLICATION_REJECTED,
            static::GREEN_BASIN => static::APPLICATION_ACCEPTED,
            static::YELLOW_BASIN => static::APPLICATION_MOVED_TO_REVIEW
        ];
    }
}
