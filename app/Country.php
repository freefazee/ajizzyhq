<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Country extends Model
{
    protected $guarded = ['id'];

    protected $table = 'countries';

    public static function getAll()
    {
        return static::getCountries();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * A country has many states/locations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function getFullNameAttribute($value)
    {
        if($value) return $value;

        return $this->name;
    }


    public function addProject($projects)
    {
        return $this->projects()->attach($projects);
    }

    /**
     * This is a many to many to project
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }


    public function localProjects()
    {
        return $this->hasMany(Project::class);
    }


    public function getShortName()
    {
        return $this->cca2;
    }

    public static function getCountries()
    {
        return static::select('id', 'name', 'cca2 as country')->whereNull('parent_id')->get();
    }

    public function getCurrency()
    {
        $currencies = json_decode(trim($this->currencies), true);

        if($currencies)
        {

            return $currencies ? array_values($currencies)[0]['symbol'] : env('DEFAULT_CURRENCY', '$');
        }

        return env('DEFAULT_CURRENCY', '$');
    }


    public function getCurrencyCode()
    {
        $currencies = json_decode(trim($this->currencies), true);

        return array_keys($currencies)[0];
    }
}
