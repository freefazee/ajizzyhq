<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Events\TalentJoined;
use App\Events\UserPasswordChanged;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webpatser\Uuid\Uuid;

class User extends Authenticatable
{
    use Notifiable, Uuids, CanFollow, CanBeFollowed;

    public $incrementing = false;

    public const ACCOUNT_TYPE_CREATOR = 'creator';
    public const ACCOUNT_TYPE_TALENT = 'talent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'country_id', 'email', 'password',
        'phone', 'terms_and_condition', 'age_verification', 'plan',
        'postal_code', 'account_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * This method is used to auto populate the profile form at the frontend
     *
     * @return mixed
     */
    public function getProfileData()
    {
        return $this->profile ?: [
            'stage_name' => e($this->first_name) . ' ' . e($this->last_name),
            'title' => '',
            'profile_url' => str_slug($this->first_name . ' ' . $this->last_name),
            'gender' => 'unspecified',
            'is_public' => 1,
            'dob' => '',
            'yob' => '',
            'mob' => '',
            'about_company' => $this->about_company,
        ];
    }

    public function scopePublic($query)
    {
        $query->whereNotNull('is_public');
    }

    public function scopeInevitable($query)
    {

    }

    public function getAvatarUrl()
    {
        $profile = $this->profile;
        if($profile && count($profile->medias))
        {
            $avatar = $profile->medias[0]->url;
        }
        else
        {
            $avatar = asset('assets/images/icon-user--blank.png');
        }

        return $avatar;
    }

    public  static function createTalentAccount(array $data)
    {

        $data['account_type'] = static::ACCOUNT_TYPE_TALENT;
        $user = static::create($data);

        return $user;
    }

    /**
     * Return user basic profile details.
     *
     * @return array
     */
    public function getBasicProfile()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'fullname' => $this->getFullname(),
            'profile' => $this->profile->occupation,
            'title' => $this->profile->title,
            'company' => $this->profile->company,
            'phone' => $this->phone,
            'dob' => $this->profile->dob,
            'yob' => $this->profile->yob,
            'mob' => $this->profile->mob
        ];
    }

    public function getFullname()
    {
        return e($this->first_name) . ' ' . e($this->last_name);
    }

    /**
     * Set new password for an account
     *
     * @param $newPassword
     *
     * @return bool
     */
    public function setNewPassword($newPassword)
    {
        $result = $this->update(['password' => $newPassword]);
        event(new UserPasswordChanged($this, $newPassword));
        return $result;
    }

    /**
     * Create user profile
     *
     * @param array $data
     *
     * @return int
     */
    public function createProfile(array $data = [])
    {
        if($this->profile) return $this->profile()->update($data);
        $this->profile()->create($data);
    }

    public  function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function pricePlan()
    {
        return $this->belongsTo(Price::class, 'plan');
    }

    public function updatePlan(Price $price)
    {
        $this->plan = $price->id;

        $this->save();
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function cards()
    {
        return $this->hasMany(Card::class, 'user_id');
    }

    public function hasSubscription()
    {
        if(!$this->profile) return false;
        return $this->profile->subscriptionActive();
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'user_id');
    }

    public function getMediaPath()
    {
        return 'profiles/' . $this->id;
    }

    public function getNameAttribute($value)
    {
        return $this->first_name;
    }

    public function isTalent()
    {
        return $this->account_type === static::ACCOUNT_TYPE_TALENT;
    }

    public function showProfileInvisibilityWarning()
    {
        if($this->isTalent())
        {
            if(!$this->profile->is_public || !$this->profile->medias->count()) return true;
            return false;
        }
    }

    /**
     * This is notes that belongs to the user.
     * These notes are created by other users for this user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(Note::class, 'user_id');
    }

    /**
     * These are the notes created by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdNotes()
    {
        return $this->hasMany(Note::class, 'creator_id');
    }


    public function addNoteFrom(User $creator, string $note)
    {
        return $this->notes()->updateOrCreate(['creator_id' => $creator->id], ['body' => $note]);
    }

    public static function getLatestNotes(User $user, $limit=3)
    {
        return $user->createdNotes()
                    ->with('owner.profile:id,user_id,stage_name', 'owner.profile.medias')
                    ->latest()
                    ->take($limit)
                    ->get();
    }

    public function appearance()
    {
        return $this->hasOne(Appearance::class, 'user_id');
    }

    public function ethnicities()
    {
        return $this->belongsToMany(Ethnicity::class);
    }

    public function emailVerified()
    {
        return $this->email_verified_at;
    }

    public function educations()
    {
        return $this->hasMany(Education::class);
    }

    /**
     * Projects I have been invited to
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitations()
    {
        return $this->hasMany(ProductionInvite::class, 'invitee_id');
    }

    public function sentInvites()
    {
        return $this->hasMany(ProductionInvite::class, 'invited_by');
    }

    public function hasBeenInvited(Project $project)
    {
        return $this->invitations()->where('project_id', $project->id)->exists();
    }

    public function invite(User $user, Project $project, $message='')
    {
            return $this->sentInvites()->create(['invitee_id' => $user->id, 'project_id' => $project->id, 'message' => $message]);
    }

    public function generateEmailVerification()
    {
        EmailVerification::where('email', $this->email)->delete();
        $this->verification = EmailVerification::create([
                        'email' => $this->email,
                        'hash' => base64_encode(Uuid::generate() . str_random(32)),
                        'expire_at' => now()->addDays(3),
                        ]);

        return $this;
    }

    public function verifyEmail($hash)
    {
        $hash = EmailVerification::where('hash', $hash)->where('expire_at', '>=', now())->first();
        if($hash)
        {
            $this->email_verified_at = now();
            $this->save();

            $hash->expire_at = now();
            $hash->save();

            return  true;
        }

        return false;
    }
}
