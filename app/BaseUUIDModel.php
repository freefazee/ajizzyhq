<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseUUIDModel extends Model
{
    use Uuids;

    public $incrementing = false;

}
