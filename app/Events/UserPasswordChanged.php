<?php

namespace App\Events;

use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;


class UserPasswordChanged
{
    use Dispatchable, SerializesModels;

    public $user;
    public $newPassword;

    /**
     * UserPasswordChanged constructor.
     *
     * @param \App\Events\User $user
     */
    public function __construct(User $user, $newPassword)
    {
       $this->user  = $user;
       $this->newPassword = $newPassword;
    }

}
