<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GenerateApplicationDownloadChunk
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chunk;

    public $project;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($chunk, $project)
    {
        $this->chunk = $chunk;

        $this->project = $project;
    }


}
