<?php

namespace App\Events;

use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;


class TalentJoined
{
    use Dispatchable, SerializesModels;

    public $user;

    /**
     * TalentJoined constructor.
     *
     * @param \App\Events\User $user
     */
    public function __construct(User $user)
    {
       $this->user  = $user;
    }

}
