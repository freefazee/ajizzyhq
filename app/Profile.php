<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;

class Profile extends Model
{
    use HasTags;

    protected $guarded = ['id', 'user_id'];

    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = ['created_at', 'updated_at', 'subscription_expires'];

    protected $appends = ['age_range', 'place', 'url'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStageNameAttribute($value)
    {
        $name = trim($value) ? $value : e($this->user->first_name) . ' ' . e($this->user->last_name);

        return trim($name) ? : 'Unknown';
    }

    public function getUrlAttribute($value)
    {
        return route('public.profile', $this->profile_url);
    }


    public function getGenderAttribute($value)
    {
        if(!$value) return 'unspecified';

        return $value;
    }

    public function getProfileUrlAttribute($value)
    {
        return $value ? $value : str_slug($this->user->first_name . ' ' . $this->user->last_name);
    }



    public function subscriptionActive()
    {

        if(!$this->subscription_expires) return false;

        return $this->subscription_expires->isFuture();
    }

    public function activateSubscriptionForMonths($months=1)
    {
        $fromDate = $this->subscription_expires;
        if(!$fromDate)
            $fromDate = now();
        $this->subscription_expires = $fromDate->addMonths($months)->addHour(1);
        $this->save();
    }

    public function cancel()
    {
        $this->subscription_canceled = now();
        $this->save();
    }

    public function isCanceled()
    {
        return $this->subscription_canceled !== null;
    }

    public function notCanceled()
    {
        return !$this->isCanceled();
    }

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function addMedia($name, $url, $type, $mime=null)
    {
        return $this->medias()->create([
                'name' => $name,
                'type' => $type,
                'url' => $url,
                'mime' => $mime
        ]);
    }

    public function scopePublic($query)
    {
        $query->where('is_public', true);
    }

    public function getAgeRangeAttribute($key)
    {

        $yob = intval($this->yob); // year of birth

        if(!$yob) return 'Unknown';
        $age = date('Y') - $yob;

        return $age <= 18 ? '17-' . ($age  + 4) : ($age - 2) . '-' . ( $age + 4);
    }

    public function getPlaceAttribute()
    {

        $location = $this->location ? $this->location->name : $this->user->country->name;

        return  $location;

    }


    public function location()
    {
        return $this->belongsTo(Country::class, 'location_id');
    }


    public function getCompanyDisplayName()
    {
        return $this->company ? $this->company : $this->user->first_name;
    }

    public function built()
    {
        return $this->profile_url && $this->medias()->count() > 0;
    }

}
