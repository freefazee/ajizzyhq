<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    use Uuids;

    public $incrementing = false;

    protected $guarded = ['id'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }


    public function calculateBasedOnRange(PriceRange $priceRange)
    {
        $total = ($this->price / 100) * $priceRange->left;

        return $total;
    }

    public function calculateDiscount($amount, PriceRange $priceRange)
    {
       return ($amount / 100) * $priceRange->discount;

    }

    public function calculateTotal(PriceRange $priceRange)
    {
        $base = $this->calculateBasedOnRange($priceRange);
        $discount = $this->calculateDiscount($base, $priceRange);

        return $base - $discount;
    }

    public function getFormattedPriceAttribute($value)
    {
        return number_format($this->price / 100, 2);
    }

}
