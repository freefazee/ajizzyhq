<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $fillable = ['name'];

    public static function getAll()
    {
        return static::get();
    }
}
