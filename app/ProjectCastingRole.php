<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCastingRole extends Model
{
    use Uuids;

    protected $hidden = ['created_at', 'updated_at'];

    protected $guarded = ['id', 'user_id', 'project_id'];

    public $incrementing = false;

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function ethnicities()
    {
        return $this->belongsToMany(Ethnicity::class);
    }

    public function roleType()
    {
        return $this->belongsTo(RoleType::class );
    }

    public function mediaTypes()
    {
        return $this->belongsToMany(MediaType::class );
    }

    public function addEthnicities($records)
    {
        return $this->ethnicities()->sync($records);
    }

    public function addMediaTypes($records)
    {
        return $this->mediaTypes()->sync($records);
    }
}
