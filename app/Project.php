<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Project extends Model
{
    public const DEFAULT_PROJECT_TITLE = 'Untitled Project';
    public const DEFAULT_PROJECT_STATUS = 'drafted';
    public const STATUS_PUBLISHED = 'published';
    public const STATUS_DRAFTED = 'drafted';
    public const DEFAULT_PROJECT_LOCATION_TYPE = 'local'; // this is going to be removed
    public const PROJECT_LOCATION_LOCAL = 'local';
    public const PROJECT_LOCATION_WORLDWIDE = 'worldwide';
    public const PROJECT_LOCATION_NATIONWIDE = 'nationwide';

    protected $appends = ['formatted_expiry_date', 'formatted_created_at', 'loc'];

    private $downloadPath = '';


    use Uuids;

    public $incrementing = false;

    protected $guarded = ['id', 'user_id'];

    protected $hidden = ['user_id', 'created_at', 'updated_at', 'locations'];

    public function scopePublished($query)
    {
        $query->where('status', static::STATUS_PUBLISHED);
    }

    public function scopeCategory($query, $categoryId)
    {
        $query->where('category_id', $categoryId);
    }

    /**
     * Can create a project as draft.
     *
     * @param $user
     *
     * @return bool
     */
    public static function createDrafted($user, $location=null)
    {
        if(!$user) return false;
        $project = $user->projects()
                ->create(['title' => static::DEFAULT_PROJECT_TITLE,
                        'status' => static::DEFAULT_PROJECT_STATUS,
                        'location' => static::DEFAULT_PROJECT_LOCATION_TYPE]);


        $defaultLocation = session('location');
        if(!$location) $location = $defaultLocation;


        if($location && isset($location->id)) {

           // $project->addLocation([$location->id => ['location_type' => 'nationwide']]);
            $project->country_id = $location->id;
            $project->save();

            return $project;
        }

        return false;
    }

    /**
     * Project has an owner $this->owner points to a User object
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * A project belongs to a category $this->category points to Category model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }


    /**
     * Project has many roles
     * $this->roles points to a collection of ProjectCastingRole
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->hasMany(ProjectCastingRole::class, 'project_id');
    }

    /**
     * An accessor that allows you to get expires date.
     * It set default value to now() in Y-d-m format
     * @param $value
     *
     * @return false|string
     */
    public function getExpiresAtAttribute($value)
    {
        return $value ? date('Y-m-d', strtotime($value)) : date('Y-m-d');
    }


    /**
     * An accessor that allows you to get created date.
     *
     * @return false|string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->diffForHumans() : 'Nil';
    }



    public function getLocationAttribute($value) {

        if($value === Project::PROJECT_LOCATION_WORLDWIDE) return Project::PROJECT_LOCATION_WORLDWIDE;

        return $value;
    }

    /**
     * Add role to a project. A role is called casting.
     * The method updates the role if it finds an "id" inside $attributes
     *
     * Used for adding and update
     *
     * @param array $attributes
     *
     * @return ProjectCastingRole
     */
    public function addRole(array $attributes)
    {
        if(isset($attributes['id']) && !empty($attributes['id']))
        {
            $id = $attributes['id'];
            unset($attributes['id']);
            $role = $this->roles()->find($id);
            if($role)
            {
                $role->update($attributes);
                return $role;
            }
        }
        return $this->roles()->create($attributes);
    }

    /**
     * Remove/detach a role from project
     * The methods also remove other dependencies like media types and ethnicities
     *
     * @param $role
     *
     * @return bool
     * @throws \Exception
     */
    public function removeRole($role)
    {
        $roleId = $role instanceof ProjectCastingRole ? $role->id : $role;

        $roleToDelete = $this->roles()->find($roleId);
        if($roleToDelete)
        {
            $roleToDelete->mediaTypes()->detach();
            $roleToDelete->ethnicities()->detach();
            $roleToDelete->delete();
        }
        return true;
    }

    /**
     * Return the status of a featured project
     * @return bool
     */
    public function isFeatured()
    {
        return $this->featured ? true : false;
    }

    /**
     * Can mark a project as featured
     * @return bool
     */
    public function markAsFeatured()
    {
        $this->featured = now();
        $this->save();
        return true;
    }

    /**
     * Can mark a project as un-featured
     * @return bool
     */
    public function markAsUnFeatured()
    {
        $this->featured = null;
        $this->save();
        return true;
    }

    /**
     * @param $locations [1,2] arrays of location. This is from App\Country model
     * @return
     */
    public function addLocation($locations)
    {
       return $this->locations()->syncWithoutDetaching($locations);
    }

    /**
     * This method removes location from project
     *  We also need to delete any dates attached to the location before remove the relationship here
     *
     * @param $locations
     * @return bool
     */
    public function removeLocation($locations)
    {
        return $this->locations()->detach($locations);
    }

    /**
     * A project has many locations and locations can have many project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function locations()
    {
        return $this->belongsToMany('App\Country')
            ->using(ProjectLocation::class)
            ->as('location')
            ->withPivot([
                'id',
                'created_at',
                'updated_at',
                'venue',
                'street',
                'city',
                'province',
                'location_type'
            ]);

    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getLocations($locationType=null)
    {
        if(!$locationType) $locationType = $this->location;

        $output = [];
        $data = $this->locations()
                    ->select('project_id', 'name', 'countries.id', 'cca2', 'official', 'parent_id')
                    ->where('location_type', $locationType)->get();

        foreach($data as $item)
        {

            $output[] = [
                        'name' =>  $item->parent_id ? $item->official : $item->name,
                        'id' => $item->id,
                        'short_name' => $item->cca2,
                        'location' => Arr::except($item->location, ['created_at', 'updated_at'])->toArray()
                        ];
        }

        return $output;

    }

    /**
     * Remove all locations
     */
    public function removeAllLocations()
    {
        return $this->locations()->detach();
    }

    public function getUrl()
    {
        return '/casting-calls/' . $this->id;
    }

    public function attachCountry($countryId)
    {
        $this->country_id = $countryId;
        $this->save();

        $country = $this->country()
                ->select('id', 'name', 'cca2 as code')->first();

        $this->clearOldLocationData('');

        return $country;
    }

    public function notPublished()
    {
        return $this->status !== static::STATUS_PUBLISHED;
    }

    public function scopeValid($query)
    {
        //->where('expires_at', '>=', now())
        $query->where('status', static::STATUS_PUBLISHED);
    }

    public function getFormattedExpiryDateAttribute()
    {
        return Carbon::parse($this->expires_at)->toFormattedDateString();
    }

    public function getCompanyAttribute($value)
    {
        return $value ?? $this->owner->profile->company;
    }

    public function getAboutCompanyAttribute($value)
    {
        return $value ?? $this->owner->profile->about_company;
    }


    public function getPublicLinkAttribute($value)
    {
        return route('single.casting-call', $this->id);
    }

    /** location to display at the frontend */
    public function getLocAttribute($value)
    {
            if($this->location === static::PROJECT_LOCATION_WORLDWIDE)
            {
                return 'Worldwide';
            }
            else
            {

                $locations = $this->locations;
                if(!$locations->count())
                {
                    return $this->country ? $this->name : 'Unknown';
                }

                $firstLocation = $locations[0];
                $out = $firstLocation->parent_id ? $firstLocation->official : $firstLocation->name;

                if($locations->count() > 1) $out .= ' and ' . count($locations) . ' other locations';

                return $out ;

            }


    }

    public function applications()
    {
        return $this->hasManyThrough(Application::class, ProjectCastingRole::class);
    }

    public function accepting()
    {
        return $this->accepting_submission;
    }

    public function scopeAppliable($query)
    {
        $query->where('accepting_submission', 1);
    }

    public function initDownloadPath()
    {
        $this->downloadPath = storage_path('downloads/applications-' . $this->id . '.csv');

        $fp = fopen($this->downloadPath, 'w');

        if($fp) {
            fputcsv($fp, ['STAGE NAME', 'EMAIL', 'PHONE', 'AGE RANGE', 'ROLE','APPLIED ON', 'URL']);
            return $fp;
        }

        return false;
    }

    public function getDownloadPath()
    {
        return $this->downloadPath;
    }

    /**
     * When a project location type is switched, we need to delete the remove locations
     * that where attached to the location type and delete any date entry created for it.
     *
     * @param $newLocationType string the new location type
     *
     * @return bool
     */

    public function clearOldLocationData($newLocationType)
    {
        $oldEntries = \DB::table('country_project')
            ->where('project_id', $this->id)
            ->where('location_type', '!=', $newLocationType)
            ->get();

        $ids = [];
        foreach($oldEntries as $entry)
        {
            $ids[] = $entry->id;
        }

        // delete any date entries

        if($ids)
        {
            \DB::table('project_dates')
                ->whereIn('country_project_id', $ids)
                ->delete();
        }

        \DB::table('country_project')->whereIn('id', $ids)->delete();

        return true;


    }
}
