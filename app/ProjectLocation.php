<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectLocation extends Pivot
{
    public $incrementing = true;

    public $table = 'country_project';

    public function dates()
    {
        return $this->hasMany(ProjectDate::class, 'country_project_id', 'id');
    }
}
