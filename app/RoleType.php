<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleType extends Model
{
    protected $fillable = ['name'];

    protected $hidden = ['created_at', 'updated_at'];

    public function projects()
    {
        return $this->hasMany(ProjectCastingRole::class);
    }
}
