<?php

namespace App\Http\Controllers\UserAccount;

use App\Events\OnInviteSent;
use App\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Class ProductionDataController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class ProductionDataController extends BaseController
{
    public function index(\Illuminate\Http\Request $request)
    {
        $productions = $request->user()->projects()->select('id', 'title')->valid()->get();

        if(!count($productions)) return ['status' => 'error', 'message' => 'No Production. Start by creating a project.'];

        return ['status' => 'ok', 'data' => $productions];
    }

    public function store(Request $request)
    {
        $request->validate([
            'production' => 'required|uuid',
            'message' => 'nullable|max:1000',
            'user' => 'required|uuid'
        ]);

        $user = User::inevitable()->findOrFail(request('user'));

        $production = $request->user()->projects()->select('id')->valid()->findOrFail(request('production'));


        if($user->hasBeenInvited($production)) return ['status' => 'error', 'message' => 'Already invited.'];

        $invite = auth()->user()->invite($user, $production, request('message'));

        event(new OnInviteSent($invite));

        return ['status' => 'ok', 'message' => 'Invitation sent.'];

    }

}
