<?php

namespace App\Http\Controllers\UserAccount;

use App\Ajizzy\GoogleAddressChecker;
use Illuminate\Routing\Controller as BaseController;
/**
 * Class UserLocationController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class UserLocationController extends BaseController
{
        public function store()
        {

            $address = GoogleAddressChecker::getInfo(request('placeId'));

            if($address)
            {
                auth()->user()->profile->update(['location_id' => $address->id]);

                return ['status' => 'ok', 'goto' => ''];
            }

            return ['status' => 'error', 'message' => 'Unable to update location. You can select a top level location like a state instead.'];
        }
}
