<?php

namespace App\Http\Controllers\UserAccount;

use Illuminate\Routing\Controller as BaseController;

class AccountController  extends BaseController{

    public function home()
    {
        //return view('account.profile');

        return redirect('/builders/profile');
    }

}
