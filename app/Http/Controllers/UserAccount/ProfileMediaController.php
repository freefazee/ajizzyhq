<?php

namespace App\Http\Controllers\UserAccount;


use App\Media;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Webpatser\Uuid\Uuid;

/**
 * Class ProfileMediaController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class ProfileMediaController extends BaseController
{

    private $disk = 'public';

    public function __construct()
    {
        $this->disk = config('ajizzy.profile-media-disk');
    }

    /**
     * GET /account/profile/media
     */
    public function index()
    {
        $profile = auth()->user()->profile;
        if(!$profile) return ['data' => []];

        return ['data' => $profile->medias()->select('id', 'url')->get()];
    }

    /**
     * POST /account/profile/media
     *
     * @param $request
     * @return array
     */
    public function store(Request $request)
    {

       if(!in_array($request->input('media_type'), ['image']))
           return response(['status' => 'error', 'message' => 'Invalid media type'], 422);

        $request->validate(['media' => 'required|image']);


        $file = request()->file('media');


        $newFilename = auth()->user()->getMediaPath() . '/' . Uuid::generate() .'.' . $file->guessClientExtension();

        $img = Image::make($file);

        $img->resize(1000,1000, function($constraint) {
              $constraint->aspectRatio();
          });

       Storage::disk($this->disk)->put($newFilename, $img->stream()->detach() );

       $url = Storage::disk($this->disk)->url($newFilename);

       Storage::disk($this->disk)->setVisibility($newFilename, 'public');

        auth()->user()->profile->medias()->create([
            'name' => $newFilename,
            'url' => $url,
            'type' => \request('media_type'),
            'mime' => $file->getClientMimeType(),
            'size' => $file->getSize(),
            'status' => Media::STATUS_PUBLISHED, // images are automatically published
        ]);

       return ['status' => 'ok', 'filename' => $newFilename];

    }

    /**
     * DELETE /account/profile/media
     *
     * @param $id
     * @return array
     */
    public function delete($id)
    {

        $media = auth()->user()->profile->medias()->findOrFail($id);

        Storage::disk($this->disk)->delete($media->name);

        $media->delete();

        return ['status' => 'ok'];

    }
}
