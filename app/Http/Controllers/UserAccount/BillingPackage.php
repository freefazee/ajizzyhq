<?php

namespace App\Http\Controllers\UserAccount;

use App\Http\Requests\OrderSubscriptionRequest;
use App\Price;
use App\PriceRange;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class BillingPackage
 *
 * @package \App\Http\Controllers\UserAccount
 */
class BillingPackage extends BaseController
{

    /**
     * GET /account/packages/{id}/change
     * where {id} = price id
     */
    public function changePackage($id)
    {
        auth()->user()->updatePlan(Price::findOrFail($id));

        return redirect('/join/plans');
    }

    public function getPricing($id)
    {

        return \App\Ajizzy\Price::getPricingDetail(auth()->user()->pricePlan, PriceRange::findOrFail($id));
    }

    /**
     * POST /account/packages/order-subscription
     * @param $request
     * @return mixed
     */
    public function orderSubscription(OrderSubscriptionRequest $request)
    {
        $user  = $request->user();

        $price = $user->pricePlan;
        $range = PriceRange::find(request('range'));

        $amount = $price->calculateTotal($range);

        $transaction = Transaction::createTransaction([
            'amount' => $amount * 100,
            'trans_for' => Transaction::PAYING_FOR_SUBSCRIPTION,
            'method' => request('method'),
            'currency' => $price->country->getCurrencyCode(),
            'qty' => $range->left
        ], $user);

        return [
            'amount' => $amount,
            'txf' => $transaction->txf,
            'email' => $user->email,
            'currency' => $price->country->getCurrencyCode()
        ];



    }
}
