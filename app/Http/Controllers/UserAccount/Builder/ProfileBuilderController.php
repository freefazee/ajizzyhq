<?php

namespace App\Http\Controllers\UserAccount\Builder;

use App\Http\Controllers\Controller as BaseController;

/**
 * Class ProfileBuilderController
 *
 * @package \App\Http\Controllers\UserAccount\Builder
 */
class ProfileBuilderController extends BaseController
{

    /**
     * GET /builders/profile
     */
    public function index()
    {
        return view('account.builders.index');
    }
}
