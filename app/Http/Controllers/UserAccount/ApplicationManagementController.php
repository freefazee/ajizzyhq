<?php

namespace App\Http\Controllers\UserAccount;

use App\Application;
use Illuminate\Routing\Controller as BaseController;
/**
 * Class ApplicationManagementController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class ApplicationManagementController extends BaseController {

    public function index()
    {
        if(request('remove')) return $this->handleRemoval(request('remove'));

        $applications = auth()->user()->applications()->valid()->with(['role.project'])->get();

        return view('account.applications.index')
                ->with('applications', $applications);
    }

    private function handleRemoval($applicationId)
    {
        $application = auth()->user()->applications()->valid()->with(['medias'])->findOrFail($applicationId);

        Application::removeUserApplication($application);

        return back()->with('success', 'Successful');

    }

}
