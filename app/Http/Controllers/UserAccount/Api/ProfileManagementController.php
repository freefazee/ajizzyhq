<?php

namespace App\Http\Controllers\UserAccount\Api;

use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Resources\Media;

/**
 * Class ProfileManagementController
 *
 * @package \App\Http\Controllers\UserAccount\Api
 */
class ProfileManagementController extends BaseController
{

    /**
     * POST account/api/profile
     *
     * Responses to ajax request for user profile update
     */
    public function updateProfile(ProfileUpdateRequest $request)
    {


        $attributes = $request->validated(); //request()->except(['created_at', 'updated_at', 'age_range']);

        $profile = auth()->user()->profile;
        if(!$profile)
        {
            // No use profile, lets create a new one
            auth()->user()->profile()->create($attributes);
        }
        else
        {
            $profile->fill($attributes);
            $profile->save();
        }


        return ['status' => 'ok', 'goto' => route('account.profile.avatar')];

    }

    /**
     * GET /account/api/profile
     *
     * Return json profile of user
     */
    public function showProfile()
    {
        $profile = auth()->user()->getProfileData();
        if(isset($profile->user))
        {
            $profile = $profile->toArray();
            unset($profile['user']);
        }
        return $profile;
    }

    /**
     * GET /account/api/profile/medias
     *
     * Return json profile of user
     */
    public function getMedias()
    {

            $perPage = 12;  //TODO: move to config when admin is built
            return Media::collection(auth()->user()->profile->medias()->paginate($perPage));
    }

    /**
     * POST /account/api/avatar

    public function uploadAvatar(Request $request)
    {

        $profile = auth()->user()->profile;
        if(!$profile) {
            return $this->errorJSonResponse([
                'message' => 'You must create a profile before uploading a profile photo',
                'errors' => [
                    'avatar' => ['You must create a profile before uploading a profile photo']
                ]
            ]);
        }


        $request->validate([
            'avatar' => 'required|image|max:2000' // 2mb max
        ]);

        $oldAvatar = $profile->avatar_url;

        $profile->update([
            'avatar_url' => request()->file('avatar')->store('avatars', 'public')
        ]);

        $path = storage_path('app/public/' . $oldAvatar);
        if(file_exists($path))
        {
            @unlink($path);
        }

        return ['status' => 'ok', 'goto' => route('home')];
    }*/
}
