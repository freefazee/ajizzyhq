<?php

namespace App\Http\Controllers\UserAccount\Api;

use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\SaveAppearanceRequest;
use App\Http\Resources\Appearance;

/**
 * Class AppearanceController
 *
 * @package \App\Http\Controllers\UserAccount\Api
 */
class AppearanceController extends BaseController
{
    public function index()
    {
        $appearance = auth()->user()->appearance;

        if(!$appearance) return ['data' => false];

        return new Appearance(auth()->user()->appearance);
    }

    public function store(SaveAppearanceRequest $request)
    {
        $appearance = \App\Appearance::newOrUpdateForUser(
                auth()->user(),
                request()->except('user_id')
        );

        return ['status' => 'ok', 'data' => $appearance ? new Appearance($appearance) : false];
    }
}
