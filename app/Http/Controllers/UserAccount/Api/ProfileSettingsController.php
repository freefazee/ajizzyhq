<?php

namespace App\Http\Controllers\UserAccount\Api;

use App\Http\Controllers\Controller as BaseController;
/**
 * Class ProfileSettingsController
 *
 * @package \App\Http\Controllers\UserAccount\Api
 */
class ProfileSettingsController extends BaseController
{

    /**
     *  GET /account/api/profile/settings
     */
    public function getProfileInfo()
    {
        return ['status' => 'ok', 'data' => auth()->user()->getBasicProfile()];
    }
}
