<?php

namespace App\Http\Controllers\UserAccount;

use App\Ajizzy\Billing\CardDetailHelper;
use App\Ajizzy\Billing\RavePaymentVerification;
use App\Ajizzy\Contracts\PaymentVerificationContract;
use App\ClaimedTransaction;
use App\Events\SubscriptionPaymentReceipt;
use App\Transaction;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class BillingTransactionVerify
 *
 * @package \App\Http\Controllers\UserAccount
 */
class BillingTransactionVerify extends BaseController
{
    private $paymentGateway;


    public function __construct(PaymentVerificationContract $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * GET /account/billing/{txf}/verify
     */
     public function check($txf)
     {
        $transaction = Transaction::findByTxf($txf);

        $user = auth()->user();

        if(!$transaction) return response(['message' => 'transaction not found'], 404);

         $this->paymentGateway->verify($transaction->amount, $txf);
         $message = $this->paymentGateway->message();

        if($this->paymentGateway->isSuccessful())
        {
            $transaction->markAsSuccessful($message);
            $user->profile->activateSubscriptionForMonths($transaction->qty);

            event(new SubscriptionPaymentReceipt($transaction));

            $this->storeCard($user);

            return ['status' => 'ok', 'message' => $message];
        }

         $transaction->markAsFailed($message);


        return ['status' => 'error', 'message' => $this->paymentGateway->message()];

     }

     private function storeCard($user)
     {
         $cardHelper = new CardDetailHelper($this->paymentGateway->card());
         $cardHelper->store($user);
     }

}
