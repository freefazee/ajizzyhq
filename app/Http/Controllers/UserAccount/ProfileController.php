<?php

namespace App\Http\Controllers\UserAccount;

use Illuminate\Routing\Controller as BaseController;

class ProfileController  extends BaseController{

    public function profile()
    {
        return redirect()->route('builders.profile');
    }

    public function addAvatar()
    {
        return redirect(route('builders.profile') . '/photos');
    }

}
