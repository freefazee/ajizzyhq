<?php

namespace App\Http\Controllers\UserAccount;


use App\Ajizzy\Project;
use App\User;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class AccountDashboardController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class AccountDashboardController extends BaseController
{

    /**
     * GET /account/home
     */
    public function index()
    {

        $data = [];
        if(auth()->user()->isTalent())
        {
            $data['latestProjects'] = Project::getLatstProjects(4);;
        }
        else
        {
            $data['notedTalents'] = User::getLatestNotes(auth()->user(), 3);
        }

        $data['productionCount']   = Project::getUserProjectCount(auth()->user());


        return view('account.dashboard')
                    ->with($data);
    }

}
