<?php

namespace App\Http\Controllers\UserAccount;

use App\Http\Requests\SetNewUserPasswordRequest;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class AccountPasswordController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class AccountPasswordController extends BaseController
{

    /**
     * POST /account/profile/password
     *
     * Set new user password
     */
    public function setNewPassword(SetNewUserPasswordRequest $request)
    {
        auth()->user()->setNewPassword(request('password'));

        auth()->logout();

        session()->flash('success', __('ajizzy.password changed'));

        return request()->isJson() ? ['status' => 'ok', 'goto' => route('login')] : redirect()->route('login');

    }
}
