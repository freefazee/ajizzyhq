<?php

namespace App\Http\Controllers\UserAccount;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class AccountSettingsController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class AccountSettingsController extends  BaseController
{
    public function __construct()
    {
        $this->middleware(function($request, $next){
            if(!$request->user()->profile)
            {
                return redirect('/account');
            }

           return $next($request);
        });

    }

    public function profileSettings()
    {

        return view('account.settings')
                    ->with('me', auth()->user());
    }

    public function subscriptions()
    {

        return view('account.settings-subscription')
                ->with('transactions', auth()->user()->transactions()->paginate(10));
    }
}
