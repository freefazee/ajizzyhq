<?php

namespace App\Http\Controllers\UserAccount;

use Illuminate\Routing\Controller as BaseController;

class LogoutController extends BaseController
{
    /**
     * GET /account/logout
     */
    public function logout()
    {
        session()->forget('location');
        session()->flush();
        \Auth::logout();
        return redirect()->route('home');
    }
}
