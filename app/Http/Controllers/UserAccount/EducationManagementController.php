<?php

namespace App\Http\Controllers\UserAccount;

use App\Http\Resources\Education;
use App\Http\Resources\EducationResource;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
/**
 * Class EducationManagementController
 *
 * @package \App\Http\Controllers\UserAccount
 */
class EducationManagementController extends BaseController
{
    /**
     * GET /account/educations
     *
     * @param $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return EducationResource::collection($request->user()->educations()->oldest('year_completed')->get());
    }

    /**
     * POST /account/educations
     */
    public function store(Request $request)
    {
        $request->validate([
                        'school' => 'required',
                        'year_completed' => 'required|numeric|min:0',
                        'degree' => 'required',
                        'location' => 'required']);

        $request->user()->educations()->create($request->except('user_id', 'id'));

        return ['status' => 'ok'];
    }

    /**
     * PATCH /account/educations/{id}
     */
    public function update($id)
    {
        $education = auth()->user()->educations()->findOrFail($id);

        $education->update(\request()->except('user_id', 'id'));

        return ['status' => 'ok'];
    }

    /**
     * DELETE /account/educations/{id}
     */
    public function delete($id)
    {
        $education = auth()->user()->educations()->findOrFail($id);

        $education->delete();

        return ['status' => 'ok'];
    }
}
