<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class ApplyToProjectBaseController
 *
 * @package \App\Http\Controllers\Core
 */
class ApplyToProjectBaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!$request->user())
            {
               if($request->input('modal')) return response(['status' => 'error', 'goto' => '/login']);
                return redirect('/login');
            }

            $user = $request->user();
            if(!$user->hasSubscription())
            {
                if($request->input('modal')) return response(['status' => 'error', 'goto' => '/join/plans']);
                return redirect('/join/plans');
            }
            return $next($request);
        });
    }
}
