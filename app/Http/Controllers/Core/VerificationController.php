<?php

namespace App\Http\Controllers\Core;

use App\Events\EmailVerificationNotification;
use App\Events\OnRegistered;
use App\Http\Controllers\Controller;

/**
 * Class VerificationController
 *
 * @package \App\Http\Controllers\Core
 */
class VerificationController extends Controller
{

    public function index()
    {
        if(request('resend'))
        {
            event(new OnRegistered(auth()->user()));
            return back()->with('success', 'Email successfully resent.');
        }


        return view('account.verify');
    }

    public function activate()
    {
         if(auth()->user()->verifyEmail(request('hash'))) {
            return redirect()->route('account.dashboard')->with('success', 'Thank you for activating your email.');
         }

         return redirect()->route('verify')->with('error', 'Unable to verify your email. You can resend the email again.');
    }

}
