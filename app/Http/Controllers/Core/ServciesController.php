<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class ServciesController
 *
 * @package \App\Http\Controllers\Core
 */
class ServciesController extends Controller
{

    public function terms()
    {
        return view('public.terms');
    }
}
