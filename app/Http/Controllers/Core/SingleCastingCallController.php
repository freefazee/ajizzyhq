<?php

namespace App\Http\Controllers\Core;

use App\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ajizzy\Project;

class SingleCastingCallController extends Controller
{

    /**
     * GET /casting-call/{id}
     */
    public function index(Request $request, $projectId)
    {
        $project = Project::getOneProjectOrFail($projectId);


        $rolesApplied = Application::getUserAppliedRoleIds($project, $request->user());

        $similar = Project::getSimilar($project);

        return view('casting-calls.single')
                ->with('project', $project)
                ->with('rolesApplied', $rolesApplied)
                ->with('similar', $similar);
    }
}
