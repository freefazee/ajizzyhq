<?php

namespace App\Http\Controllers\Core;

use App\Ajizzy\Project;
use App\Ajizzy\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class HomePageController extends Controller
{

    /**
     * GET /
     * Route name: home
     */
    public function home()
    {

        $categories = Category::getOneLatestProjectForAllChild();
        $featuredCastings = Project::featuredCastings(4);



        return view('home')
                ->with('categories', $categories)
                ->with('featuredCastings', $featuredCastings);
    }

    /**
     * GET /performers/how-it-works
     */
    public function performHowItWorks()
    {
        return view('public.performers-how-it-works');
    }

    /**
     * GET /geo/search
     */
    public function geo()
    {
        $q  = (new \App\Country())->newQuery();
        if(request('q'))
        {
            $q->where('name', 'LIKE', '%' . request('q') . '%');
        }
        else
        {
            $q->whereNull('parent_id');
        }

        return $q->get();
    }
}
