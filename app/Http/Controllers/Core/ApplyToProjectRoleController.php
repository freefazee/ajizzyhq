<?php

namespace App\Http\Controllers\Core;

use App\Ajizzy\Project;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class ApplyToProjectRoleController
 *
 * @package \App\Http\Controllers\Core
 */
class ApplyToProjectRoleController extends ApplyToProjectBaseController
{

    /**
     * POST /casting-calls/{$projectId}/apply where {$projectId] = project id
     * @param $projectId
     * @param $request
     * @return array
     */
    public function store(Request $request, $projectId)
    {
      $project = Project::findOneOrFail($projectId);

      if(!$project->accepting())  return response(['errors' => 'This project has stopped accepting applications.'], 422);

      $role = $project->roles()->findOrFail(request('role_id'));

       $rolesApplied = Application::getUserAppliedRoleIds($project, $request->user());

       if(in_array($role->id, $rolesApplied->toArray())){
           return response(['errors' => 'You have applied for this role.'], 422);
       }

      $userRole =  $project->applications()
          ->forUser(auth()->user())
          ->where('project_casting_role_id', $role->id)
          ->first();

      // lets check for required documents
        $rules = [];
       if($userRole)
       {
           $uploadedMediaIds = $userRole->medias->pluck('media_type_id')->toArray();
           foreach($role->mediaTypes as $mediaType)
           {
               if(!in_array($mediaType->id, $uploadedMediaIds))
               {
                   $v = 'required';
                   if($mediaType->input_type === 'upload')
                   {
                       $v .= '|mimetypes:' . $mediaType->valid_mime_types;
                   }
                   $rules['media-' . $mediaType->id] =  $v;
               }
           }
       }
       if($rules)
       {
           $validator = Validator::make(request()->all(), $rules);
           if($validator->fails()) {
               return response(['errors' => 'Please, provide all the required documents.', 'fullerrors' => $validator->errors()], 422);
           }
       }

       // save text content
        $postedTextContent = $request->except('role_id', 'modal');

        $application = Application::createApplication($request->user(), $project, $role);

        foreach($postedTextContent as $key=>$content)
        {
            $key = trim(str_ireplace('media-', '', $key));
            $application->createSingleMedia($key, ['filename' => $content]);
        }

        return ['status' => 'ok', 'application' => $application, 'medias' => Application::getUserMedias($application)];

    }

    /**
     * GET /casting-calls/{$projectId}/draft-apply where {$projectId] = project id
     * @param $request
     * @param $projectId
     * @return array
     */
    public function draftStore(Request $request, $projectId)
    {
        $project = Project::findOneOrFail($projectId);

        $role = $project->roles()->find(request('role_id'));

        $rolesApplied = Application::getUserAppliedRoleIds($project, $request->user());
        if(in_array($role->id, $rolesApplied->toArray())) return response(['message' => 'You have applied for this role.', 'status' => 'error'], 422);

        $application = Application::createDraft($request->user(), $project, $role);

        return ['status' => 'ok', 'application' => $application, 'medias' => Application::getUserMedias($application)];

    }

}
