<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class AboutController
 *
 * @package \App\Http\Controllers\Core
 */
class AboutController extends Controller
{

    public function index()
    {
        return view('public.about');
    }
}
