<?php

namespace App\Http\Controllers\Core;

use App\Http\Resources\Profile as ProfileResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ajizzy\Profile;


/**
 * Class TalentFeedController
 *
 * @package \App\Http\Controllers\Core
 */
class TalentFeedController extends Controller
{

    public function index(Request $request)
    {


        $records =  ProfileResource::collection(Profile::fetchAll($request->only('role_gender'), 16));


       return $records;


    }

    public function show($userId)
    {
        return ['status' => 'ok', 'data' => Profile::findOneOrFail($userId)];
    }

}
