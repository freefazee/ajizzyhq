<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ethnicity;
use App\Services\CategoryService;
use Illuminate\Http\Request;

/**
 * Class CastingCallsController
 *
 * @package \App\Http\Controllers\Core
 */
class CastingCallsController extends Controller
{
    private $categoryService = null;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * GET /casting-calls
     *
     * @return mixed
     */
    public function index()
    {
        $categories = $this->categoryService->getActiveCategories();
        $ethnicities = \App\Ethnicity::select('id', 'name')->get();

        return view('casting-calls.index')
                    ->with('ethnicities', $ethnicities)
                    ->with('categories', $categories);
    }

    /**
     * /casting-calls/feeds
     * @param $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $filters = $request->only(
                    'role_gender',
                         'production_type',
                         'is_paid', 'union_type',
                            'ethnicities', 'min_age', 'max_age'
                        );

        return \App\Ajizzy\Project::fetchProjects($filters);
    }

    /**
     * /casting-calls/feeds/{projectId}
     */

    public function show($projectId)
    {

        $project = \App\Ajizzy\Project::findOneOrFail($projectId);

        return $project;

    }
}
