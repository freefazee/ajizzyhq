<?php

namespace App\Http\Controllers\Core;

use App\Ajizzy\Project;
use App\Application;
use App\MediaType;
use Illuminate\Support\Facades\Validator;

/**
 * Class ApplyToProjectRoleAddMediaController
 *
 * @package \App\Http\Controllers\Core
 */
class ApplyToProjectRoleAddMediaController extends ApplyToProjectBaseController
{

    /**
     * POST /casting-calls/{projectId}/apply/{applicationId}/media
     *
     * @param $projectId
     * @param $applicationId
     * @param $mediaTypeId
     * @return mixed
     */
    public function store($projectId, $applicationId, $mediaTypeId)
    {
        $disk = config('ajizzy.application-media-disk');

        $project = Project::findOneOrFail($projectId);

        $application = $project->applications()->draft()->findOrFail($applicationId);

        $mediaType = MediaType::findOrFail($mediaTypeId);
        $filename =  'media-' . $mediaType->id;


        $validator = Validator::make(request()->all(), [
            $filename => 'required|mimetypes:' . $mediaType->valid_mime_types
        ], ['required' => e($mediaType->name) . ' is required', 'mimetypes' => 'This file format is not currently supported.']);

        if($validator->fails()) {
            return response(['errors' => $validator->errors()], 422);
        }

        $filePath = request()->file($filename)->store('role-applications',  $disk);

        $media = $application->medias()->where('media_type_id', $mediaType->id )->first();
        if($media)
        {
            $oldMedia = $media->filename;
            $media->update(['filename' => $filePath]);
            $path = storage_path('app/' . $oldMedia);
            if(file_exists($path))  @unlink($path);
        }
        else
        {
            $media = $application->medias()->create(['filename' => $filePath, 'media_type_id' => $mediaType->id]);
        }

        return ['status' => 'ok', 'medias' => Application::getUserMedias($application), 'data' => $media];
    }
}
