<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class CreatorPublicController
 *
 * @package \App\Http\Controllers\Core
 */
class CreatorPublicController extends Controller
{

    public function creatorHowItWorks()
    {

        return view('public.creators-how-it-works');
    }

}
