<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use App\Http\Resources\Media;
use App\Http\Resources\PublicAppearance;
use App\Profile;
use Illuminate\Support\Facades\Redirect;

/**
 * Class PublicProfileController
 *
 * @package \App\Http\Controllers\Core
 */
class PublicProfileController extends Controller
{

    /**
     * @param $username - can be a uuid for user id or public_profile string
     *
     * @return mixed
     */
    public function index($username)
    {
        $talent = Profile::with([
                    'user:id,first_name,last_name,country_id',
                    'user.country:id,name,official,cca2,parent_id',
                    'location:id,name,official,cca2,parent_id as loca',
                    'user.educations',
                    'user.ethnicities:name',
                    'tags:id,name'
                ])->where('profile_url', $username)
                            ->orWhere('user_id', $username)
                                ->first();

            if(!$talent) return Redirect::to('/talents', 302);

            $talent->appearance = $talent->user->appearance()->with('bodySize', 'eye', 'hair')->first();


            return view('public.index')
                ->with('talent', $talent);
    }


    public function media($username)
    {
        $user = Profile::where('profile_url', $username)->first();

        if(!$user) return ['status' => 'error', 'message' => 'Profile not found'];

        $perPage = request('per_page') ?: 12;

        if($perPage > 100) $perPage = 100; // we do not want people over pull the server
        return Media::collection($user->medias()->paginate($perPage));
    }


    public function appearance($username)
    {
        $profile = Profile::where('profile_url', $username)->first();

        if(!$profile) return ['status' => 'error', 'message' => 'Profile not found'];

        return new PublicAppearance($profile->user->appearance()->with(['bodySize', 'eye', 'hair', 'user.ethnicities'])->first());
    }
}
