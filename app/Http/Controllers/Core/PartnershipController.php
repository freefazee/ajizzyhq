<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class PartnershipController
 *
 * @package \App\Http\Controllers\Core
 */
class PartnershipController extends Controller
{

    public function index()
    {
        return view('public.partnerships');
    }

}
