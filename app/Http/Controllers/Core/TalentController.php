<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
/**
 * Class TalentController
 *
 * @package \App\Http\Controllers\Core
 */
class TalentController extends Controller
{

    public function index()
    {
        return view('talent.index');
    }
}
