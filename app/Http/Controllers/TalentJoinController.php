<?php

namespace App\Http\Controllers;

use App\Ajizzy\Price;
use App\Country;
use App\Events\OnRegistered;
use App\Http\Requests\TalentRegistrationRequest;
use App\PriceRange;
use App\User;

class TalentJoinController extends Controller
{

    /**
     * GET /join
     */
    public function show()
    {

        if(\request('plan'))
        {
            return view('auth.join-form')->with('countries', Country::getAll());
        }


        $pricingForLocation = Price::getPriceForLocation(session('location'));

        return view('auth.join')
                ->with('price', $pricingForLocation);
    }

    /**
     * @param $request
     * POST /join
     * @return
     */
    public function store(TalentRegistrationRequest $request)
    {

        $data = $request->validated();
        unset($data['country']);
        $data['phone'] = formatPhone(request('phone'), $request->db_country->getShortName());
        $data['country_id'] = $request->db_country->id;

        $user = User::createTalentAccount($data);

        event(new OnRegistered($user));

        \Auth::loginUsingId($user->id);

        session()->forget('location');

        if($user->plan)
            return redirect()->route('join.plan-selection');
        else
            return redirect()->route('account.profile');
    }

    /**
     * GET /join/plans
     */
    public function selectPlan()
    {
        // Only continue to this section if the user completed step 1
        $this->middleware('auth');

        auth()->user()->createProfile();

        $pricing = auth()->user()->pricePlan;
        if(!$pricing) // we need to check if we want to redirect them to profile builder or pricing plan.
        {
            return redirect('/join?changePackage=1');
        }

        $pricingInfo  = Price::getPricingDetail($pricing, PriceRange::getByRange(6));

        return view('auth.join-payment-option')
                    ->with('pricingInfo', json_encode($pricingInfo))
                    ->with('paymentRanges',  PriceRange::get());
    }
}
