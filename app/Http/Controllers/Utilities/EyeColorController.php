<?php

namespace App\Http\Controllers\Utilities;

use App\EyeColor;
use App\Http\Controllers\Controller;

/**
 * Class EyeColorController
 *
 * @package \App\Http\Controllers\Utilities
 */
class EyeColorController extends  Controller
{

    public function index()
    {
        return EyeColor::get();
    }
}
