<?php

namespace App\Http\Controllers\Utilities;

use App\BodySize;
use App\EyeColor;
use App\HairColor;
use App\Http\Controllers\Controller;
use App\Http\Resources\Appearance;
use App\Http\Resources\Ethnicity;

/**
 * Class AppearanceController
 *
 * @package \App\Http\Controllers\Utilities
 */
class AppearanceController extends Controller
{
    public function index()
    {
        $appearance = auth()->user()->appearance;

        return [
            'body_types' => BodySize::get(),
            'eye_colors' => EyeColor::get(),
            'hair_colors' => HairColor::get(),
            'appearance' => $appearance ? new Appearance($appearance) :  false,
            'ethnicities' => Ethnicity::collection(\App\Ethnicity::get())
        ];
    }

}
