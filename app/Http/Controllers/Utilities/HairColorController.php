<?php

namespace App\Http\Controllers\Utilities;

use App\HairColor;
use App\Http\Controllers\Controller;

/**
 * Class HairColorController
 *
 * @package \App\Http\Controllers\Utilities
 */
class HairColorController extends Controller
{
    public function index()
    {
        return HairColor::get();
    }
}
