<?php

namespace App\Http\Controllers\Utilities;

use App\Http\Controllers\Controller as BaseController;
use App\Http\Resources\SkillResource;
use App\Http\Resources\TagResource;
use Illuminate\Support\Facades\Log;
use Spatie\Tags\Tag;

/**
 * Class ProfileSkillController
 *
 * @package \App\Http\Controllers\UserAccount\Builder
 */
class ProfileSkillController extends BaseController
{

    public function index()
    {

        try {

            $tags = SkillResource::collection(auth()->user()->profile->tags);
            $autoComplete = TagResource::collection(Tag::get());
            return ['status' => 'ok', 'data' => ['local' => $tags, 'auto_complete' => $autoComplete]];

        } catch (\Exception $e) {
            Log::error('FETCH_TAG_ERROR: ' . $e->getMessage());
            return ['status' => 'error', 'message' => 'Unable to fetch tags'];
        }
    }

    public function store()
    {
        $tags = explode(',', request('payload'));
        //TODO: move this check to a config
        if(count($tags) > 5) {
            return ['status' => 'error', 'message' => 'You can only add a maximum of 5 skills.'];
        }

        auth()->user()->profile->syncTags($tags);

        return ['status' => 'ok', 'message' => 'Saved'];
    }
}
