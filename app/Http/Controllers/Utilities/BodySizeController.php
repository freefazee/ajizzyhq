<?php

namespace App\Http\Controllers\Utilities;

use App\BodySize;

use App\Http\Controllers\Controller;

/**
 * Class BodySizeController
 *
 * @package \App\Http\Controllers\Utilities
 */
class BodySizeController extends Controller
{

    public function index()
    {
        return BodySize::get();
    }
}
