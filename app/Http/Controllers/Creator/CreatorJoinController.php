<?php

namespace App\Http\Controllers\Creator;

use App\Country;
use App\Http\Requests\CreateNewCreatorRequest;
use App\Occupation;
use App\User;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class CreatorJoinController
 *
 * @package \App\Http\Controllers\Creator
 */
class CreatorJoinController extends BaseController
{

    /**
     * GET /creator/join
     */
    public function index()
    {
        return view('auth.creator-join')
                ->with('occupations', Occupation::getAll())
                ->with('countries', Country::getAll());
    }

    /**
     * POST /creator/join
     * @param \App\Http\Requests\CreateNewCreatorRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateNewCreatorRequest $request)
    {
        $data = $request->validated();


        $data['phone'] = formatPhone(request('phone'), $request->db_country->cca2);
        $data['country_id'] = $request->db_country->id;
        $data['account_type'] = User::ACCOUNT_TYPE_CREATOR;


        $user = User::create($data);

        $user->createProfile($request->only(['company', 'title', 'occupation_id']));

        auth()->loginUsingId($user->id);

        session()->forget('location');

        return redirect()->route('creator.company.profile-update');
    }

}
