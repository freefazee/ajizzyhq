<?php

namespace App\Http\Controllers\Creator;
use App\Ajizzy\GoogleAddressChecker;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
/**
 * Class CreatorCompanyProfileUpdate
 *
 * @package \App\Http\Controllers\Creator
 */
class CreatorCompanyProfileUpdate extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * GET /creator/company-profile-update
     */
    public function index()
    {

       $loc = auth()->user()->profile->location ?: auth()->user()->country;


       $profileData = [
           'company' => auth()->user()->profile->company,
           'about_company' => auth()->user()->profile->about_company,
           'place_id' => ($loc && isset($loc->id)) ? $loc->id : '',
           'location' => ($loc && $loc->official) ? $loc->official : 'No location set'
       ];


        return view('creator.update-company-profile', ['country' => auth()->user()->country, 'profile' => $profileData]);
    }

    /**
     * POST /creator/company-profile-update
     */
    public function store(Request $request)
    {

        $loc = auth()->user()->profile->location;

        $location = \App\Country::find($request->input('place_id'));
        $placeId = trim($request->input('place_id'));


        if(!$location && $placeId) {

                if($loc && $loc !== $placeId) {
                    $location =  GoogleAddressChecker::getInfo($placeId);
                    if(!$location)
                        return ['status' => 'error',  'message' => 'Unable to add city. Could be an issue with google service'];
                    else
                        $request['location_id'] = $location ? $location->id : '';
                } else {
                    $request['location_id'] = $loc;
                }
        } else {
            $request['location_id'] = $location->id;
        }





        auth()->user()->profile->update($request->only('location_id', 'company', 'about_company'));

        return ['status' => 'ok'];


    }
}
