<?php

namespace App\Http\Controllers\Creator\Project;

use App\Ajizzy\GoogleAddressChecker;
use App\Ethnicity;
use App\Http\Requests\AddLocationToProjectRequest;
use App\Http\Requests\AddLocationToProjectRequestWithDates;
use App\Http\Requests\AddRoleToProjectRequest;
use App\Http\Requests\ProjectStoreRequest;
use App\MediaType;
use App\Project;
use App\ProjectLocation;
use App\RoleType;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class ProjectController
 *
 * @package \App\Http\Controllers\Creator\Project
 */
class ProjectController extends BaseController
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * This method creates a drafted project and redirects to /creator/projects/{id}/status
     */
    public function index()
    {
        $project = Project::createDrafted(auth()->user());

        return redirect()->route('creator.projects.show', [$project->id, $project->status]);
    }

    /**
     * Display project creation page
     *
     * @param $id
     * @param $status - this is not used
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, $status)
    {
        $project = \App\Ajizzy\Project::findForUserOrFail($id);

        $categories = $this->categoryService->getActiveCategories();

        $meta  = json_encode([
            'ethnicities' => Ethnicity::select('id', 'name')->get(),
            'mediaTypes' => MediaType::select('id', 'name')->get(),
            'roleTypes' => RoleType::select('id', 'name')->get()
        ]);

        return view('creator.post')
                ->with(compact('project', 'categories', 'meta'));
    }

    /**
     * Store project
     *
     * @param \App\Http\Requests\ProjectStoreRequest $request
     * @param                                        $id
     *
     * @return array
     */
    public function store(ProjectStoreRequest $request, $id)
    {
        $project = \App\Ajizzy\Project::findForUserOrFail($id);

       /* if(request('save') === '0')
        {
            if($project->notPublished())
                $request['status'] = 'drafted';
            else
                $request['status'] = 'published';
        }
        else
        {
            $request['status'] = 'published';
        }*/

        $request['status'] = 'published';
        if(!$project->roles()->count())  return ['status' => 'error', 'message' => 'You need to add some roles.'];

        $excepts = [
                'id', 'save',
                'country', 'country_id',
                'formatted_expiry_date',
                'formatted_created_at',
                'owner',
                'user_id',
                'loc',
                'locations'];

        $project->update(request()->except($excepts));
        return ['status' => 'ok', 'project' => $project->id];
    }

    public function storeRole(AddRoleToProjectRequest $request, $projectId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);
        $ethnicities = request('ethnicities');
        $mediaTypes = request('media_types');
        $role = $project->addRole($request->except('media_types', 'ethnicities', 'role_type'));


        if($role) {
            $role->addEthnicities($ethnicities);
            $role->addMediaTypes($mediaTypes);
        }

        return ['status' => 'ok'];
    }

    public function getRoles($projectId)
    {
        $project = auth()->user()->projects()->with(['roles' => function($q) {
            $q->select('id', 'project_id', 'name', 'gender', 'is_transgender', 'min_age', 'max_age', 'description', 'requires_nudity', 'role_type_id');
        }, 'roles.ethnicities', 'roles.mediaTypes', 'roles.roleType'])->findOrFail($projectId);

        return [
            'status' => 'ok',
            'data' => $project->roles
        ];
    }


    public function deleteRole($projectId, $roleId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $role = $project->roles()->findOrFail($roleId);

        $project->removeRole($role->id);

        return ['status' => 'ok'];

    }

    /**
     *  POST /creator/projects/<projectId>/locations
     *
     * @param \App\Http\Requests\AddLocationToProjectRequest $request
     * @param                                                $projectId
     *
     * @return array
     */
    public function addLocation(AddLocationToProjectRequest $request, $projectId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $project->addLocation([\request('country_id') => ['location_type' => $project->location]]);

        $project->clearOldLocationData($project->location);

        return ['status' => 'ok'];
    }

    /**
     *  POST /creator/projects/<projectId>/locations-with-dates
     *
     * Add/update location with dates. A location can have multiple dates attached to it.
     *  The working on this method is similar to $this->addLocation() but here you can send date payload
     *
     * @param  $request
     * @param $projectId
     * @return array
     */
    public function addLocationWithDates(AddLocationToProjectRequestWithDates $request, $projectId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $payload = [
            \request('country_id') => array_merge($request->only('city', 'venue', 'street', 'province'), ['location_type' => $project->location])
        ];

        $project->addLocation($payload);

        $location = ProjectLocation::where('project_id', $project->id)->findOrFail(request('id'));

        //locationInfo
        $locationInfo = collect(request('locationInfo'));

        $locationInfo->each(function($item) use ($location) {
            $item = collect($item);
           $id = isset($item['id']) ? $item['id'] : false;
           if($id)
           {
               $location->dates()->where('id', $id)->update($item->except(['id', 'country_project_id'])->toArray());
           }
           else
           {
               $location->dates()->create($item->except(['id', 'country_project_id'])->toArray());
           }
        });


        return ['status' => 'ok', 'data' => $location];
    }


    /**
     * GET /creator/projects/<projectId>/locations
     * @param $projectId
     * @return array
     */
    public function getLocations($projectId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $project->update(['location' => request('location_type')]);

        $project->clearOldLocationData(request('location_type'));

        return $project->getLocations(request('location_type'));
    }

    /**
     * DELETE /creator/projects/<projectId>/locations/<locationId>
     *
     * @param $projectId
     * @param $locationId
     * @return array
     */
    public function removeLocation($projectId, $locationId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $project->removeLocation($locationId);

        return ['status' => 'ok'];

    }

    /**
     * GET /creator/projects/<projectId>/locations-with-dates/<locationId>
     *
     * @param $projectId
     * @param $locationId
     * @return array
     */
    public function getLocationDates($projectId, $locationId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $locationObj = $project->locations()->findOrFail($locationId);

        return $locationObj->location->dates;
    }

    /** DELETE  /creator/projects/{id}/locations-with-dates/{locationId}/{dateId}
     *
     * @param $projectId
     * @param $locationId
     * @param $dateId
     *
     * @return array
     */
    public function removeLocationDate($projectId, $locationId, $dateId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $locationObj = ProjectLocation::where('project_id', $project->id)
                                        ->findOrFail($locationId);

        $locationDate = $locationObj->dates()->findOrFail($dateId);

        $locationDate->delete();

        return ['status' => 'ok'];
    }

    /**
     * Attach Country to project
     *
     * POST  /creator/projects/{id}/attach-country
     */
    public function attachCountry($projectId, Request $request)
    {
        $request->validate(['country' => 'exists:countries,id']);

        $project = \App\Ajizzy\Project::findForUserOrFail($projectId);

        $result = $project->attachCountry($request->input('country'));

        return ['status' => 'ok', 'country' => $result];

    }


    public function attachCity($projectId)
    {
        $project = \App\Ajizzy\Project::findForUserOrFail($projectId);

        $location =  GoogleAddressChecker::getInfo(\request('place_id'));

        if($location)
        {
            $project->addLocation([$location->id => ['location_type' => $project->location]]);

           $project->clearOldLocationData($project->location);

            return ['status' => 'ok'];

        }

        return ['status' => 'error', 'message' => 'Unable to add this location. Add a top level location and use the audition details tab to add address and time.'];

    }


}
