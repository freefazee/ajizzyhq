<?php

namespace App\Http\Controllers\Creator\Project;

use Illuminate\Routing\Controller as BaseController;

/**
 * Class ProjectBasinController
 *
 * @package \App\Http\Controllers\Creator\Project
 */
class ProjectBasinController extends BaseController
{

    /**
     * POST /creator/projects/<projectId>/basins/<applicationId>
     *
     * @param $projectId
     * @param $applicationId
     *
     * @return mixed
     */
    public function store($projectId, $applicationId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $application =  $project->applications()->valid()->findOrFail($applicationId);

        $application->setBasin(request('basin'));

        return ['status' => 'ok', 'message' => $application->getBasinMessage()];
    }
}
