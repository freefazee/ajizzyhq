<?php

namespace App\Http\Controllers\Creator\Project;


use App\Application;
use App\Events\ApplicationDownload;
use App\Events\GenerateApplicationDownloadChunk;
use App\Http\Resources\ApplicationTalent;
use Illuminate\Routing\Controller as BaseController;

use EloquentBuilder;
use Illuminate\Support\Facades\Storage;

/**
 * Class ProjectApplicationsController
 *
 * @package \App\Http\Controllers\Creator\Project
 */
class ProjectApplicationsController extends BaseController
{

    /**
     * GET /creator/projects/<projectId>/applications-feeds
     */
    public function feeds($projectId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $perPage = request('per_page') ?: 16;

        $applications =  $project->applications()->valid()->newQuery();

        if(request('basin'))
            $applications->where('basin', request('basin'));


        $records = $applications->with(['owner.profile', 'medias'])->paginate($perPage);

       return ApplicationTalent::collection($records);

    }

    /**
     * GET /creator/projects/<projectId>/applications
     */
    public function index($projectId)
    {

        if(request('basin')) return $this->handleBasinOperation($projectId, request('basin'), request('application'));

        if(request('media')) return $this->handleMediaOperation($projectId, request('media'), request('application'));

        $project = auth()->user()->projects()->with('roles:id,project_id,name,gender')->findOrFail($projectId);


        $perPage = request('per_page') ?: 30;

        $filter = $project->applications()->valid()->newQuery();

        if(request('role')) $filter->whereHas('role', function($query){ $query->where('id', request('role')); });

        if(request('q'))
        {
            $filter->whereHas('owner', function ($query){
                $query->where('first_name', 'LIKE', '%' .  request('q') . '%')
                        ->OrWhere('last_name', 'LIKE', '%' . request('q')  . '%')
                        ->OrWhere('email', 'LIKE', '%' . request('q')  . '%')
                        ->OrWhere('phone', 'LIKE', '%' . request('q')  . '%');
            });
        }

        if(request('status'))
        {
            $filter->where('basin', request('status'));
        }


        $filter->with([
            'owner.profile:id,user_id,stage_name,profile_url', 'role:id,name', 'medias.mediaType', 'owner.profile.medias' => function($query){
            $query->where('type', 'image');
        }]);


        if(request('action') === 'download')
        {
            // do download
            $result = $project->initDownloadPath(); // create or empty the download path. $project->getDownloadPath() to get the full path

            if(!$result) return back()->with('error', 'Unable to create file for download. Report this error to the admin.');

            $filter->chunk(100, function ($applications) use ($project) {
                event(new GenerateApplicationDownloadChunk($applications, $project));
            });

            event(new ApplicationDownload($project, auth()->user()));

            return redirect()->route('creator.projects.applications', $project->id)
                        ->with('success', 'Data will be sent to ' . e(auth()->user()->email));

        }
        else
        {
            $records = $filter->latest()->paginate($perPage);
        }

        return view('creator.projects.applications')
                ->with('project', $project)
                ->with('applications', $records)
                ->with('status', Application::statusList());
    }


    private function handleBasinOperation($projectId, $basin, $applicationId)
    {
        $project = auth()->user()->projects()->findOrFail($projectId);

        $application = $project->applications()->valid()->findOrFail($applicationId);

        $application->setBasin($basin);

        return back()->with('success', 'Status updated');
    }


    private function handleMediaOperation($projectId, $mediaId, $applicationId)
    {
        $application = auth()->user()->projects()->findOrFail($projectId)->applications()->valid()->findOrFail($applicationId);


        $media = $application->medias()->with('mediaType')->findOrFail($mediaId);

        if($media->mediaType->input_type === 'upload')
        {
            // get temp url
           $url = Storage::disk(config('ajizzy.application-media-disk'))->temporaryUrl($media->filename, now()->addMinute(3));

           return redirect($url);
        }

        return view('creator.projects._media-text-input')
                ->with('media', $media)
                ->with('application', $application);
    }

}
