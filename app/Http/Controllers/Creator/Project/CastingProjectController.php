<?php

namespace App\Http\Controllers\Creator\Project;

use App\User;
use Illuminate\Routing\Controller as BaseController;
/**
 * Class CastingProjectController
 *
 * @package \App\Http\Controllers\Creator\Project
 */
class CastingProjectController extends BaseController
{

    /**
     * GET /creator/projects
     */
    public function index()
    {
        $projectsCount = auth()->user()->projects()->count();
        $projects = auth()->user()->projects()->withCount(['applications' => function($query){
            $query->valid();
        }])->latest()->paginate(10);

        $notedTalents = User::getLatestNotes(auth()->user(), 3);

        return view('creator.projects.index')
                ->with(compact('projectsCount', 'projects', 'notedTalents'));
    }
}
