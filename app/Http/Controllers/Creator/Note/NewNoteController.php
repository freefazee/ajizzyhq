<?php

namespace App\Http\Controllers\Creator\Note;

use App\User;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class NewNoteController
 *
 * @package \App\Http\Controllers\Creator\Note
 */
class NewNoteController extends BaseController
{

    /**
     * GET /creator/notes/<userId>
     */
    public function show($userId)
    {
        $user = User::findOrFail($userId);

        $note = auth()->user()->createdNotes()->forUser($user)->first();

        return [
            'status' => 'ok',
            'note' => new \App\Http\Resources\Note($note)
        ];
    }


    /**
     * POST /creator/notes/<userId>
     */
    public function store($userId)
    {
        $note  = trim(request('note'));

        if(!$note) return ['status' => 'error', 'message' => 'Note must not be empty'];

        User::findOrFail($userId)
                ->addNoteFrom(auth()->user(), $note);

        return ['status' => 'ok'];

    }

    /**
     * DELETE /creator/notes/<userId>
     */
    public function delete($userId)
    {
        $user = User::findOrFail($userId);

        auth()->user()->createdNotes()->forUser($user)->delete();

        return [
            'status' => 'ok'
        ];
    }
}
