<?php

namespace App\Http\Controllers\Graph;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use Spatie\Tags\Tag;

/**
 * Class SkillsController
 *
 * @package \App\Http\Controllers\Graph
 */
class SkillsController extends Controller
{


    public function index()
    {
        //TODO: this needs to be paginated.
        return TagResource::collection(Tag::get());
    }
}
