<?php

namespace App\Http\Controllers;

use App\Http\Requests\Loginrequest;

class LoginController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function loginProcess(Loginrequest $request)
    {
        if (\Auth::attempt($request->only('email', 'password'), request('remember'), true))
        {
            session()->forget('location');
            if($request->input('modal')) return ['status' => 'ok'];
            return redirect()->intended(route('account.dashboard'));
        }


        if($request->input('modal')) return  ['status' => 'error', 'message' => 'Login Failed. Please check your email/password.'];

        return redirect()
            ->route('login')
            ->with('error', __('ajizzy.unable to login'));
    }
}
