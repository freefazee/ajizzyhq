<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Appearance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'height' => $this->height,
            'weight' => $this->weight,
            'height_unit_us' => intval($this->height_unit_us) <= 0 ? false : true,
            'weight_unit_us' => intval($this->weight_unit_us) <= 0 ? false: true,
            'body_size_id' => $this->body_size_id,
            'eye_color' => $this->eye_color,
            'hair_color' => $this->hair_color,
            'ethnicities' => $this->user->ethnicities->pluck('id')
        ];
    }
}
