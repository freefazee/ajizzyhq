<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Profile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'stage_name' => $this->stage_name,
            'public_url' => $this->url,
            'age_range' => $this->age_range,
            'gender' => $this->gender,
            'title' => $this->title,
            'is_transgender' =>  $this->is_transgender,
            'medias' => Media::collection($this->medias),
            'user' => new User($this->user),
            'place' => $this->place,
            'driver_license' => $this->driver_license ? true : false,
            'passport' => $this->passport ? true : false

        ];
    }
}
