<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationTalent extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|object
     */
    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'project_id' => $this->laravel_through_key,
                'basin' => $this->basin,
                'profile' => new Profile($this->owner->profile),
              //  'medias' => ApplicationMedia::collection($this->medias)
        ];

    }
}
