<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PublicAppearance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'height_unit_us' => $this->height_unit_us,
            'weight_unit_us' => $this->weight_unit_us,
            'height' => $this->height,
            'weight' => $this->weight,
            'body_size' => $this->bodySize ? $this->bodySize->name : 'Unknown',
            'eye_color' => $this->eye ? $this->eye->name : 'Unknown',
            'hair_color' => $this->hair ? $this->hair->name : 'Unknown',
            'ethnicities' => Ethnicity::collection($this->user->ethnicities)
        ];
    }
}
