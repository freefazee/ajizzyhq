<?php

namespace App\Http\Middleware;

use App\Ajizzy\LocationManager;
use App\Country;
use Closure;

class LocationInterceptor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $location = session('location');

        if(!$location)
        {
            $location = LocationManager::getUserLocation();
            session()->put('location', $location);
        }

        return $next($request);
    }
}
