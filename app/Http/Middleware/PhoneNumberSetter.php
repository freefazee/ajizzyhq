<?php

namespace App\Http\Middleware;

use App\Country;
use Closure;

class PhoneNumberSetter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If we detect country code e.g NG or US in the request
        // fetch the data and set it into the request
        if($request->input('country'))
        {
            $country = Country::where('cca2', $request->input('country'))->first();
            if(!$country) {
                abort(403, 'Invalid country code');
            }

            $request->merge(['db_country' => $country]);

        }
        return $next($request);
    }
}
