<?php

namespace App\Http\Middleware;

use Closure;

class AccountVerificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()) return redirect('/login');

       if(!$request->user()->email_verified_at) return redirect('/verify');

        return $next($request);
    }
}
