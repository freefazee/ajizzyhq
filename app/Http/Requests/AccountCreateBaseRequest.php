<?php

namespace App\Http\Requests;

use App\Ajizzy\Helpers;
use App\BannedName;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AccountCreateBaseRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required',
                'max:150',
                new \App\Rules\BannedName
            ],
            'last_name' => [
                'required',
                'max:150',
                new \App\Rules\BannedName,
            ],
            'email' => [
                'required',
                'email',
                new \App\Rules\BannedName,
                'unique:users',
            ],
            'phone' => [
                'required',
                function($attribute, $value, $fail) {
                    $old = $value;
                    $value = formatPhone($value, $this->country);
                    if(!$value) {
                        $fail(__('ajizzy.phone not valid'));
                    }
                    if(User::where('phone', $value)->first())
                    {
                        $fail(__('ajizzy.phone already taken'));
                    }
                }
            ],
            'country' => 'required|size:2',
            'password' => 'required|min:5',
            'terms_and_condition' => [
                'required',
                function($attribute, $value, $fail) {
                    if($value !== '1')
                    {
                        $fail(__('ajizzy.accept terms and condition'));
                    }
                }
            ],
        ];
    }

    protected function prepareForValidation()
    {
        return $this->merge(['phone' => Helpers::removeNoneNumericChars($this->phone)]);
    }
}
