<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveAppearanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'height' => 'required|max:50',
            'weight' => 'required|max:50',
            'body_size_id' => 'required|numeric|exists:body_sizes,id',
            'eye_color' => 'required|numeric|exists:eye_colors,id',
            'hair_color' => 'required|numeric|exists:hair_colors,id',
            'height_unit_us' => 'required|boolean',
            'weight_unit_us' => 'required|boolean',
        ];
    }
}
