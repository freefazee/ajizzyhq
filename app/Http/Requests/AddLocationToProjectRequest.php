<?php

namespace App\Http\Requests;

use App\Country;
use Illuminate\Foundation\Http\FormRequest;

class AddLocationToProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => [
                'required',
                function ($attribute, $value, $fail) {
                    if(!Country::find($value))
                    {
                        $fail($attribute.' is invalid.');
                    }
                },
            ]
        ];
    }
}
