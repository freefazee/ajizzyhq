<?php

namespace App\Http\Requests;

use App\Rules\BannedName;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'stage_name' => 'required|max:255',
            'dob' => 'required|min:1|max:31',
            'yob' => sprintf('required|numeric|min:%d|max:%d', date('Y') - (config('ajizzy.min_age') + 99), date('Y') - config('ajizzy.min_age')),
            'mob' => 'required|min:1|max:12',
            'title' => 'required|max:255',
            'is_public' => 'required|numeric:min:0|max:1',
            'gender' => 'min:4',
            'is_transgender' => 'boolean',
            'about_company' => 'required|max:160',
            'driver_license' => 'nullable',
            'passport' => 'nullable'
        ];

        $profile = auth()->user()->profile;
        $rules['profile_url']  = $profile ?
                        ['required',Rule::unique('profiles')->ignore($profile->id), new BannedName ] :
                        ['required', 'unique:profiles', new BannedName];

        return $rules;
    }

    protected function validationData()
    {
        $this->cleanProfileUrl();
        return parent::validationData();
    }

    protected function cleanProfileUrl()
    {
        if($this->request->has('profile_url')){
            $this->merge([
                'profile_url' => str_slug($this->request->get('profile_url'))
            ]);
        }
    }

    public function messages()
    {
        return [
            'about_company.required' => 'about is required',
            'about_company.max' => 'about is too long'
        ];
    }
}
