<?php

namespace App\Http\Requests;


use Carbon\Carbon;

class AddLocationToProjectRequestWithDates extends AddLocationToProjectRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /*audition_note: "ds"
audition_type: "closed"
end_time: "ds"
start_time: "dd*/
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'city' => 'required',
            'locationInfo' => 'required|array',
            'locationInfo.*.audition_type' => 'required|in:open,closed',
            'locationInfo.*.audition_date' => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                    $expires = Carbon::create($value);
                    if($expires->isPast()) {
                        $fail('Pick a date in the future.');
                    }
                },
            ],
            'locationInfo.*.audition_note' => 'max:1000',
            'locationInfo.*.start_time' => 'required',
            'locationInfo.*.end_time' => 'required'

        ];

        return array_merge(parent::rules(), $rules);
    }
}
