<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddRoleToProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'gender' => 'required',
            'role_type_id' => 'required|exists:role_types,id',
            'min_age' => 'required|numeric|min:' . config('ajizzy.min_age'),
            'max_age' => 'required|numeric|min:'. config('ajizzy.min_age') .'|max:100'
        ];
    }
}
