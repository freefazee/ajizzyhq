<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateNewCreatorRequest extends AccountCreateBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = parent::rules();
        //$rules['company'] = 'max:255';

        $rules['account_type'] = [
            'required',
            Rule::in(['creator', 'performer'])
        ];
        $rules['postal_code'] = 'max:255';

        return $rules;
    }
}
