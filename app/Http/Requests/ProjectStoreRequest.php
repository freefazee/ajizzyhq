<?php

namespace App\Http\Requests;

use App\Project;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class ProjectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $mode = $this->input('save');
       //if($mode === '0') return [];
        return [
            'title' => [
                'required',
                'min:3',
                'max:255',
                function($attribute, $value, $fail) {
                    if(strtolower($value) === strtolower(Project::DEFAULT_PROJECT_TITLE)){
                        $fail('The title is not valid.');
                    }
                }
            ],
            'category_id' => [
                'required',
                'numeric'
            ],
            'company' => 'required|max:255',
            'description' => 'required|min:50',
            'union_type' =>  [
                'required',
                'in:Union and Nonunion,Union,Nonunion,N/A'
            ],
            'is_paid' => 'required|numeric|min:0|max:1',
            'compensation_detail' => 'present|max:1000',
            'location' => 'in:local,nationwide,worldwide',
            'expires_at' => [
                'date',
                function($attribute, $value, $fail) {
                    $expires = Carbon::create($value);
                    if($expires->isPast()) {
                        $fail('The expiry date is not valid. Pick a date in the future.');
                    }
                }
            ]
        ];
    }


    public function attributes()
    {
        return [
            'category_id' => 'category',
        ];
    }
}
