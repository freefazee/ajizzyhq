<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class SetNewUserPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => [
                'required',
                function ($attribute, $value, $fail){
                    if(!Hash::check($value, $this->user()->password)) {
                        $fail('Your current password is not correct.');
                    }
                },
            ],
            'password' => [
                'required',
                'min:6',
                function ($attribute, $value, $fail) {
                    if($this->current_password === $value) {
                        $fail('You can not use this password.');
                    }
                },
            ],
            'confirm_password' => [
                'required',
                function ($attribute, $value, $fail) {
                    if($this->password !== $value) {
                        $fail('Password confirmation does not match.');
                    }
                },
            ]
        ];
    }
}
