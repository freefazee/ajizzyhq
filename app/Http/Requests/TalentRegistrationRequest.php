<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TalentRegistrationRequest extends AccountCreateBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['age_verification'] = [
                'required',
                function($attribute, $value, $fail) {
                    if($value !== '1')
                    {
                        $fail(__('ajizzy.age verification'));
                    }
                }
            ];
        $rules['plan'] = 'nullable|uuid|exists:prices,id';

        return $rules;
    }
}
