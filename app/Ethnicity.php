<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ethnicity extends Model
{
    protected $fillable = ['name'];

    public function roles()
    {
        return $this->belongsToMany(ProjectCastingRole::class);
    }


    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
