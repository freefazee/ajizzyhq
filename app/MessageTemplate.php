<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageTemplate extends Model
{
    protected $fillable = [
        'channel',
        'title',
        'message',
    ];

    public static function createTemplate($key, array $data)
    {
        $tp = MessageTemplate::where('key', $key)->first();
        if(!$tp)
        {
            $tp = new MessageTemplate;
            $tp->key = $key;
        }

        $tp->message = json_encode($data);
        $tp->save();
        return $tp;
    }

    public static function fetch($key)
    {
        return static::where('key', $key)->first();
    }

    public function getCleanKey()
    {
        return ucfirst(str_replace('-',' ', $this->message_key));
    }
}
