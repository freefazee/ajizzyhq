<?php

namespace App\Console\Commands;

use App\Country;
use Illuminate\Console\Command;

class CountryLoader extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'country:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load country info from csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $filePath  = storage_path('countries.json');
        $countries = json_decode(file_get_contents($filePath), true);

        Country::truncate();

        foreach($countries as $country)
        {
            if($country['region'] == 'Africa' || $country['cca2'] == 'US') {
                $data = [
                    'name' => $country['name']['common'],
                    'official' => $country['name']['official'],
                    'cca2' => $country['cca2'],
                    'ccn3' => $country['ccn3'],
                    'cca3' => $country['cca3'],
                    'currencies' => json_encode($country['currencies']),
                    'capital' => isset($country['capital'][0]) ? $country['capital'][0] : '',
                    'region' => $country['region'],
                    'subregion' => $country['subregion'],
                    'demonym' => $country['demonym'],
                    'borders' => json_encode($country['borders'])
                ];

                Country::create($data);
            }


        }

        // Update country phone code
        /*$filePath  = storage_path('country-code.txt');
        $records = explode(PHP_EOL, file_get_contents($filePath));
        $notFound = [];
        foreach($records as $record)
        {
            $data = explode(",", $record);
            if(count($data) === 2)
            {
                    $code = trim($data[0]);
                    $name = trim($data[1]);
                    $country = Country::where('name', $name)
                                        ->orWhere('official', $name)
                                        ->first();
                    if($country)
                    {
                        // update the code
                        $country->update(['phone_prefix' => $code]);
                    }
                    else {
                        $notFound[] = $name;
                    }
            }
        }*/

    }
}
