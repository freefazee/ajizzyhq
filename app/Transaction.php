<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Transaction extends Model
{
    use Uuids;

    public $incrementing = false;

    public const STATUS_SUCCESSFUL = 'successful';
    public const STATUS_PENDING = 'pending';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_FAILED = 'failed';
    public const PAYING_FOR_SUBSCRIPTION  = 'subscription';
    public const PAYING_FOR_COMMUNICATION =  'communication';
    public const PAYING_FOR_ADS = 'ads';

    protected $guarded = ['id', 'user_id'];


    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getFormattedAmountAttribute($value)
    {
        return $this->currency . number_format($this->amount / 100, 2);
    }

    public static function createTransaction(array $data, User $user)
    {
        $data['txf'] = Uuid::generate()->string;
        $data['status'] = static::STATUS_PENDING;

        return $user->transactions()->create($data);
    }

    public static function findByTxf($txf)
    {
        return static::where('txf', $txf)->first();
    }

    public function markAsSuccessful($message='successful')
    {
        return $this->update(['status' => static::STATUS_SUCCESSFUL, 'updated_at' => now(), 'api_response' => $message]);
    }

    public function markAsFailed($message='failed')
    {
        return $this->update(['status' => static::STATUS_FAILED, 'updated_at' => now(), 'api_response' => $message]);
    }
}
