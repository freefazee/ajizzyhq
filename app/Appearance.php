<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appearance extends Model
{
    protected $guarded = ['user_id', 'id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function newOrUpdateForUser(User $user, $data)
    {
        $ethnicities =  null;
        if(isset($data['ethnicities']) && is_array($data['ethnicities']))
        {
            $user->ethnicities()->sync($data['ethnicities']);
            unset($data['ethnicities']);
        }

        $appearance = $user->appearance;
        if($appearance)
        {
            $appearance->update($data);
        } else {
            $user->appearance()->create($data);
        }

        return $appearance;
    }

    public function bodySize()
    {
        return $this->belongsTo(BodySize::class, 'body_size_id');
    }


    public function eye()
    {
        return $this->belongsTo(EyeColor::class, 'eye_colors');
    }

    public function hair()
    {
        return $this->belongsTo(HairColor::class, 'hair_color');
    }

}
