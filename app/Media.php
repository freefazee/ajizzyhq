<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $guarded = ['id'];

    protected $hidden = ['name', 'mediable_id', 'mediable_type', 'created_at', 'updated_at'];

    public const MEDIA_TYPE_IMAGE = 'image';
    public const MEDIA_TYPE_VIDEO = 'video';
    public const MEDIA_TYPE_AUDIO = 'audio';
    public const MEDIA_TYPE_DOCUMENT = 'document';

    public const STATUS_NEW = 0;
    public const STATUS_PUBLISHED = 1;
    public const STATUS_PROCESSING = 2;
    public const STATUS_ERROR_PROCESSING = 3;
    public const STATUS_FLAGGED = 4;

    /**
     * Get the owning mediable model.
     */
    public function mediable()
    {
        return $this->morphTo();
    }
}
