<?php

use Illuminate\Support\Facades\Log;

/**
 * Format phone number to international format
 * @param $phone
 * @param $countryCode
 *
 * @return bool|string
 */
function formatPhone($phone, $countryCode)
{

    $phone = preg_replace("/[^0-9]/", "", $phone);

    $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

    try{
        $phoneObj = $phoneUtil->parse($phone, $countryCode);

        $isValid = $phoneUtil->isValidNumber($phoneObj);
       if($isValid) {
           return $phoneUtil->format($phoneObj, \libphonenumber\PhoneNumberFormat::E164);
       }

    }catch (\libphonenumber\NumberParseException $e) {
        Log::error('INVALID_PHONE: '  . $phone . ': ' .  e($e->getMessage()));
        return false;
    }

    return false;
}
