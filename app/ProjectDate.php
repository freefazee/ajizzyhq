<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProjectDate extends Model
{
    public $incrementing = true;

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function country() {
        return $this->belongsTo(Country::class, 'city', 'id');
    }

    public function setAuditionDateAttribute($value)
    {
        if($value)
        {

            $auditionDate = strtotime($value);

            $this->attributes['audition_date'] = date('Y-m-d', $auditionDate);
        }

    }

    public function setStartTimeAttribute($value)
    {
        if($value)
        {
            $auditionStartTime = strtotime($value);

            $this->attributes['start_time'] = date('H:i:s', $auditionStartTime);
        }

    }

    public function setEndTimeAttribute($value)
    {
       if($value)
       {
           $auditionEndTime = strtotime($value);

           $this->attributes['end_time'] = date('H:i:s', $auditionEndTime);
       }
    }
}
