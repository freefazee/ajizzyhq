<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRange extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at', 'range'];


    public static function getByRange($rangeNumber)
    {
        $ranges = static::get();
        foreach($ranges as $pRange)
        {
            $rangeData = explode('-', $pRange->range);
            if(count($rangeData) === 2)
            {
                if($rangeNumber >= $rangeData[0] && $rangeNumber <= $rangeData[1])
                {
                    return $pRange;
                }
            }
        }

        return false;
    }

    private function rangeParts()
    {
        return explode('-', $this->range);
    }

    public function getLeftAttribute($key)
    {
        return $this->rangeParts()[0];
    }

    public function getRightAttribute($key)
    {
        return $this->rangeParts()[1];
    }

    public function getDiscountAttribute($value)
    {
        return intval($value);
    }
}
