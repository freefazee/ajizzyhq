<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionInvite extends Model
{

    protected $fillable = ['message', 'acceptance_message', 'invitee_id', 'project_id'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner() {
        return $this->belongsTo(User::class, 'invited_by');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invitee() {
        return $this->belongsTo(User::class, 'invitee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

}
