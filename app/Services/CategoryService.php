<?php

namespace App\Services;

use App\Category;

/**
 * Class CategoryService
 *
 * @package \App\services
 */
class CategoryService
{

    public function getActiveCategories()
    {
        return Category::select('id', 'name')
                ->parentCategory()->with(['children' => function($q){
            $q->select('id', 'name', 'parent_id');
        }])->orderBy('sort')->get();

    }
}
