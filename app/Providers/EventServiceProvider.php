<?php

namespace App\Providers;

use App\Events\SendMessage;
use App\Listeners\OnSendMessage;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'App\Events\OnRegistered' => [
            'App\Listeners\SendEmailVerificationNotification',
        ],

        'App\Events\SendMessage' => [
            'App\Listeners\OnSendMessage',
        ],

        'App\Events\OnPasswordResetRequest' => [
            'App\Listeners\HandlePasswordReset',
        ],

        'App\Events\OnInviteSent' => [
            'App\Listeners\HandleSentInvite',
        ],



        'App\Events\GenerateApplicationDownloadChunk' => [
            'App\Listeners\HandleApplicationDownloadChunk',
        ],

        'App\Events\ApplicationDownload' => [
            'App\Listeners\HandleApplicationDownloadRequest',
        ],


        'App\Events\SubscriptionPaymentReceipt' => [
            'App\Listeners\HandleSubscriptionPayment',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
