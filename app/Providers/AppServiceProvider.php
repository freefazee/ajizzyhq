<?php

namespace App\Providers;

use App\Ajizzy\Country;
use App\Observers\ProjectObserver;
use App\Project;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Project::observe(ProjectObserver::class);

        // Shared view data
        try {
            $countries = Country::getCountries();
        }catch (\Exception $e) {
            $countries = [];
        }

        if(!$countries) $countries = '';

        View::share('countries', $countries);

        $this->app->instance(\App\Ajizzy\Contracts\PaymentVerificationContract::class, new \App\Ajizzy\Billing\RavePaymentVerification());
    }
}
