<?php

namespace App\EloquentFilters\Project;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\IFilter as Filter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductionTypeFilter
 *
 * @package \App\EloquentFilters\Project
 */
class UnionTypeFilter implements Filter
{

    public function apply(Builder $builder, $value): Builder
    {
        return $builder->where('union_type', $value);
    }
}
