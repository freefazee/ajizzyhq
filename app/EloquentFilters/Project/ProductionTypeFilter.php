<?php

namespace App\EloquentFilters\Project;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\IFilter as Filter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductionTypeFilter
 *
 * @package \App\EloquentFilters\Project
 */
class ProductionTypeFilter implements Filter
{

    public function apply(Builder $builder, $value): Builder
    {
        if($value !== 'all')
            return $builder->where('category_id', $value);
        else
            return $builder;
    }
}
