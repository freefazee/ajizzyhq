<?php

namespace App\EloquentFilters\Project;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\IFilter as Filter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class RoleGenderFilter
 *
 * @package \App\EloquentFilters\Project
 */
class AgeFilter implements Filter
{
    public function apply(Builder $builder, $value): Builder
    {
        return $builder->whereHas('roles', function($query) use ($value){
            $query->where('age', '>=',  16)->whereNotNull('terms');
        });
    }

}
