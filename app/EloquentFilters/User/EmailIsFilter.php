<?php

namespace App\EloquentFilters\User;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\IFilter as Filter;
use Illuminate\Database\Eloquent\Builder;
/**
 * Class EmailIs
 *
 * @package \App\EloquentFilters\User
 */
class EmailIsFilter implements Filter
{
    /**
     * Apply email_is filter
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param                                       $value
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Builder $builder, $value): Builder
    {
        return $builder->where('email', 'LIKE' , '%' . $value . '%');
    }
}
