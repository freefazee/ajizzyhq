<?php

namespace App\Listeners;

use App\Events\SubscriptionPaymentReceipt;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HandleSubscriptionPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionPaymentReceipt  $event
     * @return void
     */
    public function handle(SubscriptionPaymentReceipt $event)
    {

        //echo 'Transaction by ' . $event->transaction->owner->email .PHP_EOL;
    }
}
