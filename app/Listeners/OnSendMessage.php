<?php

namespace App\Listeners;

use App\Events\SendMessage;
use App\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class OnSendMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SendMessage $event
     * @return bool|void
     */
    public function handle(SendMessage $event)
    {
        if ($event->message->status !== Message::STATUS_NEW) {
            return;
        }

        if ($event->message->message_type === 'sms') {
            $this->sendSMSMessage($event->message);
        }
        else if ($event->message->message_type === 'email') {
            $this->sendEmailMessage($event->message);
        }
        else if ($event->message->message_type === 'both') {
            $this->sendEmailMessage($event->message);
            $this->sendSMSMessage($event->message);
        }

        $event->message->status = Message::STATUS_SENT;
        $event->message->save();
    }

    protected function sendEmailMessage($message)
    {
        try {
            Mail::raw($message->message, function ($msg) use ($message) {
                $msg->to($message->user->email)
                    ->subject($message->subject);
            });
        }
        catch (\Exception $exception) {
            echo "SEND MAIL ERROR: " . $exception->getMessage();

            return false;
        }

        return true;
    }

    protected function sendSMSMessage($message)
    {

        try {
           // sendSMS($message->user->phone, $message->message);
        }
        catch (\Exception $exception) {
            echo "SEND SMS ERROR: " . $exception->getMessage();

            return false;
        }

        return true;
    }
}
