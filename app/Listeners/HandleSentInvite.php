<?php

namespace App\Listeners;

use App\Events\OnInviteSent;
use App\Mail\InviteTalent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class HandleSentInvite implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnInviteSent  $event
     * @return void
     */
    public function handle(OnInviteSent $event)
    {
        // $event->invite
        $invite = $event->invite;
        Mail::to($invite->invitee)->send(new InviteTalent($invite));

    }
}
