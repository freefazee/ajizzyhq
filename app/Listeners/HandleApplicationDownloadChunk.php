<?php

namespace App\Listeners;

use App\Events\GenerateApplicationDownloadChunk;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class HandleApplicationDownloadChunk implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateApplicationDownloadChunk  $event
     * @return void
     */
    public function handle(GenerateApplicationDownloadChunk $event)
    {

        // lets make the csv
        // stage name | email | phone | age | role | date applied |

        $path = $event->project->getDownloadPath();
        if($path) {

            Log::info('Making CSV  for '  . e($event->project->title));

            $fp = fopen($path, 'a');
            if(!$fp) {
                Log::error('Error opening ' . $path);
                return;
            }

            foreach($event->chunk as $application) {

                $line = [
                    $application->owner->profile->stage_name,
                    $application->owner->email,
                    $application->owner->phone,
                    $application->owner->profile->age_range,
                    $application->role->name,
                    $application->formatted_created,
                    route('public.profile', $application->owner->id)
                ];

                fputcsv($fp, $line);
            }

            fclose($fp);

        } else {
            Log::error('Unable to get download path: ' . e($path));
        }




    }


}
