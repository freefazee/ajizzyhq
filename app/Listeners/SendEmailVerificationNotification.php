<?php

namespace App\Listeners;

use App\Events\OnRegistered;
use App\Mail\WelcomeNewUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailVerificationNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnRegistered  $event
     * @return void
     */
    public function handle(OnRegistered $event)
    {
        $user = $event->user->generateEmailVerification();
        Mail::to($user->email)->send(new WelcomeNewUser($user));
    }
}
