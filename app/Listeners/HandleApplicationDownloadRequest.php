<?php

namespace App\Listeners;

use App\Events\ApplicationDownload;
use App\Mail\ApplicationDownloadMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HandleApplicationDownloadRequest implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationDownload  $event
     * @return void
     */
    public function handle(ApplicationDownload $event)
    {
            Mail::to($event->user->email)->send(new ApplicationDownloadMail($event->project));
            Log::info('Email sent to ' . $event->user->email);
    }
}
