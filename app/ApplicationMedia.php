<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationMedia extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $guarded = ['id'];

    public function mediaType()
    {
        return $this->belongsTo(MediaType::class, 'media_type_id');
    }
}
