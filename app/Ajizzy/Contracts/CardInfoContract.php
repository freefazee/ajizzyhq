<?php

namespace App\Ajizzy\Contracts;

interface CardInfoContract
{
    public function getExpireMonth(): string ;
    public function getExpireYear(): string;
    public function getNumber(): string;
    public function getToken(): string;
    public function getIssuingCountry(): string;
    public function getType(): string;
    public function getBrand(): string;
    public function getName(): string ;
}
