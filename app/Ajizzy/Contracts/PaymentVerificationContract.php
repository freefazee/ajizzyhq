<?php

namespace App\Ajizzy\Contracts;

/**
 * Class PaymentVerificationContract
 *
 * @package \App\Ajizzy\Contracts
 */
interface PaymentVerificationContract
{

    public function isSuccessful() : bool;

    /**
     * @param $amount - amount to check
     * @param $txf  - transaction reference
     *
     * @return mixed
     */
    public function verify(int $amount, string $txf);

    public function message() : string ;

    public function card(): CardInfoContract;
}
