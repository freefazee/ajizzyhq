<?php

namespace App\Ajizzy\Billing;

use App\Ajizzy\Contracts\CardInfoContract;

/**
 * Class FakeCard
 *
 * @package \App\Ajizzy\Billing
 */
class FakeCard implements CardInfoContract
{
    private $cardInfo;

    public function __construct($cardInfo)
    {
        $this->cardInfo = $cardInfo;
    }

    public function getExpireMonth(): string
    {
        return '01';
    }

    public function getExpireYear(): string
    {
        return '2020';
    }

    public function getNumber(): string
    {
        return '1234567890981';
    }

    public function getToken(): string
    {
        return 'test-token';
    }

    public function getIssuingCountry(): string
    {
        return 'NG';
    }

    public function getType(): string
    {
        return 'Visa';
    }

    public function getBrand(): string
    {
        return 'Ajizzy Fake card';
    }

    public function getName(): string
    {
         return 'fake';
    }
}
