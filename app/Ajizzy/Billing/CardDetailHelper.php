<?php

namespace App\Ajizzy\Billing;

use App\Ajizzy\Contracts\CardInfoContract;
use App\Ajizzy\Country;
use App\User;

/**
 * Class CardDetailHelper
 *
 * @package \App\Ajizzy\Billing
 */
class CardDetailHelper
{
    private $card;

    public function __construct(CardInfoContract $card)
    {
        $this->card  = $card;
    }

    public function store(User $user)
    {
        $card = $user->cards()->where('name', $this->card->getName())
                        ->where('num', $this->card->getNumber())->first();

        if($card) return $card;

        $country = Country::getByCode($this->card->getIssuingCountry());

        return $user->cards()->create([
            'num' => $this->card->getNumber(),
            'exp_month' => $this->card->getExpireMonth(),
            'exp_year' => $this->card->getExpireYear(),
            'brand' =>  $this->card->getBrand(),
            'country' => $country ? $country->id : $this->card->getIssuingCountry(),
            'token' => $this->card->getToken(),
            'type' => $this->card->getType(),
            'name' => $this->card->getName(),
        ]);
    }
}
