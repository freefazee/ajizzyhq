<?php

namespace App\Ajizzy\Billing;

use App\Ajizzy\Contracts\CardInfoContract;
use App\Ajizzy\Contracts\PaymentVerificationContract;

/**
 * Class FakePaymentVerification
 *
 * @package \App\Ajizzy\Billing
 */
class FakePaymentVerification implements PaymentVerificationContract
{
    private $success = false;

    private $cardInfo; // details about the card

    public function isSuccessful(): bool
    {
        return $this->success;
    }

    public function verify(int $amount, string $txf)
    {
        $this->success = true;
    }

    public function message(): string
    {
       return 'fake payment gateway';
    }

    public function card(): CardInfoContract
    {
        return new FakeCard($this->cardInfo);
    }
}
