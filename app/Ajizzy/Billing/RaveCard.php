<?php

namespace App\Ajizzy\Billing;

use App\Ajizzy\Contracts\CardInfoContract;

/**
 * Class RaveCardDetails
 *
 * @package \App\Ajizzy\Billing
 */
class RaveCard implements CardInfoContract
{
    private $cardInfo;

    public function __construct($cardInfo)
    {
        $this->cardInfo = $cardInfo;
    }

    public function getExpireMonth(): string
    {
        return  $this->cardInfo->expirymonth;
    }

    public function getExpireYear(): string
    {
        return  $this->cardInfo->expiryyear;
    }

    public function getNumber(): string
    {
        return  $this->cardInfo->cardBIN . '*********' . $this->cardInfo->last4digits;
    }

    public function getToken(): string
    {
        return  $this->cardInfo->card_tokens[0]->embedtoken;
    }

    public function getIssuingCountry(): string
    {
        $parts = preg_split('/\s+/', $this->cardInfo->issuing_country);
        return array_pop($parts);
    }

    public function getType(): string
    {
        return  $this->cardInfo->type;
    }

    public function getBrand(): string
    {
        return  $this->cardInfo->brand;
    }

    public function getName(): string
    {
        return 'rave';
    }
}
