<?php

namespace App\Ajizzy\Billing;

use App\Ajizzy\Contracts\CardInfoContract;
use GuzzleHttp\Client;
use App\Ajizzy\Contracts\PaymentVerificationContract;
use Illuminate\Support\Facades\Log;

/**
 * Class RavePaymentVerification
 *
 * @package \App\Ajizzy\Billing
 */
class RavePaymentVerification implements PaymentVerificationContract
{
    private $success = false;

    private $messageResponse = '';

    private $cardInfo; // details about the card


    public function isSuccessful(): bool
    {
        return $this->success;
    }

    public function verify(int $amount, string $txf)
    {
        $client = new Client();

        $url = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify';
        $data = array('txref' => $txf,'SECKEY' => env('RAVE_SECRET_API_KEY') );

        $response = null;
       try
       {
           $response = $client->request('POST', $url, ['json' => $data]);
       }
       catch (\Exception $e)
       {
          $this->messageResponse = 'Error making api request';
          return;

       }


        $body = \GuzzleHttp\json_decode($response->getBody());

       $this->cardInfo = $body->data->card;

        if ($body->data->status === "successful" && $body->data->chargecode === "00") {
            // the $amount / 100 is because the amount was initially multiplied by 100
            $amountPaid = $body->data->amount;
            $transAmount = ($amount / 100);
            Log::info('PAYMENT INFO: PAID: ' . $amountPaid . ' ORIGINAL AMOUNT: ' . $transAmount);
            if ($amountPaid >= $transAmount ) {
                $this->success = true;
                $this->messageResponse = $body->data->vbvmessage;
            } else {
                $this->messageResponse = 'The amount charged is less than the transaction amount. In a situation like this, your transaction must be reviewed manually by our billing department. Amount: ' . number_format($body->data->amount,2) . ' REF: ' . $txf;
                Log::error('PAYMENT-MISMATCH-ERROR: ' . $this->messageResponse);
            }
        } else {
            $this->messageResponse = $body->data->vbvmessage;
            Log::error('PAYMENT ERROR: ' . $this->messageResponse);
        }
        //return json_decode($response->getBody(), true);
    }

    public function message(): string
    {
        return $this->messageResponse;
    }

    public function card(): CardInfoContract
    {
        return new RaveCard($this->cardInfo);
    }
}
