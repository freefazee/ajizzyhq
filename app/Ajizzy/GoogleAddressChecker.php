<?php

namespace App\Ajizzy;

/**
 * Class GoogleAddressChecker
 *
 * @package \App\Ajizzy
 */
class GoogleAddressChecker
{

    /**
     * Get google address using place id
     *
     * @param $placeId - mostly returned by the google auto complete api or a previous call
     *
     * @return bool
     */
    public static function getAddressById($placeId)
    {

        $req = \GoogleMaps::load('geocoding')
            ->setParam ([
                'place_id' => $placeId,
            ])->get();

        $response = json_decode($req);

        if($response->status!== 'ok')
        {

            return $response->results[0];
        }
        return false;

    }

    /**
     * Check if we have a place stored already
     *
     * @param $placeId
     *
     * @return mixed
     */
    public static function placeExistsInDb($placeId)
    {
        return \App\Country::where('external_id', $placeId)->first();
    }

    public static function getInfo($placeId)
    {
        $place = static::getAddressById($placeId);


        if($location = static::placeExistsInDb($place->place_id)) return $location;

        $addressType = $place->types[0];


        if($addressType === 'country') return false; // we do not want to store a country

        $addressComponents = static::extractAddress($place->address_components); // extract address component

        // May the good lord help me here. This section of code is due to the complexity of google geo location
        // TODO: Move this to a helper method
        $defaultAddressType = 'country';
        if(isset($addressComponents['route']))
            $defaultAddressType = 'route';
        elseif (isset($addressComponents['neighborhood']))
            $defaultAddressType = 'neighborhood';
        elseif (isset($addressComponents['locality']))
            $defaultAddressType = 'locality';
        elseif (isset($addressComponents['administrative_area_level_2']))
            $defaultAddressType = 'administrative_area_level_2';
        elseif (isset($addressComponents['administrative_area_level_1']))
            $defaultAddressType = 'administrative_area_level_1';


        $locationDetail = isset($addressComponents[$addressType]) ? $addressComponents[$addressType] :  $addressComponents[$defaultAddressType];

        $country = Country::getByCode($addressComponents['country']->short_name);
        // we can only store the location if we found the country in db
        if($country && $locationDetail)
        {
            $insertData = [
                'name' => $locationDetail->long_name,
                'official' =>  $place->formatted_address,
                'cca2' => $locationDetail->short_name,
                'cca3' => '',
                'external_id' => $place->place_id,
                'parent_id' => $country->id,
                'types' => $addressType,
                'lat' =>  $place->geometry->location->lat,
                'lng' => $place->geometry->location->lng
            ];

           return  Country::firstOrCreate($insertData);
        }


        return false;

        // we need to country id to create reference
    }


    /**
     * Extract Google address info
     * @param array $addressComponents
     *
     * @return array
     */
    public static function extractAddress( $addressComponents)
    {
        if(!$addressComponents || !is_array($addressComponents)) return [];
        $output = [];



        foreach($addressComponents as $addressComponent)
        {
            switch($addressComponent->types[0]) {
                case 'country':
                    $output['country'] = $addressComponent;
                    break;
                case 'administrative_area_level_1':
                    $output['administrative_area_level_1'] = $addressComponent;
                    break;
                case 'administrative_area_level_2':
                    $output['administrative_area_level_2'] = $addressComponent;
                    break;
                case 'locality':
                    $output['locality'] = $addressComponent;
                    break;
                case 'political':
                    $output['political'] = $addressComponent;
                    break;
                case 'route':
                    $output['route'] = $addressComponent;
                    break;
                case 'neighborhood':
                    $output['neighborhood'] = $addressComponent;
                    break;
                case 'establishment':
                    $output['establishment'] = $addressComponent;
                    break;
            }
        }

        return $output;
    }
}
