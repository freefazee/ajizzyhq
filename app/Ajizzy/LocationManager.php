<?php

namespace App\Ajizzy;

use Illuminate\Support\Facades\Log;

/**
 * Class LocationManager
 *
 * @package \App\Ajizzy
 */
class LocationManager
{

    public static function getUserLocation($user=null)
    {
         $user = $user ? $user : request()->user();

        if(!$user)
        {
            // user is not logged in
            try {
                return Country::getByCode(geoip()->getLocation(request()->ip())->iso_code);
            } catch (\Exception $e)
            {
                Log::error($e->getMessage());
                return Country::getByCode(env('DEFAULT_COUNTRY'));
            }
        }
        else
        {
            return Country::getByCode($user->country->cca2);
        }



    }


}
