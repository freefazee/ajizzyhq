<?php

namespace App\Ajizzy;

/**
 * Class Helpers
 *
 * @package \App\Ajizzy
 */
class Helpers
{

    /**
     * remove non-numeric characters from a string
     * @param $str
     * @return string
     */
    public static function removeNoneNumericChars($str) : string
    {
        return preg_replace('/\D/', '', $str);
    }

}
