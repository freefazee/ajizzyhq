<?php

namespace App\Ajizzy;

use App\Category as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Category
 *
 * @package \App\Ajizzy
 */
class Category
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get categories with children
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public static function getCategoryWithChildren(): ?Collection
    {
        return Model::select('id', 'name')
            ->parentCategory()
            ->enabled()
            ->with(['children' => function($query)
            {
                 $query->orderBy('sort');
            }])
            ->orderBy('sort')->get();
    }

    /**
     * This method takes a category with children and fetch one latest project for each child.
     * Returns null if no project is found for a child
     *
     * @param $parent
     *
     * @return \stdClass|null
     */
    public static function getOneLatestProjectForEachChild($parent)
    {
        $result = new \stdClass();
        $result->id = $parent->id;
        $result->name = $parent->name;

        $result->children = [];
       foreach ($parent->children as $child)
       {
            $latestProject = Project::getOneLatestByCategoryId($child->id);
            if($latestProject)
            {
                $child->project = $latestProject;
                $result->children[] = $child;
            }
       }
       if(!$result->children) return null;


       return $result;
    }

    public static function getOneLatestProjectForAllChild()
    {
        $categories = static::getCategoryWithChildren();
        $output = [];
        foreach($categories as $category)
        {
            $projects = static::getOneLatestProjectForEachChild($category);
            if($projects)
                $output[] = $projects;
        }

        return collect($output);
    }
}
