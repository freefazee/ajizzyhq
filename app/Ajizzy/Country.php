<?php

namespace App\Ajizzy;

use App\Country as Model;

/**
 * Class Country
 *
 * @package \App\Ajizzy
 */
class Country
{

    //TODO: cache data here
    public static function getCountries()
    {
        return Model::getCountries();
    }

    public static function getByCode($countryCode)
    {
        $result = Model::select('id', 'name', 'cca2 as code', 'currencies')
                        ->where('cca2', $countryCode)
                        ->first();

        if(!$result)
            $result = Model::select('id', 'name', 'cca2 as code', 'currencies')
                ->orWhere('cca2', env('DEFAULT_COUNTRY'))
                ->first();

        if($result)
        {
            $result->currency = $result->getCurrency();
            unset($result->currencies);
        }



        return $result;

    }

    public static function saveLocationsFromAddressComponent($components)
    {
        if(!$components) return false;

        $country = static::getByCode($components['country']->short_name);

        if(!$country) return false;

        // do we have state


    }

    private function saveState($addressComponent) : int
    {
        if(isset($addressComponent['state']) && $addressComponent['state'])
        {
            $state = Model::where('location_type', 'state')
                        ->where('name', $addressComponent['state']->long_name)
                        ->where('cca2', $addressComponent['state']->short_name)
                        ->first();

            if($state) return $state->id;

            $state = Model::create([
                'location_type' => 'state',
                'name' => $addressComponent['state']->long_name,
                'cca2' => $addressComponent['state']->short_name,
                'cca3' => ''
            ]);

            return $state->id;
        }

        return false;
    }

    public static function create(array $data)
    {
        return Model::create($data);
    }

    public static function firstOrCreate($data)
    {
        return Model::firstOrCreate($data);
    }
}
