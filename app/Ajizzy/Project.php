<?php

namespace App\Ajizzy;
use App\Project as Model;
use App\User;
use EloquentBuilder;

/**
 * Class Project
 *
 * @package \App\Ajizzy
 */
class Project
{
    private static $FIELDS = [
            'id', 'title', 'category_id'
    ];

    /**
     * Get one latest project based on the category ID
     *
     * @param int $categoryId
     *
     * @return \App\Project|null
     */
    public static function getOneLatestByCategoryId(int $categoryId) : ?Model
    {
        return Model::select(static::$FIELDS)
                ->published()
                ->latest()
                ->category($categoryId)
                ->first();
    }


    public static function getOneProjectOrFail($projectId)
    {
        return  Model::select([
                            'id', 'title', 'category_id', 'description', 'company',
                            'is_paid', 'audition_instructions', 'expires_at', 'user_id', 'country_id', 'location'
                      ])
                    ->published()
                    ->with(['roles.roleType', 'category', 'roles.mediaTypes', 'owner' => function($query) {
                        $query->select('users.id', 'first_name');
                    }, 'owner.profile' => function($query) {
                            $query->select('id', 'profiles.user_id', 'company', 'about_company');
                    }, 'country' => function($query) {
                        $query->select('id', 'name', 'official');
                    }])
                    ->findOrFail($projectId);
    }

    public static function findForUserOrFail($id, $user=null)
    {
        if(!$user) $user = auth()->user();

        if(!$user) abort(404);

        return $user->projects()->with(['country' => function($q){
            $q->select('id', 'name', 'cca2 as code');
        }])->findOrFail($id);
    }

    public static function findOneOrFail($id)
    {
        return Model::valid()->findOrFail($id);
    }


    public static function fetchProjects($requestData)
    {
        $query = Model::with(['category', 'roles.roleType', 'roles.mediaTypes', 'roles.ethnicities', 'country' => function($query) {
            $query->select('id', 'name', 'cca2');
        }, 'locations' => function($query) {
            $query->select('countries.id', 'name', 'cca2', 'official', 'parent_id');
        }, 'owner' => function($query) {
            $query->select('users.id', 'first_name');
        }, 'owner.profile' => function($query) {
            $query->select('profiles.id', 'profiles.user_id', 'company', 'about_company');
        }])->orderBy('sponsored', 'desc')->latest()->valid();

        return EloquentBuilder::to($query,$requestData)
            ->paginate(10);
    }

    public static function getLatstProjects($limit)
    {
        $query = Model::with(['category', 'roles.roleType', 'roles.mediaTypes', 'country' => function($query) {
            $query->select('id', 'name', 'cca2');
        }, 'locations' => function($query) {
            $query->select('countries.id', 'name', 'cca2', 'official', 'parent_id');
        }, 'owner' => function($query) {
            $query->select('users.id', 'first_name');
        }, 'owner.profile' => function($query) {
            $query->select('profiles.id', 'profiles.user_id', 'company', 'about_company');
        }])->valid();

        return $query->latest()->take($limit)->get();
    }

    public static function featuredCastings($limit)
    {
        $query = Model::with(['category',  'country' => function($query) {
            $query->select('id', 'name', 'cca2');
        }, 'locations' => function($query) {
            $query->select('countries.id', 'name', 'cca2', 'official', 'parent_id');
        }])->valid();

        return $query->latest()->take($limit)->get();
    }

    public static function getUserProjectCount(User $user)
    {
        return $user->projects()->published()->count();
    }

    public static function getSimilar($project)
    {
       return Model::select('id', 'title', 'created_at', 'category_id')
                    ->where('id', '!=', $project->id)
                    ->with('category')
                    ->valid()
                    ->take(10)
                    ->inRandomOrder()
                    ->get();
    }

}
