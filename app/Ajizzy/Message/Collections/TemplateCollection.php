<?php

namespace App\Ajizzy\Message\Collections;

use App\MessageTemplate as Model;
use App\Ajizzy\Helpers\BaseCollection;
use App\Ajizzy\Message\Template;

class TemplateCollection extends BaseCollection
{

    public static function getAll()
    {
        $collection = new static();

        $models = Model::orderBy('key')->get();

        foreach ($models as $model) {
            $collection->push(new Template($model));
        }

        return $collection;
    }

    public function push(Template $template)
    {
        $this->items->push($template);

        return $this;
    }


}
