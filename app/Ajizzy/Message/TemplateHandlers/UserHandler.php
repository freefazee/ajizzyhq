<?php

namespace App\Ajizzy\Message\TemplateHandlers;

use App\User;

class UserHandler implements HandlerInterface
{

    public static function handle(string $message, ...$params): string
    {
        $user = $params[0];

        $fields = [
            '#USER_NAME#' => $user->name,
            '#USER_PHONE#' => $user->phone,
            '#USER_EMAIL#' => $user->email,
        ];

        return str_replace(array_keys($fields), array_values($fields), $message);
    }

}
