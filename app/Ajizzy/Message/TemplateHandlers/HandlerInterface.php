<?php

namespace App\Ajizzy\Message\TemplateHandlers;

interface HandlerInterface
{

    public static function handle(string $message, ...$params): string;

}
