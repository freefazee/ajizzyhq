<?php

namespace App\Ajizzy\Commands;

/**
 * Class PaymentConfirmation
 *
 * This class check payment gateways for transactions that have not been confirmed.
 * Basically, it calls each payment class verification method.
 *
 * @package \App\Ajizzy\Commands
 */
class PaymentConfirmation
{

    public static function checkTransactions(): bool {

    }

}
