<?php

namespace App\Ajizzy;
use App\Price as Model;

/**
 * Class Price
 *
 * @package \App\Ajizzy
 */
class Price {

    /**
     * Get a price by its name
     * @param $name
     *
     * @return \App\Price
     */
    public static function getByName($name)
    {
        return Model::where('name', $name)->first();
    }

    /**
     * Get a price by country id
     * @param $countryId - int
     *
     * @return \App\Price
     */
    public static function getByCountryId($countryId)
    {
        return Model::where('country_id', $countryId)->first();
    }


    public static function countDefaults()
    {
        return Model::where('is_default', 1)->count();
    }

    public static function makeDefault(Model $price)
    {
        $price->update(['is_default' => true]);

        Model::where('id', '!=', $price->id)->update(['is_default' => false]);

        return $price;

    }

    public static function empty()
    {
        return Model::truncate();
    }

    public static function count()
    {
        return Model::count();
    }

    public static function create(array $data)
    {
        return Model::create($data);
    }

    public static function getPriceForLocation($location)
    {
        if(!$location) $location = Country::getByCode('NG');

        return Model::where('country_id', $location->id)
                    ->with(['country' => function($query) {
                        $query->select('id', 'currencies', 'cca2', 'name');
                    }] )->first();
    }

    public static function getPricingDetail($price, $range)
    {
        return [
            'currency' => $price->country->getCurrency(),
            'total' =>    number_format($price->calculateTotal($range),2),
            'discount' => $range->discount,
            'default' => $range->id,
            'label' => strtolower($range->label)
        ];
    }

}
