<?php

namespace App\Ajizzy;
use App\Media;
use App\Profile as Model;
use App\User;
use EloquentBuilder;

/**
 * Class Project
 *
 * @package \App\Ajizzy
 */
class Profile
{

    public static function fetchAll($requestData, $perPage=10)
    {
        $query = Model::with(['medias' => function($query){
            $query->select('id' ,'url', 'type', 'mime', 'mediable_id')
                    ->where('type', Media::MEDIA_TYPE_IMAGE);
        }, 'user.country:id,name,official,parent_id', 'user:id,country_id,first_name', 'user.appearance'])
            ->select('id', 'stage_name', 'user_id', 'profile_url', 'yob', 'gender', 'title', 'location_id', 'passport', 'driver_license')
            ->whereHas('medias')
            ->public();



        return EloquentBuilder::to($query,$requestData)
            ->paginate($perPage);
    }

    public static function findOneOrFail($userId)
    {
        return User::with(['profile' => function($query){

        }, 'country' => function($query) {

        }, 'profile.location' => function($query){
                $query->select('id', 'parent_id', 'official');
        }])

            ->findOrFail($userId);
    }
}
