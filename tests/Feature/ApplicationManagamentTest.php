<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApplicationManagamentTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_view_applications_page()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $this->get('/account/applications')
                ->assertStatus(200);

    }
}
