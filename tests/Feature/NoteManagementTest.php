<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoteManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_add_a_note_to_a_user()
    {
        $creator = $this->signIn();

        $user = factory('App\User')->create();

        $data = ['note' => 'test'];

        $this->json('POST', '/creator/notes/' . $user->id, $data)
                ->assertJson(['status' => 'ok']);

        $this->assertDatabaseHas('notes', ['body' => 'test', 'creator_id' => $creator->id, 'user_id' => $user->id]);
    }

    /** @test */
    public function get_note_for_a_user()
    {
        $creator = $this->signIn();

        $user = factory('App\User')->create();

        $note = $user->addNoteFrom($creator, 'this is the note');

        $this->json('GET', '/creator/notes/' . $user->id)
            ->assertJson(['status' => 'ok', 'note' => ['id' => $note->id, 'user_id' => $user->id, 'body' => 'this is the note']]);
    }

    /** @test */
    public function can_delete_a_note_from_a_user()
    {
        $creator = $this->signIn();

        $user = factory('App\User')->create();

        $user->addNoteFrom($creator, 'this is the note');

        $this->json('DELETE', '/creator/notes/' . $user->id)
            ->assertJson(['status' => 'ok']);

        $this->assertDatabaseMissing('notes', ['body' => 'this is the note', 'creator_id' => $creator->id, 'user_id' => $user->id]);
    }
}
