<?php

namespace Tests\Feature;


use App\Media;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileMediaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_upload_a_valid_profile_image()
    {

        $profile = factory('App\Profile')->create();
       $this->signIn($profile->user);

        Storage::fake('medias');

        $res = $this->json('POST', '/account/profile/media', [
                'media' =>  $file = UploadedFile::fake()->image('required_media.jpg'),
                'media_type' => Media::MEDIA_TYPE_IMAGE])
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);

      $payload = json_decode($res->getContent());

      Storage::disk('public')->assertExists($payload->filename);

       $this->assertDatabaseHas('media',
            ['name' => $payload->filename, 'mediable_id' => $profile->id]);


        $this->assertEquals(1, $profile->medias()->count());
    }

    /** @test */
    public function can_not_upload_invalid_media_type()
    {

        $this->withoutExceptionHandling();

        $profile = factory('App\Profile')->create();
        $this->signIn($profile->user);


         $this->json('POST', '/account/profile/media', [
            'media_type' => 'invalid'])
           ->assertStatus(422)
            ->assertJson(['status' => 'error']);
    }

    /** @test */
    public function image_is_required_to_upload()
    {

        $profile = factory('App\Profile')->create();
        $this->signIn($profile->user);

        $this->json('POST', '/account/profile/media', [
            'media' =>  $file = UploadedFile::fake()->create('sample.pdf'),
            'media_type' => Media::MEDIA_TYPE_IMAGE])
            ->assertStatus(422)
            ->assertJsonValidationErrors('media');


        $this->assertEquals(0, $profile->medias()->count());
    }
}
