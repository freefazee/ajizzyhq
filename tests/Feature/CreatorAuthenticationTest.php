<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatorAuthenticationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        factory('App\Country')->create(['cca2' => 'KP']);
    }

    /** @test */
    public function creator_can_sign_up()
    {
        $this->withoutExceptionHandling();

        $this->get('/creator/join')
            ->assertStatus(200);

        $country = factory('App\Country')->create(['cca2' => 'NG']);

        $attributes = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'phone' => '8066100671',
            'country' => $country->cca2,
            'title' => $this->faker->jobTitle,
            'company' => $this->faker->company,
            'postal_code' => $this->faker->postcode,
            'password' => 'password123',
            'terms_and_condition' => '1',
            'account_type' => 'creator'
        ];

        $this->post('/creator/join', $attributes)
                ->assertRedirect('/creator/company-profile-update');

        $this->get('/creator/company-profile-update')
                    ->assertStatus(200);
    }
}
