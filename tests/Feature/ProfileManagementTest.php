<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileManagementTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function authenticated_user_can_see_account()
    {
        $this->signIn();
        $this->get('/account')->assertStatus(302);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_account()
    {
        $this->get('/account')
                ->assertStatus(302)
                ->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_update_profile_with_valid_data()
    {
        $user = $this->signIn();

        $attributes  = factory('App\Profile')->raw();
        unset($attributes['user_id']);

        $this->post('/account/api/profile', $attributes, array('HTTP_X-Requested-With' => 'XMLHttpRequest'))
                ->assertStatus(200)
                ->assertJson([
                    'status' => 'ok',
                    'goto' => route('account.profile.avatar')
                ]);


        $attributes['user_id'] = $user->id;
        $this->assertDatabaseHas('profiles', ['title' => $attributes['title'], 'user_id' => $attributes['user_id']]);

    }


    /** @test */
    public function profile_url_must_not_contain_banned_name()
    {

        $user = $this->signIn();

        factory('App\BannedName')->create(['name' => 'admin']);

        $attributes  = factory('App\Profile')->raw(['profile_url' => 'admin']);
        unset($attributes['user_id']);

        $this->post('/account/api/profile', $attributes, array('HTTP_X-Requested-With' => 'XMLHttpRequest'))
            ->assertSessionHasErrors('profile_url');

    }


    /** @test */
    public function user_can_fetch_medias()
    {

        $this->withoutExceptionHandling();


        $profile = factory('App\Profile')->create();

        $this->signIn($profile->user);

        $this->get(route('account.api.show.medias'))
            ->assertJson(['data' => []])
            ->assertStatus(200);


    }

    /** @test */
    public function  authenticated_user_updating_of_profile_requires_stage_name()
    {
        //$this->withoutExceptionHandling();

        $this->signIn();
        $attributes = factory('App\Profile')->raw(['stage_name' => '']);

        $this->json('POST', '/account/api/profile', $attributes)
                    ->assertStatus(422);
                    //->assertJson(['The stage name field is required.']);

    }

    /** @test */
    public function authenticated_user_can_view_profile()
    {
        $this->signIn();
        $this->get('/account/api/profile')
            ->assertStatus(200);
    }

    /** @test */
    public function creator_can_update_company_profile(){

        $this->withoutExceptionHandling();

        $profile = factory('App\Profile')->create();
        $this->signIn($profile->user);

        $location = factory('App\Country')->create();


        $data = ['company' => 'test', 'about_company' => 'about us', 'place_id' => $location->id];


        $this->json('POST', '/creator/company-profile-update', $data)
                    ->assertStatus(200)
                    ->assertJson(['status' => 'ok']);

    }
}
