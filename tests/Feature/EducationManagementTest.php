<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EducationManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_get_all_educations()
    {

       $user =  $this->signIn();

       factory('App\Education')->create(['user_id' => $user->id]);

       $this->get('/account/educations')
            ->assertStatus(200)
            ->assertJson([
                'data' => []
            ]);

    }


    /** @test */
    public function can_create_new_education_with_valid_data()
    {

        $user = $this->signIn();

        $data = factory('App\Education')->raw(['user_id' => $user->id]);

        $this->json('POST', '/account/educations', $data)
            ->assertJson(['status' => 'ok'])
            ->assertStatus(200);

        $this->assertDatabaseHas('education', $data);

    }


    /** @test */
    public function can_update_education_with_valid_data()
    {

        $user = $this->signIn();

        $data = factory('App\Education')->create(['user_id' => $user->id]);

        $this->json('POST', '/account/educations/' . $data->id,  ['school' => 'st joseph'])
            ->assertJson(['status' => 'ok'])
            ->assertStatus(200);

        $this->assertDatabaseHas('education', ['user_id' => $data->user_id, 'school' => 'st joseph']);

    }


    /** @test */
    public function can_delete_education()
    {
        $this->withoutExceptionHandling();

        $user = $this->signIn();

        $data = factory('App\Education')->create(['user_id' => $user->id]);

        $this->json('DELETE', '/account/educations/' . $data->id)
            ->assertJson(['status' => 'ok'])
            ->assertStatus(200);

        $this->assertDatabaseMissing('education', $data->toArray());

    }
}
