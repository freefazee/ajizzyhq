<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectCastingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guest_can_not_access_projects_page()
    {
        $this->get('/creator/projects')
                ->assertStatus(302)
                ->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_access_projects_page()
    {
        $this->signIn();

       $this->get('/creator/projects')->assertStatus(200);
    }
}
