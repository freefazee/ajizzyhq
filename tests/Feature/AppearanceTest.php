<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppearanceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_load_user_appearance()
    {

        $user = $this->signIn();

        $appearance = factory('App\Appearance')->create(['user_id' => $user->id]);

        $this->get(route('account.api.appearance'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'height' => $appearance->height,
                    'weight' => $appearance->weight,
                    'height_unit_us' =>  $appearance->height_unit_us,
                    'weight_unit_us' =>  $appearance->weight_unit_us,
                    'body_size_id' =>  $appearance->body_size_id,
                    'eye_color' =>  $appearance->eye_color,
                    'hair_color' =>  $appearance->hair_color,
                ]
            ]);
    }


    /** @test */
    public function can_load_empty_user_apperance()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $this->get(route('account.api.appearance'))
            ->assertStatus(200)
            ->assertJson(['data' => false]);
    }


    /** @test */
    public function can_save_user_appearance()
    {
        $this->withoutExceptionHandling();

        $user = $this->signIn();
        $payload = [
            'height' => 1,
            'weight' => 1,
            'body_size_id' => factory('App\BodySize')->create()->id,
            'eye_color' => factory('App\EyeColor')->create()->id,
            'hair_color' => factory('App\HairColor')->create()->id,
            'height_unit_us' => true,
            'weight_unit_us' => true
        ];
        $this->json('POST', route('account.api.appearance.store'), $payload)
                ->assertStatus(200)
                ->assertJson([
                    'status' => 'ok',
                    'data' => []
                ]);

        $payload['user_id'] = $user->id;
        $this->assertDatabaseHas('appearances', $payload);

    }


}
