<?php

namespace Tests\Feature;

use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectManagementTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function can_create_a_drafted_project()
    {
        $location = factory('App\Country')->create(['cca2' => 'KP']);

        $project = Project::createDrafted(factory('App\User')->create(), $location);

        $this->assertNotNull($project);

        $this->assertInstanceOf(Project::class, $project);

        $this->assertNotNull($project->country);

        $this->assertEquals(0, $project->locations()->count());

        $this->assertEquals('drafted', $project->status);
    }

    /** @test */
    public function can_update_new_project()
    {
        $user = factory('App\User')->create();
        $this->signIn($user);

        $project = Project::createDrafted($user, factory('App\Country')->create());

        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        $attributes = factory('App\Project')->raw();



        $url = '/creator/projects/' . $project->id;

      $this->json('POST', $url, $attributes)
                ->assertStatus(200)
                ->assertJson(['status' => 'ok']);

    }


    /** @test */
    public function do_not_save_invalid_project_details()
    {
        $user = factory('App\User')->create();
        $this->signIn($user);

        $project = Project::createDrafted($user, factory('App\Country')->create());
        $attributes = factory('App\Project')->raw([
            'title' => '',
            'description' => '',
            'category_id' => '',
            'company' => '',
            'expires_at' => date('Y-m-d', time() - (60*60*24)),
            'is_paid' => 111,
            'location' => 'bad example',
            'union_type' => 'not valid'
        ]);

        $url = '/creator/projects/' . $project->id;

        $this->post( $url, $attributes)
            ->assertStatus(302)
            ->assertSessionHasErrors([
                            'title',
                            'description',
                            'category_id',
                            'company',
                            'expires_at',
                            'is_paid',
                            'location',
                            'union_type'
                        ]);


    }

    /** @test */
    public function can_add_role_to_project()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $attributes = [
            'name' => $this->faker->name,
            'gender' => $this->faker->randomElement(['male & female', 'female', 'male']),
            'is_transgender' => $this->faker->boolean(1),
            'min_age' => config('ajizzy.min_age'),
            'max_age' => rand(config('ajizzy.min_age'), 99),
            'description' => $this->faker->paragraph,
            'requires_nudity' => $this->faker->boolean(1),
            'user_id' => factory('App\User')->create()->id,
            'role_type_id' => factory('App\RoleType')->create()->id
        ];

        $this->json('POST', '/creator/projects/' . $project->id . '/roles', $attributes)
                ->assertStatus(200)
                ->assertJson(['status' => 'ok']);
    }



    /** @test */
    public function can_fetch_project_roles()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        factory('App\ProjectCastingRole', 3)->create(['project_id' => $project->id]);

        $this->get('/creator/projects/' . $project->id . '/roles')
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok'
            ]);
    }

    /** @test */
    public function can_delete_project_role()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $role = factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        $this->delete('/creator/projects/' . $project->id . '/roles/' . $role->id)
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok'
            ]);
    }


    /** @test */
    public function can_add_location_to_project()
    {
        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $location = factory('App\Country')->create();

        $attributes = [
            'country_id' => $location->id,
        ];

        $this->post('/creator/projects/' . $project->id . '/locations', $attributes)
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /** @test */
    public function can_not_add_invalid_location_to_a_project()
    {
        $project = factory('App\Project')->create();
        $this->signIn($project->owner);


        $attributes = [
            'country_id' => '-12222',
        ];

        $this->post('/creator/projects/' . $project->id . '/locations', $attributes)
            ->assertStatus(302)
            ->assertSessionHasErrors('country_id');
    }


    /** @test */
    public function can_fetch_project_locations()
    {
        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $attributes = [
            'country_id' => '-12822', // we are sure this ID is not in db
        ];

        $this->post('/creator/projects/' . $project->id . '/locations', $attributes)
            ->assertSessionHasErrors('country_id');
    }


    /** @test */
    public function delete_remove_location_from_a_project()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $location = factory('App\Country')->create();
        $project->addLocation([$location->id]);

        $this->delete('/creator/projects/' . $project->id  . '/locations/' . $location->id)
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /** @test */
    public function can_add_location_with_dates_to_project()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $location = factory('App\Country')->create();

        $project->addLocation([$location->id]);

        $auditionType = $this->faker->randomElement(['open', 'closed']);
        $note  = $this->faker->paragraph(10);

        $attributes = [
            'id' => $project->locations[0]->location->id,
            'country_id' => $location->id,
            'city' => $location->id,
            'province' => $location->id,
            'street' => '1 while loop',
            'venue' => 'Simple HQ',
            'locationInfo' => [
                        [  'audition_date' => date('Y-m-d', time() + (66*60*24)),
                            'audition_type' => $auditionType,
                            'audition_note' => $note,
                            'start_time' => '08:00',
                            'end_time' => '17:00',]
            ]
        ];

        $this->post('/creator/projects/' . $project->id . '/locations-with-dates', $attributes)
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);

        $this->assertDatabaseHas('project_dates', ['audition_type' => $auditionType, 'audition_note' => $note]);
    }

    /** @test */
    public function can_get_project_locations()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $locations = [$location->id => ['location_type' => 'nationwide'], $location2->id => ['location_type' => 'nationwide']];

        $project->addLocation($locations);

        $locations = $project->locations;

        $expected = [
            [
                'name' => $locations[0]->name,
                'id' => $locations[0]->id,
                'location' => [
                    'project_id' => $project->id,
                    'country_id' => $locations[0]->id,
                    'id' => $locations[0]->location->id,
                    'venue' => null,
                    'street' => null,
                    'city' => null,
                    'province' => null,
                    'location_type' => 'nationwide'
                ]
            ],
            [
                'name' => $locations[1]->name,
                'id' => $locations[1]->id,
                'location' => [
                    'project_id' => $project->id,
                    'country_id' => $locations[1]->id,
                    'id' => $locations[1]->location->id,
                    'venue' => null,
                    'street' => null,
                    'city' => null,
                    'province' => null,
                    'location_type' => 'nationwide'
                ]
            ]
        ];

        $response = $this->get('/creator/projects/' . $project->id .'/locations?location_type=nationwide')
                ->assertStatus(200)
                 ->assertJson($expected);
    }


    /** @test */
    public function can_delete_a_location_from_project()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $project->addLocation([$location->id, $location2->id]);

        $locations = $project->locations;

        $this->assertEquals(2, count($locations));


        $this->delete('/creator/projects/' . $project->id .'/locations/' . $locations[0]->id)
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);

        $this->assertEquals(1, $project->locations()->count());

    }

    /** @test */
    public function can_get_dates_from_a_locations()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $projectLocation = factory('App\ProjectLocation')->create(['project_id' => $project->id]);

        $projectDates = factory('App\ProjectDate', 2)->create(['country_project_id' => $projectLocation->id]);

        $this->get('/creator/projects/'.$project->id.'/locations-with-dates/' . $projectLocation->country_id)
                ->assertStatus(200)
                ->assertJson($projectDates->toArray());
    }

    /** @test */
    public function can_remove_date_from_a_locations()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $projectLocation = factory('App\ProjectLocation')->create(['project_id' => $project->id]);

        $projectDates = factory('App\ProjectDate', 2)->create(['country_project_id' => $projectLocation->id]);

        $this->assertEquals(2, $projectLocation->dates()->count());

        $this->delete('/creator/projects/'.$project->id.'/locations-with-dates/' . $projectLocation->id . '/' . $projectDates[0]->id)
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);

        $this->assertEquals(1, $projectLocation->dates()->count());

    }


    /** @test */
    public function attach_country_to_a_project()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $country = factory('App\Country')->create();

        $this->post('/creator/projects/' . $project->id . '/attach-country', ['country' => $country->id])
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /** @test */
    public function can_not_attach_invalid_country_to_a_project()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $this->post('/creator/projects/' . $project->id . '/attach-country', ['country' => -11])
            ->assertStatus(302)
            ->assertSessionHasErrors('country');
    }

    /** @test */
    public function can_not_attach_country_to_invalid_project()
    {

        $project = factory('App\Project')->create();
        $this->signIn($project->owner);

        $country = factory('App\Country')->create();

        $this->post('/creator/projects/1111/attach-country', ['country' => $country->id])
            ->assertStatus(404);
    }


}
