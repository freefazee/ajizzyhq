<?php

namespace Tests\Feature;

use App\Application;
use App\Project;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserApplyingForRoleTest extends TestCase
{
    use RefreshDatabase;

   /** @test */
   public function guest_can_not_apply_for_a_role()
   {

       $project = factory('App\Project')->create();
       $this->post('/casting-calls/' . $project->id . '/apply')
            ->assertStatus(302)
            ->assertRedirect('/login');
   }

    /** @test */
    public function user_without_a_subscription_can_not_apply_for_a_role()
    {
        $this->signIn();

        $project = factory('App\Project')->create();

        $this->post('/casting-calls/' . $project->id . '/apply')
            ->assertStatus(302)
            ->assertRedirect('/join/plans');
    }

    /** @test */
    public function user_can_apply_for_a_project_role(){

        $profile = factory('App\Profile')->create();
        $user = $this->signIn($profile->user);

        $user->profile->activateSubscriptionForMonths();

        $project = factory('App\Project')->create(['status' => Project::STATUS_PUBLISHED]);

        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);
        $role = factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);
        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        $this->post('/casting-calls/' . $project->id . '/apply', ['user_id' => $user->id, 'role_id' => $role->id])
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);


        $this->assertDatabaseHas('applications', ['project_casting_role_id' => $role->id, 'user_id' =>  $user->id]);
    }

    /** @test */
    public function user_can_apply_for_a_project_role_as_drafted(){

        $profile = factory('App\Profile')->create();
        $user = $this->signIn($profile->user);

        $user->profile->activateSubscriptionForMonths();

        $project = factory('App\Project')->create(['status' => Project::STATUS_PUBLISHED]);

        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);
        $role = factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);
        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        $this->post('/casting-calls/' . $project->id . '/draft-apply', ['user_id' => $user->id, 'role_id' => $role->id])
            ->assertStatus(200)
            ->assertJson(['status' => 'ok']);


        $this->assertDatabaseHas('applications', ['project_casting_role_id' => $role->id, 'user_id' =>  $user->id, 'draft' => 1]);
    }

    /** @test
     * TODO: This test is really confusing dut to dependencies. It needs to be refactored.
     */
    public function user_can_apply_for_a_project_role_with_required_document_upload(){

        $profile = factory('App\Profile')->create();
        $user = $this->signIn($profile->user);

        $user->profile->activateSubscriptionForMonths();

        $project = factory('App\Project')->create(['status' => Project::STATUS_PUBLISHED]);

        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);
        $role = factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        // add media types
        $mediaTypes = [
            factory('App\MediaType')->create(['valid_mime_types' => 'image/jpeg,image/pipeg,image/png', 'input_type' => 'upload'])->id,
            ];
        $role->addMediaTypes($mediaTypes);

        $application = Application::createDraft($user, $project, $role);

        // test our validation
       $this->json('POST', '/casting-calls/' . $project->id . '/apply/' . $application->id . '/media/' . $mediaTypes[0])
           ->assertStatus(422)
            ->assertJsonValidationErrors('media-' . $mediaTypes[0]);

        Storage::fake('medias');

        $res = $this->json('POST', '/casting-calls/' . $project->id . '/apply/' . $application->id . '/media/' . $mediaTypes[0], [
            'media-' . $mediaTypes[0] =>  $file = UploadedFile::fake()->image('required_media.jpg')
        ])->assertStatus(200)
            ->assertJson(['status' => 'ok']);

        $payload = json_decode($res->getContent());

        Storage::disk(env('APPLICATION_MEDIA_DISK'))->assertExists($payload->data->filename);

        $this->assertDatabaseHas('application_media',
            ['filename' => $payload->data->filename,
                'application_id' => $payload->data->application_id,
                'media_type_id' =>  $mediaTypes[0]]);


        $this->assertEquals(1, $application->medias()->count());

    }

}
