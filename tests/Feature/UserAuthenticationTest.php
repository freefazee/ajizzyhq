<?php

namespace Tests\Feature;
use App\Events\OnRegistered;
use App\Events\UserPasswordChanged;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAuthenticationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        factory('App\Country')->create(['cca2' => 'KP']);
    }

    /**
     * @test
     */
    public function guest_can_login_with_valid_credentials()
    {

        $this->get('/login')
                ->assertStatus(200);

        $user = factory('App\User')->create(); //default password is password

        $attributes = ['email' => $user->email, 'password' => 'password'];

        $this->post('/login', $attributes)
                    ->assertStatus(302)
                    ->assertSessionDoesntHaveErrors('error')
                    ->assertRedirect('/account/home');

        $this->signIn($user);

        $this->get('/account')->assertStatus(302);
    }

    /** @test */
    public function guest_can_not_login_with_invalid_credentials()
    {
        $this->get('/login')
            ->assertStatus(200);

        factory('App\User')->create();

        $attributes = ['email' => 'test@example.com', 'password' => '12345'];

        $this->post('/login', $attributes)
            ->assertStatus(302)
            ->assertRedirect('/login')
            ->assertSessionHas('error');
    }

    /** @test */
    public function login_form_requires_email_and_password()
    {

        $attributes = factory('App\User')->raw(['email' => '', 'password' => '1234']);

        $this->post('/login', $attributes)
            ->assertSessionHasErrors(['email', 'password'])->assertStatus(302);

    }



    /** @test */
    public function only_logged_in_user_can_signout()
    {
        $this->get('/logout')
                ->assertRedirect('/')
                ->assertStatus(302);

    }

    /** @test */
    public function logged_in_user_can_redirect()
    {
        $this->signIn();
        $this->get('/logout')
            ->assertRedirect('/')
            ->assertStatus(302);
    }

    /** @test */
    public function guest_talent_can_join()
    {
        $this->withoutExceptionHandling();

        Event::fake();

        $country = factory('App\Country')->create(['cca2' => 'NG']);

        $this->session(['location' => $country]);

        $price = factory('App\Price')->create(['country_id' => $country->id]);

        $this->json('GET', '/join', ['price' => $price])
                ->assertViewHas('price')
                ->assertStatus(200);

        // select a place

        $this->get('/join?plan=' . $price->id)->assertStatus(200);



        $attributes = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'email' => $this->faker->email,
            'country' => $country->cca2,
            'phone' => '8066100671',
            'password' => str_random(10),
            'terms_and_condition' => '1',
            'age_verification' => '1',
            'plan' => $price->id
        ];


       $res = $this->post('/join', $attributes)
            ->assertRedirect('/join/plans')
            ->assertStatus(302);


        $this->assertDatabaseHas('users', ['email' => $attributes['email'],
                                                'first_name' => $attributes['first_name'],
                                                'phone' => '+234' . $attributes['phone'],
                                                ]);

        Event::assertDispatched( OnRegistered::class);

    }

    /** @test */
    public function only_logged_in_users_can_see_plans_page()
    {

        $this->get('/join/plans')->assertStatus(302)->assertRedirect('/login');


    }

    /** @test */
    public function a_reg_form_requires_first_name()
    {
        $attributes = factory('App\User')->raw(['first_name' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('first_name');
    }

    /** @test */
    public function a_reg_form_requires_last_name()
    {
        $attributes = factory('App\User')->raw(['last_name' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('last_name');
    }

    /** @test */
    public function a_reg_form_requires_a_phone()
    {

        $attributes = factory('App\User')->raw(['phone' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('phone');

        $country = factory('App\Country')->create(['cca2' => 'NG']);

        $attributes = factory('App\User')->raw(['phone' => '11111', 'country' => $country->cca2]);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('phone');
    }

    /** @test */
    public function a_reg_form_requires_an_email()
    {
        $attributes = factory('App\User')->raw(['email' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_reg_form_requires_a_plan()
    {
        $attributes = factory('App\User')->raw(['plan' => 'invalid plan']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('plan');
    }



    /** @test */
    public function a_reg_form_requires_a_country()
    {
        $attributes = factory('App\User')->raw(['cca2' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('country');
    }

    /** @test */
    public function banned_names_must_not_be_used_for_singing_up()
    {
        factory('App\BannedName')->create(['name' => 'admin@ajizzy.com']);
        factory('App\BannedName')->create(['name' => 'admin']);

        $attributes = factory('App\User')->raw(['email' => 'admin@ajizzy.com', 'first_name' => 'admin', 'last_name' => 'ADMIN']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('email')
            ->assertSessionHasErrors('first_name')
            ->assertSessionHasErrors('last_name');
    }




    /** @test */
    public function a_reg_form_requires_age_verification()
    {
        $attributes = factory('App\User')->raw(['age_verification' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('age_verification');

        $attributes = factory('App\User')->raw(['age_verification' => 'other value']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('age_verification');
    }

    /** @test */
    public function a_reg_form_requires_terms_and_condition()
    {
        $attributes = factory('App\User')->raw(['terms_and_condition' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('terms_and_condition');

        $attributes = factory('App\User')->raw(['terms_and_condition' => 'other value']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('terms_and_condition');
    }

    /** @test */
    public function a_reg_form_requires_valid_password()
    {

        factory('App\Country')->create(['cca2' => 'KP']);

        $attributes = factory('App\User')->raw(['password' => '']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('password');

        // test the length too
        $attributes = factory('App\User')->raw(['password' => '12s2']);

        $this->post('/join', $attributes)
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function only_logged_in_user_can_change_password()
    {
        $this->post('/account/profile/password')
                    ->assertStatus(302)
                    ->assertRedirect('/login');

    }



    /** @test */
    public function logged_in_user_can_change_password_via_ajax()
    {
        $this->signIn();
        $this->makePasswordChangeRequest(true);
    }

    /** @test */
    public function logged_in_user_can_change_password_via_regular_post()
    {
        $this->signIn();

        $this->makePasswordChangeRequest();

    }

    private function makePasswordChangeRequest($isAjax=false)
    {
        Event::fake();

        $attributes = [
            'current_password' => 'password',
            'password' => 'passwordNew',
            'confirm_password' => 'passwordNew'
        ];

        if($isAjax) {
            $this->json('POST', '/account/profile/password', $attributes)
                ->assertStatus(200)
                ->assertJson([
                    'status' => 'ok',
                    'goto' => route('login')
                ]);
        }
        else
        {
            $this->post('/account/profile/password', $attributes)->assertStatus(302)
                ->assertRedirect('/login');
        }

        $this->get('/account')
            ->assertStatus(302)
            ->assertRedirect('/login');

        Event::assertDispatched( UserPasswordChanged::class);

    }

    /** @test */
    public function user_change_password_requires_current_password()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['password' => 'password123', 'confirm_password' => 'password123'])
                ->assertStatus(302)
                ->assertSessionHasErrors('current_password');
    }

    /** @test */
    public function user_change_password_requires_password()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['current_password' => 'password', 'confirm_password' => 'password123'])
            ->assertStatus(302)
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function user_change_password_requires_confirm_password()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['password' => 'password', 'current_password' => 'password'])
            ->assertStatus(302)
            ->assertSessionHasErrors('confirm_password');
    }

    /** @test */
    public function user_change_password_requires_confirm_password_must_match()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['password' => 'password', 'current_password' => 'password', 'confirm_password' => 'passwordNoMatch'] )
            ->assertStatus(302)
            ->assertSessionHasErrors('confirm_password');
    }

    /** @test */
    public function user_change_password__must_valid_valid_length()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['password' => 'pass', 'current_password' => 'password', 'confirm_password' => 'password'] )
            ->assertStatus(302)
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function user_change_password__current_password_must_be_different_from_new_password()
    {
        $this->signIn();

        $this->post('/account/profile/password', ['password' => 'password', 'current_password' => 'password', 'confirm_password' => 'password'] )
            ->assertStatus(302)
            ->assertSessionHasErrors('password');
    }


}
