<?php

namespace Tests\Feature;

use App\Ajizzy\Billing\FakePaymentVerification;
use App\Ajizzy\Contracts\PaymentVerificationContract;
use App\Profile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BillingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function logged_in_user_can_change_subscription_package()
    {
        $this->withoutExceptionHandling();

        $user = $this->signIn();

        $price = factory('App\Price')->create();

        $this->get(route('account.change.package', $price->id))
                ->assertStatus(302)
                ->assertRedirect('/join/plans');


        $this->assertEquals($user->pricePlan->id, $price->id);
    }

    /** @test */
    public function logged_in_user_can_fetch_new_pricing_info()
    {


        $user = $this->signIn();

        // seed the database
        $this->artisan('db:seed --class=PriceRangeSeeder');

        // create a price and attach count
        $country = factory('App\Country')->create(['currencies' => '{"USD": {"name": "United States dollar", "symbol": "$"}}', 'cca2' => 'US', 'name' => 'United State']);

        $price  =  factory('App\Price')->create(['price' => 1000, 'country_id' => $country->id]);

        // set the price for the the user
        $user->updatePlan($price);

        // 2 = 3-5 a.k.a Quarterly
        $this->get('/account/packages/1/pricing')
                ->assertStatus(200)
                ->assertJsonStructure(['currency', 'discount', 'total', 'default']);


    }

    /** @test */
    public function user_can_order_subscription()
    {

        $user = $this->signIn();

        $this->artisan('db:seed --class=PriceRangeSeeder');

        $country = factory('App\Country')->create(['currencies' => '{"USD": {"name": "United States dollar", "symbol": "$"}}', 'cca2' => 'US', 'name' => 'United State']);

        $price  =  factory('App\Price')->create(['price' => 1000, 'country_id' => $country->id]);

        $user->updatePlan($price);

        $this->post('/account/packages/order-subscription', ['range' => 2, 'method' => 'ajizzy'])
            ->assertStatus(200)
            ->assertJsonStructure(['amount', 'txf', 'email', 'currency']);

    }


    /** @test */
    public function method_is_required_to_order_a_subscription()
    {

        $user = $this->signIn();

        $this->json('POST', '/account/packages/order-subscription', ['range' => 2])
            ->assertStatus(422)
            ->assertJsonValidationErrors('method');

    }

    /** @test */
    public function range_is_required_to_order_a_subscription()
    {

        $user = $this->signIn();

        $this->json('POST', '/account/packages/order-subscription', ['method' => 'ajizzy'])
            ->assertStatus(422)
            ->assertJsonValidationErrors('range');

    }


    /** @test */
    public function verify_rave_transaction()
    {
        $this->withoutExceptionHandling();

        $profile = factory('App\Profile')->create();

        $user = $this->signIn($profile->user);

        $transaction = factory('App\Transaction')->create(['user_id' => $user->id, 'qty' => 2]);

        $paymentGateway =  new FakePaymentVerification;

        $this->app->instance(PaymentVerificationContract::class, $paymentGateway);

        $this->get('/account/billing/' . $transaction->txf . '/verify')
                ->assertStatus(200)
                ->assertJson([
                    'status' => 'ok'
                ]);

        // lets make sure the user now have subscription
        $profile = Profile::find($profile->id);
        $this->assertEquals(2, now()->diffInMonths($profile->subscription_expires));
    }



}
