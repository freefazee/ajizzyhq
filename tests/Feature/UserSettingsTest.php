<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserSettingsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function can_not_load_basic_user_details_if_not_signed_in()
    {
        $this->json('GET', '/account/api/profile/settings')
                ->assertStatus(401);
    }

    /** @test */
    public function user_can_load_basic_details_from_endpoint()
    {
        $this->withoutExceptionHandling()->signIn(factory('App\Profile')->create()->user);

        $this->json('GET', '/account/api/profile/settings')
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok'
            ]);
    }

    /** @test */
    public function user_should_load_account_settings_page()
    {
        $this->withoutExceptionHandling()->signIn(factory('App\Profile')->create()->user);
        $this->get('/account/profile/settings')
                ->assertStatus(200);
    }

    /** @test */
    public function user_should_not_see_settings_page_without_a_profile()
    {
        $this->withoutExceptionHandling();
        $this->signIn(); // no profile account

        $this->get('/account/profile/settings')
            ->assertStatus(302)
            ->assertRedirect('/account');
    }
}
