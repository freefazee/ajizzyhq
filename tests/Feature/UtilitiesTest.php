<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UtilitiesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_load_appearance()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $this->get('/utilities/appearance')
                ->assertStatus(200)
                ->assertJson([
                    'body_types' => [],
                    'eye_colors' => [],
                    'hair_colors' => [],
                    'appearance' => [ ],
                    'ethnicities' => []
                ]);
    }


    /** @test */
    public function can_load_body_sizes()
    {
        $this->get('/utilities/body-sizes')
            ->assertStatus(200);
    }

    /** @test */
    public function can_load_eye_colors()
    {
        $this->get('/utilities/eye-colors')
            ->assertStatus(200);
    }


    /** @test */
    public function can_load_hair_colors()
    {
        $this->get('/utilities/hair-colors')
            ->assertStatus(200);
    }

}
