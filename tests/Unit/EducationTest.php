<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EducationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function belong_to_a_user()
    {
        $education = factory('App\Education')->create([
            'school' => 'st 1',
            'degree' => 'bsc',
            'instructor' => 'test',
            'location' => 'usa',
            'year_completed' => '2019'
        ]);

        $this->assertInstanceOf(User::class, $education->owner);
    }

}
