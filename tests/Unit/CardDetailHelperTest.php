<?php

namespace Tests\Unit;

use App\Ajizzy\Billing\CardDetailHelper;
use App\Ajizzy\Billing\FakeCard;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CardDetailHelperTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_store_new_card(){

        $card = new FakeCard(null);
        $helper = new CardDetailHelper($card);
        $user = factory('App\User')->create();

       $newcard =  $helper->store($user);

        $this->assertDatabaseHas('cards', ['num' => $newcard->num]);

        $user = User::find($user->id);

        $this->assertEquals(1, $user->cards->count());

    }
}
