<?php

namespace Tests\Unit;

use App\Category;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
   use RefreshDatabase, WithFaker;

   /** @test */
   public function can_create_a_category()
   {
       $category = factory('App\Category')->create();

       $this->assertNotNull($category->name);
       $this->assertInstanceOf(Category::class, $category);
   }

   /** @test */
   public function can_have_parent()
   {
       $category1 = factory('App\Category')->create();
       $category2 = factory('App\Category')->create(['parent_id' => $category1->id]);

       $this->assertNotNull($category2->parent);
       $this->assertInstanceOf(Category::class, $category2->parent);
   }

   /** @test */
   public function can_have_children()
   {
       $parent = factory('App\Category')->create();
       factory('App\Category')->create(['parent_id' => $parent->id]);
       factory('App\Category')->create(['parent_id' => $parent->id]);

       $this->assertNotNull($parent->children);

       $this->assertInstanceOf(Collection::class, $parent->children);
   }

   /** @test */
   public function can_have_many_projects()
   {
       $category = factory('App\Category')->create();
       factory('App\Project')->create(['category_id' => $category->id]);
       factory('App\Project')->create(['category_id' => $category->id]);

       $this->assertInstanceOf(Collection::class, $category->projects);

       $this->assertEquals(2, $category->projects()->count());
   }
}
