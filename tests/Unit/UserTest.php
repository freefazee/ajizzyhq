<?php

namespace Tests\Unit;

use App\Appearance;
use App\Country;
use App\Price;
use App\Profile;
use App\User;
use App\Media;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_belongs_to_a_country()
    {
        $user = factory('App\User')->create();

        $this->assertInstanceOf(Country::class, $user->country);

    }

    /** @test */
    public function user_password_is_hashed_by_default()
    {
        $user = factory('App\User')->create();

        $this->assertNotEquals($user->password, 'password');
    }

    /** @test */
    public function user_has_a_profile()
    {
        $user = factory('App\User')->create();
        $profile = factory('App\Profile')->create(['user_id' => $user->id]);

        $this->assertInstanceOf('App\Profile', $user->profile);
    }

    /** @test */
    public function user_can_fetch_profile()
    {
        $user = factory('App\User')->create();

        $ourProfile = $user->getProfileData();

       $this->assertEquals(str_slug($user->first_name . ' ' . $user->last_name), $ourProfile['profile_url']);
    }

    /** @est */
    public function get_user_default_avatar_url_without_a_profile()
    {
        $user = factory('App\User')->create();

        $this->assertEquals($user->getAvatarUrl(), asset('assets/images/icon-user--blank.png'));

    }

    /** @est */
    public function get_a_user_with_a_profile_avatar_url()
    {
        $profile = factory('App\Profile')->create();

        $this->assertEquals($profile->user->getAvatarUrl(), asset('storage/' . $profile->avatar_url));

    }

    /** @test */
    public function created_user_has_id()
    {
        $user  = factory('App\User')->create(['id' => null]);

        $this->assertNotNull($user->id);
    }

    /** @test */
    public function user_have_an_appearance()
    {
        $user  = factory('App\User')->create();

        factory('App\Appearance')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Appearance::class, $user->appearance);
    }

    /** @test */
    public function should_return_user_fullname()
    {
        $user = factory('App\User')->create(['first_name' => 'John', 'last_name' => 'Doe']);

        $this->assertEquals('John Doe', $user->getFullname());
    }

    /** @test */
    public function user_can_set_new_password()
    {
        $user = factory('App\User')->create();
        $newPassword = '123password@';
        $oldPassword = $user->password;

        $user->setNewPassword($newPassword);

        $this->assertNotEquals($user->password, $oldPassword);
        $this->assertNotEquals($user->password, $newPassword);
    }

    /** @test */
    public function can_create_user_profile()
    {


        $user = factory('App\User')->create();
        $this->assertNull($user->profile);

        $user->createProfile(['title' => 'test']);

        $newUser = User::find($user->id);

        $this->assertNotNull($newUser->profile);

        $this->assertInstanceOf(Profile::class, $newUser->profile);
    }


    /** @test */
    public function user_has_many_projects()
    {

        $user = factory('App\User')->create();

        $this->assertInstanceOf(Collection::class, $user->projects);

    }


    /** @test */
    public function has_belongs_to_price_plan()
    {
        $plan = factory('App\Price')->create();

        $user = factory('App\User')->create(['plan' => $plan->id]);

        $this->assertInstanceOf(Price::class, $user->pricePlan);

    }

    /** @test */
    public function can_update_user_plan()
    {
        $price = factory('App\Price')->create();

        $user = factory('App\User')->create();

        $user->updatePlan($price);

        $this->assertEquals($user->pricePlan->id, $price->id);
    }

    /** @test */
    public function can_have_many_transactions()
    {
        $user = factory('App\User')->create();

        factory('App\Transaction')->create(['user_id' => $user->id]);
        factory('App\Transaction')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Collection::class, $user->transactions);
        $this->assertEquals(2, $user->transactions->count());

    }

    /** @test */
    public function can_have_many_cards()
    {
        $user = factory('App\User')->create();

        factory('App\Card')->create(['user_id' => $user->id]);
        factory('App\Card')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Collection::class, $user->cards);
        $this->assertEquals(2, $user->cards->count());

    }

    /** @test */
    public function user_has_many_applications() {

        $user = factory('App\User')->create();

        factory('App\Application')->create(['user_id' => $user->id]);
        factory('App\Application')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Collection::class, $user->applications);

        $this->assertEquals(2, $user->applications()->count());
    }

    /** @test */
    public function has_many_card_medias()
    {

        $profile = factory('App\Profile')->create();

       $media =  $profile->medias()->create(['name' => 'test', 'type' => Media::MEDIA_TYPE_IMAGE]);

        $this->assertNotNull($media);

        $this->assertEquals('test', $media->name);
        $this->assertEquals(Media::MEDIA_TYPE_IMAGE, $media->type);
    }

    /** @test */
    public function can_have_many_notes()
    {
        $user = factory('App\User')->create();

        $this->assertInstanceOf(Collection::class, $user->notes);
    }

    /** @test */
    public function can_have_many_created_notes()
    {
        $user = factory('App\User')->create();

        $this->assertInstanceOf(Collection::class, $user->createdNotes);
    }

    /** @test */
    public function can_add_new_note_to_a_user()
    {
        $user = factory('App\User')->create();
        $creator = factory('App\User')->create();

        $note = $user->addNoteFrom($creator, 'I like this user');

       $this->assertEquals($user->id, $note->owner->id);
       $this->assertEquals($creator->id, $note->creator->id);
    }

    /** @test */
    public function only_one_note_can_exist_per_user_from_a_creator()
    {
        $user = factory('App\User')->create();
        $creator = factory('App\User')->create();

        $user->addNoteFrom($creator, 'I like this user');
        $user->addNoteFrom($creator, 'the first note should be overwritten by this one');

        $this->assertEquals(1, $user->notes->count());
        $this->assertEquals('the first note should be overwritten by this one', $user->notes()->first()->body);
    }

    /** @test */
    public function can_have_many_ethnicities()
    {
        $user = factory('App\User')->create();

        $this->assertInstanceOf(Collection::class, $user->ethnicities);
    }

    /** @test */
    public function can_have_many_educations()
    {
        $user = factory('App\User')->create();

        factory('App\Education')->create(['user_id' => $user->id]);
        factory('App\Education')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Collection::class, $user->educations);

        $this->assertEquals(2, $user->educations->count());

    }
}
