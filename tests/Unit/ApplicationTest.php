<?php

namespace Tests\Unit;

use App\Application;
use App\Events\ApplicationApproved;
use App\ProjectCastingRole;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApplicationTest extends TestCase
{
    use RefreshDatabase;

   /** @test */
   public function can_create_an_application()
   {
       $projectRole = factory('App\ProjectCastingRole')->create();

       $application = factory('App\Application')->create([
           'user_id' => $projectRole->project->owner->id,
           'project_casting_role_id' => $projectRole->id
       ]);

       $this->assertInstanceOf(Application::class, $application);

       $this->assertDatabaseHas('applications', ['project_casting_role_id' => $projectRole->id, 'user_id' =>  $projectRole->project->owner->id]);
   }

   /** @test */
   public function application_belongs_to_project_role()
   {
       $application = factory('App\Application')->create();

       $this->assertInstanceOf(ProjectCastingRole::class, $application->role);
   }

    /** @test */
    public function application_belongs_to_user()
    {
        $application = factory('App\Application')->create();

        $this->assertInstanceOf(User::class, $application->owner);
    }

    /** @test */
    public function has_many_media()
    {
        $application = factory('App\Application')->create();

        factory('App\ApplicationMedia')->create([
            'application_id' => $application->id,
            'media_type_id' => factory('App\MediaType')->create()->id,
            'filename' => 'sample.jpg'
            ]);

        $this->assertInstanceOf(Collection::class, $application->medias);
    }

    /** @test */
    public function application_fetch_drafted()
    {
        $application = factory('App\Application')->create(['draft' => 1]);

        $result = Application::draft()->find($application->id);

        $this->assertInstanceOf(Application::class, $result);

        $this->assertEquals($application->id, $result->id);
    }


    /** @test */
    public function can_set_red_basin()
    {
        $application = factory('App\Application')->create(['draft' => 0]);

        $application->setRedBasin();

        $this->assertEquals(Application::RED_BASIN, $application->basin);
    }


    /** @test */
    public function can_set_green_basin()
    {

        $application = factory('App\Application')->create(['draft' => 0]);

        Event::fake();

        $application->setGreenBasin();

        $this->assertEquals(Application::GREEN_BASIN, $application->basin);

        Event::assertDispatched(ApplicationApproved::class);
    }


    /** @test */
    public function can_set_yellow_basin()
    {
        $application = factory('App\Application')->create(['draft' => 0]);

        $application->setYellowBasin();

        $this->assertEquals(Application::YELLOW_BASIN, $application->basin);
    }



    /** @test */
    public function can_set_basin()
    {
        $application = factory('App\Application')->create(['draft' => 0]);

        $application->setBasin('yellow');

        $this->assertEquals(Application::YELLOW_BASIN, $application->basin);

        $application->setBasin('green');

        $this->assertEquals(Application::GREEN_BASIN, $application->basin);

        $application->setBasin('red');

        $this->assertEquals(Application::RED_BASIN, $application->basin);
    }


}
