<?php

namespace Tests\Unit;

use App\Project;
use App\ProjectCastingRole;
use App\RoleType;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectCastingRoleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_a_role()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $this->assertInstanceOf(ProjectCastingRole::class, $role);
    }

    /** @test */
    public function belongs_to_a_project()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $this->assertInstanceOf(Project::class, $role->project);
    }

    /** @test */
    public function can_have_many_ethnicities()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $this->assertInstanceOf(Collection::class, $role->ethnicities);
    }

    /** @test */
    public function belongs_to_a_role_type()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $this->assertNotNull($role->roleType);
        $this->assertInstanceOf(RoleType::class, $role->roleType);
    }

    /** @test */
    public function can_have_many_media_types()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $this->assertInstanceOf(Collection::class, $role->mediaTypes);
    }

    /** @test */
    public function can_add_ethnicities()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $records = factory('App\Ethnicity', 3)->create();

        $data = [];
        foreach($records as $record)
            $data[] = $record->id;

        $role->addEthnicities($data);

        $this->assertEquals(3, $role->ethnicities()->count());
    }


    /** @test */
    public function can_add_media_types()
    {
        $role = factory('App\ProjectCastingRole')->create();

        $records = factory('App\MediaType', 3)->create();

        $data = [];
        foreach($records as $record)
            $data[] = $record->id;

        $role->addMediaTypes($data);

        $this->assertEquals(3, $role->mediaTypes()->count());
    }
}
