<?php

namespace Tests\Unit;

use App\Note;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoteTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_create_note()
    {
        $data = [
            'user_id' => factory('App\User')->create()->id,
            'body' => 'test',
            'creator_id' => factory('App\User')->create()->id,
        ];

        $note = Note::create($data);

        $this->assertNotNull($note);

        $this->assertDatabaseHas('notes', $data);

    }


    /** @test */
    public function not_has_owner()
    {
        $user = factory('App\User')->create();

        $note = factory('App\Note')->create(['user_id' => $user->id]);

        $this->assertEquals($user->id, $note->owner->id);

    }

    /** @test */
    public function not_has_a_creator()
    {
        $creator = factory('App\User')->create();

        $note = factory('App\Note')->create(['creator_id' => $creator->id]);

       $this->assertEquals($creator->id, $note->creator->id);

    }


}
