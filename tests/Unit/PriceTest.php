<?php

namespace Tests\Unit;

use App\Country;
use App\Price;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PriceTest extends TestCase
{

    use RefreshDatabase;

     /** @test */
     public function can_create_new_price()
     {
         $price = factory('App\Price')->create();

         $this->assertNotNull($price);

         $this->assertInstanceOf(Price::class, $price);

     }

    /** @test */
    public function can_get_price_by_name()
    {
       $name = 'starter';

        factory('App\Price')->create(['name' => $name]);

        $price = \App\Ajizzy\Price::getByName($name);

        $this->assertNotNull($price);

        $this->assertEquals($name, $price->name);
    }


    /** @test */
    public function can_get_price_by_country_id()
    {
        $country = factory('App\Country')->create();

        factory('App\Price')->create(['country_id' => $country->id]);

        $price = \App\Ajizzy\Price::getByCountryId($country->id);

        $this->assertNotNull($price);

        $this->assertEquals($country->id, $price->country_id);
    }

    /** @test */
    public function can_set_a_price_as_the_default()
    {

        $price1 = factory('App\Price')->create();
        $price2 = factory('App\Price')->create();
        $price3 = factory('App\Price')->create();

        $this->assertEquals(0,  \App\Ajizzy\Price::countDefaults());

        $defaultPrice = \App\Ajizzy\Price::makeDefault($price2);

        $this->assertEquals(1,  \App\Ajizzy\Price::countDefaults());

        $this->assertEquals($defaultPrice->name, $price2->name);

        // lets be sure that no matter how many times we call set default we can only have one
        $defaultPrice = \App\Ajizzy\Price::makeDefault($price3);
        $this->assertEquals(1,  \App\Ajizzy\Price::countDefaults());
        $this->assertEquals($defaultPrice->name, $price3->name);


    }

    /** @test */
    public function can_empty_price_table()
    {
       factory('App\Price')->create();
       factory('App\Price')->create();
       factory('App\Price')->create();

       $this->assertEquals(3, \App\Ajizzy\Price::count());

       \App\Ajizzy\Price::empty();

        $this->assertEquals(0, Price::count());

    }

    /** @test */
    public function can_create_price_using_create_method()
    {
        $attributes = factory('App\Price')->raw();

        $price = \App\Ajizzy\Price::create($attributes);

        $this->assertNotNull($price);

        $this->assertEquals(1, Price::count());
    }

    /** @test */
    public function price_belongs_to_country()
    {

        $price = factory('App\Price')->create();

        $this->assertInstanceOf(Country::class, $price->country);
    }

    /** @test */
    public function can_get_prices_by_location()
    {

        $location  = factory('App\Country')->create();
        factory('App\Price')->create(['country_id' => $location->id]);
        factory('App\Price')->create(['country_id' => $location->id]);
        factory('App\Price')->create(['country_id' => $location->id]);

        $price = \App\Ajizzy\Price::getPriceForLocation($location);

        $this->assertInstanceOf(Price::class, $price);

    }



    /** @test */
    public function calculate_monthly_price_based_on_selected_range()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '1-2', 'label' => 'Monthly', 'discount' => 0]);

        $price  =  factory('App\Price')->create(['price' => 1000]);


        $total = $price->calculateBasedOnRange($priceRange);

        $this->assertEquals(1000 / 100, $total);

    }


    /** @test */
    public function calculate_yearly_price_based_on_selected_range()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '12-100000', 'label' => 'Yearly', 'discount' => 15]);

        $price  =  factory('App\Price')->create(['price' => 1000]);


        $total = $price->calculateBasedOnRange($priceRange);

        $this->assertEquals(120, $total);

    }

    /** @test */
    public function calculate_3_months_price_based_on_selected_range()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '3-5', 'label' => 'Yearly', 'discount' => 15]);

        $price  =  factory('App\Price')->create(['price' => 1000]);


        $total = $price->calculateBasedOnRange($priceRange);

        $this->assertEquals(30, $total);

    }

    /** @test */
    public function calculate_discount()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '3-5', 'label' => 'Yearly', 'discount' => 15]);

        $price  =  factory('App\Price')->create(['price' => 1000]);


        $total = $price->calculateBasedOnRange($priceRange);

        $discount = $price->calculateDiscount($total, $priceRange);

        $this->assertEquals(4.50, $discount);
    }

    /** @test */
    public function calculate_discount_for_monthly()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '1-2', 'label' => 'Yearly', 'discount' => 0]);

        $price  =  factory('App\Price')->create(['price' => 1000]);


        $total = $price->calculateBasedOnRange($priceRange);

        $discount = $price->calculateDiscount($total, $priceRange);

        $this->assertEquals(0, $discount);
    }

    /** @test */
    public function calculate_total_for_monthly()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '1-2', 'label' => 'Yearly', 'discount' => 0]);

        $price  =  factory('App\Price')->create(['price' => 1000]);

        $total = $price->calculateTotal($priceRange);


        $this->assertEquals(10, $total);
    }

    /** @test */
    public function calculate_total_for_yearly()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '12-100000', 'label' => 'Yearly', 'discount' => 15]);

        $price  =  factory('App\Price')->create(['price' => 1000]);

        $total = $price->calculateTotal($priceRange);


        $this->assertEquals(102, $total);
    }

    /** @test */
    public function can_construct_pricing_detail()
    {

        $priceRange = factory('App\PriceRange')->create(['range' => '12-100000', 'label' => 'Yearly', 'discount' => 15]);

        $country = factory('App\Country')->create(['currencies' => '{"USD": {"name": "United States dollar", "symbol": "$"}}', 'cca2' => 'US', 'name' => 'United State']);

        $price  =  factory('App\Price')->create(['price' => 1000, 'country_id' => $country]);


        $expected = [
            'currency' => '$',
            'total' => 102,
            'discount' => '15',
            'default' => $priceRange->id,
            'label' => 'yearly'

        ];


       $this->assertArraySimilar($expected, \App\Ajizzy\Price::getPricingDetail($price, $priceRange));
    }

    /** @test */
    public function can_get_formatted_price()
    {
        $price = factory('App\Price')->create(['price' => 1000]);

        $this->assertEquals(10, $price->formatted_price);
    }
}
