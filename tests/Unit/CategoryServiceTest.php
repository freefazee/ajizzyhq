<?php

namespace Tests\Unit;

use App\Category;
use App\Services\CategoryService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_return_active_categories_with_children()
    {

        $category1  = factory('App\Category')->create();
        $category1->children()->create(factory('App\Category')->raw());
        $category1->children()->create(factory('App\Category')->raw());
        $category1->children()->create(factory('App\Category')->raw());

        $category2  = factory('App\Category')->create();
        $category2->children()->create(factory('App\Category')->raw());
        $category2->children()->create(factory('App\Category')->raw());
        $category2->children()->create(factory('App\Category')->raw());


        $categoryService = (new CategoryService())->getActiveCategories();

        $this->assertEquals(2, count($categoryService));
        $this->assertEquals(3, $categoryService[0]->children()->count());
        $this->assertEquals(3, $categoryService[1]->children()->count());
    }
}
