<?php

namespace Tests\Unit;

use App\Country;
use App\State;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_test()
    {
        $state = factory('App\State')->create();

        $this->assertInstanceOf(State::class, $state);
    }

    /** @test */
    public function must_belong_to_a_country()
    {
        $state = factory('App\State')->create();

        $this->assertInstanceOf(Country::class, $state->country);
    }

}
