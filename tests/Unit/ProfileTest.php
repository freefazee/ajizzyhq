<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
   use RefreshDatabase;

   /** @test */
   public function profile_belongs_to_a_user()
   {
        $profile = factory('App\Profile')->create();

        $this->assertInstanceOf(User::class, $profile->user);
   }

   /** @test */
   public function subscription_should_be_active()
   {
       $profile = factory('App\Profile')
           ->create(['subscription_expires' => now()
               ->addMonth()]);

       $this->assertTrue($profile->subscriptionActive());

       $this->assertTrue($profile->notCanceled());

   }


    /** @test */
    public function subscription_should_be_inactive()
    {
        $profile = factory('App\Profile')
                    ->create(['subscription_expires' => now()
                        ->subMinutes(1)]);

        $this->assertFalse($profile->subscriptionActive());

    }

    /** @test */
    public function new_profile_should_not_have_subscrition()
    {
        $profile = factory('App\Profile')->create();

        $this->assertFalse($profile->subscriptionActive());
    }

    /** @test */
    public function can_activate_subscription_for_n_months()
    {
       $profile = factory('App\Profile')->create();

       $profile->activateSubscriptionForMonths(2);

       $this->assertEquals(2, now()->diffInMonths($profile->subscription_expires));

        $this->assertTrue($profile->subscriptionActive());


    }

    /** @test */
    public function subscription_can_be_canceled()
    {
        $profile = factory('App\Profile')
            ->create(['subscription_expires' => now()
                ->addMonth()]);


        $profile->cancel();

        $this->assertTrue($profile->isCanceled());
    }

    /** @test */
    public function has_subscription_by_canceled()
    {
        $profile = factory('App\Profile')->create();

        $profile->activateSubscriptionForMonths();

        $profile->cancel();

        $this->assertTrue($profile->subscriptionActive());

       $this->assertTrue($profile->isCanceled());

    }

    /** @test */
    public function can_add_media()
    {
        $profile = factory('App\Profile')->create();
        $profile->addMedia('test name', 'url', 'image');

        $this->assertEquals(1, $profile->medias()->count());
    }
}
