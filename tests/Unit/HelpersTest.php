<?php

namespace Tests\Unit;

use App\Ajizzy\Helpers;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpersTest extends TestCase
{
    /** @test */
    public function remove_non_numeric_charters_from_string()
    {
       $s = '+1 234 2338 (38383) ';

       $this->assertEquals('1234233838383', Helpers::removeNoneNumericChars($s));
    }
}
