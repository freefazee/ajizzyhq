<?php

namespace Tests\Unit;

use App\BannedName;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BannedNameTest extends TestCase
{
    use RefreshDatabase;

   /** @test */
    public function can_create_a_banned_name()
    {
        $bannedName = factory('App\BannedName')->create();

        $this->assertInstanceOf(BannedName::class, $bannedName);
    }

    /** @test */
    public function can_get_list_of_banned_names()
    {
        $bannedName1 = factory('App\BannedName')->create();
        $bannedName2 = factory('App\BannedName')->create();
        $bannedName3 = factory('App\BannedName')->create();

        $bannedNames = BannedName::getNames();

        $this->assertArraySimilar([$bannedName1->name, $bannedName2->name, $bannedName3->name], $bannedNames);


    }

}
