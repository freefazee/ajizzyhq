<?php

namespace Tests\Unit;

use App\Ajizzy\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AjizzyCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_get_list_of_categories()
    {
        $c1 = factory('App\Category')->create();
        $c2 = factory('App\Category')->create();
        factory('App\Category')->create(['sort' => 1]);

        //children
        factory('App\Category')->create(['parent_id' => $c1->id]);
        factory('App\Category')->create(['parent_id' => $c2->id]);


        $categories  = Category::getCategoryWithChildren();

        $this->assertEquals(3, count($categories)); // count parents

        $this->assertEquals(1, count($categories[0]->children));

        $this->assertEquals(1, count($categories[1]->children));

        $this->assertEquals(0, count($categories[2]->children));

        $this->assertEquals($c1->name, $categories[0]->name);
        $this->assertEquals($c2->name, $categories[1]->name);

    }

    /** @test */
    public function can_get_one_latest_project_for_each_child()
    {
        $c1 = factory('App\Category')->create();
        $c2 = factory('App\Category')->create();
        factory('App\Category')->create(['sort' => 1]);

        //children
        $child1 = factory('App\Category')->create(['parent_id' => $c1->id]);
        $child2= factory('App\Category')->create(['parent_id' => $c2->id]);

        factory('App\Project')->create(['status' => 'published', 'category_id' => $child1->id]);
        factory('App\Project')->create(['status' => 'published', 'category_id' => $child2->id]);


        $result  = Category::getOneLatestProjectForEachChild($c1);

        $this->assertEquals(1, count($result->children));
    }
}
