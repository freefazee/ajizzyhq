<?php

namespace Tests\Unit;

use App\Card;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CardTest extends TestCase
{
    use RefreshDatabase;

   /** @test */
    public function can_create_new_card()
    {
        $card = factory('App\Card')->create([
            'num' => '1234567890',
            'exp_month' => '01',
            'exp_year' => '2020',
            'brand' => 'Ajzz',
            'country' => 'Nigeria',
            'token' => 'test',
            'type' => 'visa',
            'name' => 'ajizz'
        ]);

        $this->assertInstanceOf(Card::class, $card);
    }

    /** @test */
    public function card_belongs_to_a_user()
    {

        $card = factory('App\Card')->create();

        $this->assertInstanceOf(User::class, $card->owner);
    }

}
