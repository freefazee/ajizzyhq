<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use App\RoleType;
use Illuminate\Foundation\Testing\RefreshDatabase;


class RoleTypeTest extends TestCase
{
   use RefreshDatabase;

   /** @test */
   public function can_fetch_role_type()
   {
       $roleType = factory('App\RoleType')->create();

       $this->assertNotNull($roleType);
       $this->assertInstanceOf(RoleType::class, $roleType);
   }

   /** @test */
   public function has_many_collections()
   {
       $roleType = factory('App\RoleType')->create();

       $this->assertInstanceOf(Collection::class, $roleType->projects);
   }

}
