<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EthnicityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_fetch_ethnicities()
    {
        $ethnicities = factory('App\Ethnicity', 3)->create();

        $this->assertInstanceOf(Collection::class, $ethnicities);

        $this->assertEquals(3, count($ethnicities));
    }

    /** @test */
    public function has_many_roles()
    {
        $ethnicity = factory('App\Ethnicity')->create();

        $this->assertInstanceOf(Collection::class, $ethnicity->roles);

    }

    /** @test */
    public function has_many_users()
    {
        $ethnicity = factory('App\Ethnicity')->create();

        $this->assertInstanceOf(Collection::class, $ethnicity->users);

    }
}
