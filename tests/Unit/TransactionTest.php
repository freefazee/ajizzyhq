<?php

namespace Tests\Unit;

use App\Transaction;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_transaction()
    {
        $transaction = factory('App\Transaction')->create([
            'amount' => 1000,
            'method' => 'rave',
            'txf' => 'test-ref',
            'status' => 'success',
            'trans_for' => 'subscription',
            'api_response' => '',
            'qty' => 1
        ]);

        $this->assertInstanceOf(Transaction::class, $transaction);
    }

    /** @test */
    public function transaction_belongs_to_a_user()
    {
        $user = factory('App\User')->create();

        $transaction = factory('App\Transaction')->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $transaction->owner);
    }

    /** @test */
    public function can_get_formatted_amount()
    {
        $transaction = factory('App\Transaction')->create(['amount' => 1000, 'qty' => 1]);

        $this->assertEquals(10, $transaction->formatted_amount);
    }

    /** @test */
    public function can_create_a_transaction_using_createTransaction_method()
    {
        $user = factory('App\User')->create();

        $data = [
            'amount' => 1000,
            'trans_for' => Transaction::PAYING_FOR_SUBSCRIPTION,
            'method' => 'rave',
            'qty' => 1
        ];
        $transaction = Transaction::createTransaction($data, $user);

        $this->assertInstanceOf(Transaction::class, $transaction);
    }

    /** @test */
    public function can_mark_transaction_as_successful()
    {
        $transaction = factory('App\Transaction')->create();

        $this->assertNotTrue($transaction->status === Transaction::STATUS_SUCCESSFUL);

        $transaction->markAsSuccessful();

        $this->assertTrue($transaction->status === Transaction::STATUS_SUCCESSFUL);
    }

    /** @test */
    public function can_mark_transaction_as_failed()
    {
        $transaction = factory('App\Transaction')->create();

        $this->assertNotTrue($transaction->status === Transaction::STATUS_FAILED);

        $transaction->markAsFailed('Failed from api');

        $this->assertTrue($transaction->status === Transaction::STATUS_FAILED);
    }
}
