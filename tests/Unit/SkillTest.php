<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Skill;

class SkillTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_create_skill()
    {
        $skill = factory('App\Skill')->create();

        $this->assertNotNull($skill);

        $this->assertInstanceOf(Skill::class, $skill);
    }

    /** @test */
    public function can_fetch_skills()
    {
        $this->withoutExceptionHandling();

        $this->json('GET', route('graph.skills'))
                ->assertStatus(200)
                ->assertJson([
                    'data' => []
                ]);

    }
}
