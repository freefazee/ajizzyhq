<?php

namespace Tests\Unit;

use App\Category;
use App\Country;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Project;
use App\User;

class ProjectTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function can_create_a_project()
    {
        $project = factory('App\Project')->create();

        $this->assertNotNull($project);
        $this->assertInstanceOf(Project::class, $project);
    }

    /** @test */
    public function project_belongs_to_user()
    {
        $project = factory('App\Project')->create();

        $this->assertInstanceOf(User::class, $project->owner);
    }

    /** @test */
    public function project_has_many_roles()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();

        $this->assertInstanceOf(Collection::class, $project->roles);
    }

    /** @test */
    public function can_add_new_role()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();

        $attributes = [
            'name' => $this->faker->name,
            'gender' => $this->faker->randomElement(['male & female', 'female', 'male']),
            'is_transgender' => $this->faker->boolean(1),
            'min_age' => 16,
            'max_age' => rand(17, 99),
            'description' => $this->faker->paragraph,
            'requires_nudity' => $this->faker->boolean(1),
            'user_id' => factory('App\User')->create()->id
        ];

        $project->addRole($attributes);

        $this->assertEquals(1, $project->roles()->count());
    }



    /** @test */
    public function can_delete_a_project_role()
    {

        $project = factory('App\Project')->create();

        $attributes = [
            'name' => $this->faker->name,
            'gender' => $this->faker->randomElement(['male & female', 'female', 'male']),
            'is_transgender' => $this->faker->boolean(1),
            'min_age' => 16,
            'max_age' => rand(17, 99),
            'description' => $this->faker->paragraph,
            'requires_nudity' => $this->faker->boolean(1),
            'user_id' => factory('App\User')->create()->id
        ];

        $project->addRole($attributes);

        $this->assertEquals(1, $project->roles()->count());

        $firstRole =  $project->roles()->first();

        $project->removeRole($firstRole);

        $this->assertEquals(0, $project->roles()->count());
    }

    /** @test */
    public function can_be_featured()
    {

        $project = factory('App\Project')->create(['featured' => now()]);

        $this->assertTrue($project->isFeatured());

    }

    /** @test */
    public function can_be_marked_as_featured()
    {

        $project = factory('App\Project')->create();

        $this->assertFalse($project->isFeatured());

        $project->markAsFeatured();

        $this->assertTrue($project->isFeatured());

    }

    /** @test */
    public function can_be_marked_as_unfeatured()
    {

        $project = factory('App\Project')->create(['featured' => now()]);

        $this->assertTrue($project->isFeatured());

        $project->markAsUnFeatured();

        $this->assertFalse($project->isFeatured());

    }

    /** @test */
    public function project_belongs_to_a_category()
    {
        $category = factory('App\Category')->create();
        $project = factory('App\Project')->create(['category_id' => $category->id]);

        $this->assertInstanceOf(Category::class, $project->category);
    }

    /** @test */
    public function can_attach_one_location_to_project()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();

        $project->addLocation($location);

        $this->assertEquals(1, $project->locations()->count());

    }

    /** @test */
    public function can_attach_multiple_locations_to_project()
    {
        $project = factory('App\Project')->create();

        $location1 = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $project->addLocation([$location1->id, $location2->id]);

        $this->assertEquals(2, $project->locations()->count());

    }

    /** @test */
    public function can_attach_one_location_with_additional_data_to_project()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();


        $venue = $this->faker->name;
        $street = $this->faker->streetAddress;

        $project->addLocation([
            $location->id => [
                    'venue' => $venue,
                    'street' => $street,
                    'city' => 0,
                    'province' => 0

                ]

        ]);

        $this->assertEquals(1, $project->locations()->count());

        $location = $project->locations[0];

        $this->assertEquals($venue, $location->location->venue);
        $this->assertEquals($street, $location->location->street);
        $this->assertEquals(0, $location->location->province);
        $this->assertEquals(0, $location->location->city);


    }

    /** @test */
    public function must_not_attach_duplicate_location()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();

        $project->addLocation([$location->id, $location->id]);

        $this->assertEquals(1, $project->locations()->count());

    }

    /** @test */
    public function can_fetch_attached_locations_using_relationship()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $project->addLocation([$location->id, $location2->id]);

        $this->assertEquals($location->name, $project->locations[0]->name);
        $this->assertEquals($location2->name, $project->locations[1]->name);

    }


    /** @test */
    public function can_remove_attached_location()
    {
        $project = factory('App\Project')->create();
        $location = factory('App\Country')->create();

        $project->addLocation([$location->id]);

        $this->assertEquals(1, $project->locations()->count());

        $project->removeLocation([$location->id]);

        $this->assertEquals(0, $project->locations()->count());

    }

    /** @test */
    public function can_get_locations_using_getLocations()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $locations = [
                        $location->id => ['location_type' => 'nationwide'],
                        $location2->id => ['location_type' => 'nationwide']
        ];

        $project->addLocation($locations);

        $projectLocations = $project->getLocations();



        $expacted = [
           [
               'id' => $project->locations[0]->id,
               'name' => $project->locations[0]->name,
               'short_name' => $project->locations[0]->cca2,
               'location' => [
                   'project_id' => $project->id,
                   'country_id' => $project->locations[0]->id,
                   'id' =>  $project->locations[0]->location->id,
                   'venue' => null,
                   'street' => null,
                   'city' => null,
                   'province' => null,
                   'location_type' => 'nationwide'
               ],
           ],
           [
               'id' => $project->locations[1]->id,
               'name' => $project->locations[1]->name,
               'short_name' => $project->locations[1]->cca2,
               'location' => [
                   'project_id' => $project->id,
                   'country_id' => $project->locations[1]->id,
                   'id' =>  $project->locations[1]->location->id,
                   'venue' => null,
                   'street' => null,
                   'city' => null,
                   'province' => null,
                   'location_type' => 'nationwide'
               ],
           ],
       ];

        $this->assertArraySimilar($expacted, $project->getLocations('nationwide'));


    }

    /** @test */
    public function can_remove_location_from_project_via_removeLocation()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $project->addLocation([$location->id, $location2->id]);

        $this->assertEquals(2, $project->locations()->count());

        $project->removeLocation($location->id);

        $this->assertEquals(1, $project->locations()->count());
    }

    /** @test */
    public function can_remove_all_locations_from_a_project_via_removeAllLocations()
    {
        $project = factory('App\Project')->create();

        $location = factory('App\Country')->create();
        $location2 = factory('App\Country')->create();

        $project->addLocation([$location->id, $location2->id]);

        $this->assertEquals(2, $project->locations()->count());

        $project->removeAllLocations();

        $this->assertEquals(0, $project->locations()->count());
    }


    /** @test */
    public function can_get_project_url()
    {
        $project = factory('App\Project')->create();
        $expected = '/casting-calls/'. $project->id;
        $this->assertEquals($expected, $project->getUrl());

    }

    /** @test */
    public function project_belongs_to_country()
    {

        $project = factory('App\Project')->create();

        $this->assertInstanceOf(Country::class, $project->country);
    }

    /** @test */
    public function can_format_expiry_date()
    {


        $project = factory('App\Project')->create(['expires_at' =>'2019-07-01']);

        $this->assertEquals('Jul 1, 2019', $project->formatted_expiry_date);
    }

    /** @test */
    public function can_have_many_applications()
    {
        $project = factory('App\Project')->create();
        $role = factory('App\ProjectCastingRole')->create(['project_id' => $project->id]);

        // allow two users to apply for this role
        factory('App\Application')->create(['project_casting_role_id' => $role->id, 'user_id' => factory('App\User')->create()->id]);
        factory('App\Application')->create(['project_casting_role_id' => $role->id, 'user_id' => factory('App\User')->create()->id]);

        $this->assertEquals(2, $project->applications()->count());

    }


}
