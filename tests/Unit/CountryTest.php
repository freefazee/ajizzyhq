<?php

namespace Tests\Unit;

use App\Country;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountryTest extends TestCase {

    use RefreshDatabase;

    /** @test */
    public function a_country_has_many_users()
    {
        $country = factory('App\Country')->create();

        $this->assertInstanceOf(Collection::class, $country->users);

    }

    /** @test */
    public function a_country_has_many_projects()
    {
        $country = factory('App\Country')->create();

        $this->assertInstanceOf(Collection::class, $country->localProjects);

    }


    /** @test */
    public function a_country_getAll_should_return_all_records()
    {
        factory('App\Country', 3)->create();

        $this->assertInstanceOf(Collection::class, Country::getAll());

        $this->assertEquals(3, count(Country::getAll()));

    }

    /** @test */
    public function a_country_has_many_states()
    {
        $country = factory('App\Country')->create();

        $this->assertInstanceOf(Collection::class, $country->states);
    }
    /** @test */
    public function can_fetch_projects_attached_to_a_country()
    {
        $country = factory('App\Country')->create();

        $project = factory('App\Project')->create();

        $country->addProject($project);

        $this->assertEquals(1, $country->projects()->count());

    }

    /** @test */
    public function can_get_currency_symbol()
    {
        $country = factory('App\Country')
            ->create(['currencies' => '{"USD": {"name": "United States dollar", "symbol": "$"}}', 'cca2' => 'US', 'name' => 'United State']);


        $this->assertEquals('$', $country->getCurrency());

    }

    /** @test */
    public function can_get_currency_code()
    {
        $country = factory('App\Country')
            ->create(['currencies' => '{"USD": {"name": "United States dollar", "symbol": "$"}}', 'cca2' => 'US', 'name' => 'United State']);


        $this->assertEquals('USD', $country->getCurrencyCode());

    }
}
