<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppearanceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function belong_to_user()
    {
       $appearance = factory('App\Appearance')->create();

       $this->assertInstanceOf(User::class, $appearance->user);
    }
}
