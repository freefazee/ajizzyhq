<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Media;

class MediaTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_create_media()
    {
        $this->withoutExceptionHandling();

        $media = factory('App\Media')->create([
            'type' => 'image',
            'name' => 'test name',
            'mediable_id' => factory('App\Profile')->create()->id,
            'mediable_type' => 'App\Profile'
        ]);

        $this->assertNotNull($media);

        $this->assertInstanceOf(Media::class, $media);
    }
}
