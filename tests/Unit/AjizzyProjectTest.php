<?php

namespace Tests\Unit;

use App\Ajizzy\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AjizzyProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_get_one_latest_project_by_category_id()
    {
            $category = factory('App\Category')->create();

            factory('App\Project')->create(['status' => 'published', 'created_at' => now()->subHours(2), 'category_id' => $category->id]);
            $project = factory('App\Project')->create(['status' => 'published', 'category_id' => $category->id]); // newer

            $result = Project::getOneLatestByCategoryId($category->id);

            $this->assertNotNull($result);

            $this->assertEquals($project->title, $result->title);
            $this->assertEquals($project->category->id, $result->category->id);


    }
}
