<?php

namespace Tests\Unit;

use App\Price;
use App\PriceRange;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PriceRangeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_price_range()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '1', 'label' => 'Monthly', 'discount' => 0]);

        $this->assertNotNull($priceRange);

        $this->assertEquals('1',  $priceRange->range);
        $this->assertEquals('Monthly', $priceRange->label);
        $this->assertEquals(0, $priceRange->discount);
    }

    /** @test */
    public function can_get_price_ranges()
    {
        $this->artisan('db:seed --class=PriceRangeSeeder');

        $priceRanges = PriceRange::get();

        $this->assertEquals(4, $priceRanges->count());

        $this->assertInstanceOf(Collection::class, $priceRanges);
    }


    /** @test */
    public function can_get_monthly_range_based_on_the_range_number()
    {
        $this->artisan('db:seed --class=PriceRangeSeeder');

        foreach([1,2] as $t)
        {
            $priceRange = PriceRange::getByRange($t);

            $this->assertNotNull($priceRange);
            $this->assertEquals('1-2', $priceRange->range);
            $this->assertEquals(0, $priceRange->discount);
            $this->assertEquals('Monthly', $priceRange->label);
        }


    }

    /** @test */
    public function can_get_quarterly_range_based_on_the_range_number()
    {
        $this->artisan('db:seed --class=PriceRangeSeeder');


       foreach([3,4,5] as $t)
       {
           $priceRange = PriceRange::getByRange($t);

           $this->assertNotNull($priceRange);
           $this->assertEquals('3-5', $priceRange->range);
           $this->assertEquals(5, $priceRange->discount);
           $this->assertEquals('Quarterly', $priceRange->label);

       }
    }

    /** @test */
    public function can_get_every_6_months_range_based_on_the_range_number()
    {
        $this->artisan('db:seed --class=PriceRangeSeeder');


        foreach([6,7,8,9,10,11] as $t)
        {
            $priceRange = PriceRange::getByRange($t);

            $this->assertNotNull($priceRange);
            $this->assertEquals('6-11', $priceRange->range);
            $this->assertEquals(10, $priceRange->discount);
            $this->assertEquals('Every 6 months', $priceRange->label);

        }
    }

    /** @test */
    public function can_get_yearly_range_based_on_the_range_number()
    {
        $this->artisan('db:seed --class=PriceRangeSeeder');


        foreach([12,13,14,15,16] as $t)
        {
            $priceRange = PriceRange::getByRange($t);

            $this->assertNotNull($priceRange);
            $this->assertEquals('12-10000000', $priceRange->range);
            $this->assertEquals(40, $priceRange->discount);
            $this->assertEquals('Yearly', $priceRange->label);

        }
    }

    /** @test */
    public function can_get_left_and_right()
    {
        $priceRange = factory('App\PriceRange')->create(['range' => '1-2', 'label' => 'Monthly', 'discount' => 0]);

        $this->assertEquals(1, $priceRange->left);
        $this->assertEquals(2, $priceRange->right);
    }
}
