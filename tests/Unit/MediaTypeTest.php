<?php

namespace Tests\Unit;

use App\MediaType;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MediaTypeTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_create_media_type()
    {
        $mediaType = factory('App\MediaType')->create([
            'name' => 'Video Reel',
            'description' => 'Short video clip',
            'valid_mime_types' => 'video/mp4,video/quicktime,video/3gpp,video/x-msvideo',
            'input_type' => 'upload'
        ]);

        $this->assertInstanceOf(MediaType::class, $mediaType);
    }

    /** @test */
    public function can_fetch_mediaTypes()
    {
        $mediaTypes = factory('App\MediaType', 3)->create();

        $this->assertInstanceOf(Collection::class, $mediaTypes);

        $this->assertEquals(3, count($mediaTypes));
    }

    /** @test */
    public function has_many_roles()
    {
        $mediaType = factory('App\MediaType')->create();

        $this->assertInstanceOf(Collection::class, $mediaType->roles);

    }
}
