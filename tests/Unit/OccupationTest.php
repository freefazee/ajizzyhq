<?php

namespace Tests\Unit;

use App\Occupation;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OccupationTest extends TestCase
{
    use RefreshDatabase;

   /** @test */
    public function can_create_occupations()
    {
        $occupations = factory('App\Occupation', 1)->create();

        $this->assertCount(1, $occupations);
    }

    /** @test */
    public function can_fetch_all_occupations()
    {
        factory('App\Occupation', 1)->create();

        $occupations = Occupation::getAll();

        $this->assertCount(1, $occupations);
    }
}
