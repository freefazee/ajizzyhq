<?php

use Faker\Generator as Faker;

$factory->define(App\Media::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'url' => 'test',
        'mediable_id' => 'test',
        'mediable_type' => 'test',
        'mime' => $faker->mimeType
    ];
});
