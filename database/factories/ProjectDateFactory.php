<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectDate::class, function (Faker $faker) {
    return [
        'country_project_id' => factory('App\ProjectLocation')->create()->id,
        'audition_date' => date('Y-m-d', time() + (66*60*24)),
        'audition_type' => $faker->randomElement(['closed', 'open']),
        'audition_note' => $faker->paragraph(),
        'start_time' => '08:00',
        'end_time' => '17:00'
    ];
});
