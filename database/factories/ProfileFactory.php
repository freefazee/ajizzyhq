<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory('App\User')->create()->id;
        },
        'stage_name' => $faker->userName,
        'title' => $faker->jobTitle,
        'profile_url' => $faker->userName,
        'gender' => $faker->randomElement(['male', 'female', 'unspecified']),
        'is_transgender' => $faker->randomElement(['0', '1']),
        'dob' => $faker->numberBetween(1, 31),
        'mob' => $faker->numberBetween(1, 12),
        'yob' => $faker->numberBetween(date('Y') - 16, (date('Y') - 116)),
        'is_public' => (int)$faker->boolean(90),
        'about_company' => 'new about data'
    ];
});
