<?php

use Faker\Generator as Faker;

$factory->define(App\Application::class, function (Faker $faker) {
    $projectRole = factory('App\ProjectCastingRole')->create();
    return [
        'user_id' => $projectRole->project->owner->id,
        'project_casting_role_id' => $projectRole->id,
        'draft' => false,
        'status' => 'new'
    ];
});
