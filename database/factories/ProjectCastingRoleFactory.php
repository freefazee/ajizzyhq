<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectCastingRole::class, function (Faker $faker) {
    return [
        'project_id' => function() {
            return factory('App\Project')->create()->id;
        },
        'name' => $faker->word(3, true),
        'gender' => $faker->randomElement(['male', 'female', 'male and female']),
        'is_transgender' => $faker->boolean(1),
        'min_age' => 16,
        'max_age' => rand(17, 99),
        'description' => $faker->sentence(10),
        'requires_nudity' => $faker->boolean(1),
        'user_id' => null,
        'role_type_id' => function() {
            return factory('App\RoleType')->create()->id;
        }
    ];
});
