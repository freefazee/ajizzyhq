<?php

use Faker\Generator as Faker;

$factory->define(App\MediaType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->word,
        'valid_mime_types' => $faker->mimeType,
        'input_type' => 'upload'
    ];
});
