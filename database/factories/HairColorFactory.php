<?php

use Faker\Generator as Faker;

$factory->define(App\HairColor::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
