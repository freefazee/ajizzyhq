<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'category_id' => function() {
                return factory('App\Category')->create()->id;
        },
        'user_id' => function() {
            return factory('App\User')->create()->id;
        },
        'company' => $faker->company,
        'country_id' => factory('App\Country')->create()->id,
        'description' => $faker->paragraph(6),
        'is_paid' => 1,
        'compensation_detail' => $faker->paragraph(2),
        'expires_at' => date('Y-m-d', time() + (60* 60*24)),
        'location' => $faker->randomElement(['local', 'nationwide', 'worldwide']),
        'accepting_submission' => $faker->boolean(90),
        'union_type' =>  $faker->randomElement(['Union and Nonunion', 'Union', 'Nonunion', 'N/A']),
        'status' => 'drafted'
    ];
});
