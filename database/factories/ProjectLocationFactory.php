<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectLocation::class, function (Faker $faker) {
    return [
        'country_id' => factory('App\Country')->create()->id,
        'project_id' => factory('App\Project')->create()->id,
        'venue' => $faker->city,
        'street' => $faker->streetAddress,
        'city' => factory('App\Country')->create()->id,
        'province' => factory('App\Country')->create()->id,
    ];
});
