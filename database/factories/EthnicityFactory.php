<?php

use Faker\Generator as Faker;

$factory->define(App\Ethnicity::class, function (Faker $faker) {
    return [
        'name' => $faker->word(6)
    ];
});
