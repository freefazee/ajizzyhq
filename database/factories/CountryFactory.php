<?php

use Faker\Generator as Faker;

$factory->define(App\Country::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'official' => $faker->country,
        'ccn3' => $faker->numberBetween(3,3),
        'cca3' => $faker->countryISOAlpha3,
        'cca2' => $faker->countryCode,
        'currencies' => json_encode([]),
        'capital' => $faker->country,
        'region' => $faker->name,
        'subregion' => $faker->name,
        'demonym' => $faker->name,
        'borders' => json_encode([]),
    ];
});
