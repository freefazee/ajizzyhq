<?php

use Faker\Generator as Faker;

$factory->define(App\Appearance::class, function (Faker $faker) {
    return [
        'height' => 2,
        'weight' =>  3,
        'user_id' => factory('App\User')->create()->id
    ];
});
