<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Education;
use Faker\Generator as Faker;

$factory->define(Education::class, function (Faker $faker) {
    return [
        'school' => $faker->name,
        'degree' => $faker->name,
        'instructor' => $faker->name,
        'location' => $faker->city,
        'year_completed' => $faker->year,
        'user_id' => factory('App\User')->create()->id
    ];
});
