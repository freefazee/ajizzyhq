<?php

use Faker\Generator as Faker;

$factory->define(\App\Card::class, function (Faker $faker) {
    return [
            'num' => '1234567890',
            'exp_month' => '01',
            'exp_year' => '2020',
            'brand' => 'Ajzz',
            'country' => 'Nigeria',
            'token' => 'test',
            'type' => 'visa',
            'name' => 'ajizz',
            'user_id' => factory('App\User')->create()->id
    ];
});
