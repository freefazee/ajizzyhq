<?php

use Faker\Generator as Faker;

$factory->define(App\BodySize::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
