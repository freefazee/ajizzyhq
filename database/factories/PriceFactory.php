<?php

use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use App\Price;

$factory->define(Price::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'country_id' => factory('App\Country')->create()->id,
        'price' => $faker->randomElement([1000, 2000, 3000]),
        'short_desc' => $faker->paragraph(1),
        'description' => $faker->paragraph,
        'is_default' => false,
        'id' => Uuid::generate()->string

    ];
});
