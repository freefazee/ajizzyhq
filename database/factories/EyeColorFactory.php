<?php

use Faker\Generator as Faker;

$factory->define(App\EyeColor::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
