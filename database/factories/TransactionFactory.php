<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'amount' => 1000,
        'method' => 'rave',
        'txf' => str_random(30),
        'status' => 'success',
        'trans_for' => 'subscription',
        'qty' => 1
    ];
});
