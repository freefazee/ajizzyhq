<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;



$factory->define(User::class, function (Faker $faker) {
    return [
        //'id' => $faker->uuid,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'phone' => $faker->phoneNumber,
        'password' => 'password', // password
        'remember_token' => Str::random(10),
        'country_id' => function() {
            return factory(App\Country::class)->create()->id;
        },
        'plan' => $faker->randomElement(['starter', 'pro']),
        'age_verification' => '1',
        'terms_and_condition' => '1'

    ];
});
