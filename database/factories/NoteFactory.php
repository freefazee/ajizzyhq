<?php


use Faker\Generator as Faker;

$factory->define(App\Note::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'body' => 'test',
        'creator_id' => factory('App\User')->create()->id,
    ];
});
