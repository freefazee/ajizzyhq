<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('user_id');
            $table->unsignedInteger('dob')->default(0);
            $table->unsignedInteger('mob')->default(0);
            $table->unsignedInteger('yob')->default(0);
            $table->string('title')->nullable();
            $table->string('stage_name')->nullable();
            $table->string('avatar_url')->nullable();
            $table->string('profile_url')->unique()->nullable()->default(null);
            $table->string('gender', 45)->nullable();
            $table->boolean('is_transgender')->nullable()->default(false);
            $table->boolean('is_public')->default(true);
            $table->string('company')->nullable();
            $table->integer('occupation_id')->nullable();
            $table->dateTime('subscription_expires')->nullable();
            $table->dateTime('subscription_canceled')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
