<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMimeStatusFilepathSizeToMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->string('mime')->nullable();
            $table->unsignedInteger('status')->default(0); // 0 = new file. Not ready
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('size')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn(['mime', 'status', 'title', 'description', 'size']);
        });
    }
}
