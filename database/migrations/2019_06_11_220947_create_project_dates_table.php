<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_project_id');
            $table->date('audition_date')->nullable();
            $table->enum('audition_type', ['open', 'closed'])->default('closed');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->string('audition_note', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_dates');
    }
}
