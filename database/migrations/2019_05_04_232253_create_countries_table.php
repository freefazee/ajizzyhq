<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('official')->nullable();
            $table->string('cca2')->nullable();
            $table->string('ccn3')->nullable();
            $table->string('cca3', 3)->index();
            $table->json('currencies')->nullable();
            $table->string('capital')->nullable();
            $table->string('region')->nullable();
            $table->string('subregion')->nullable();
            $table->string('demonym')->nullable();
            $table->json('borders')->nullable();
            $table->string('phone_prefix')->nullable();
            $table->decimal('lat', 11,8)->default(0);
            $table->decimal('lng', 11, 8)->default(0);
            $table->bigInteger('parent_id')->nullable();
            $table->string('level')->nullable();
            $table->bigInteger('population')->default(0);
            $table->string('types')->nullable();
            $table->string('external_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
