<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_invites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('project_id');
            $table->uuid('invited_by');
            $table->uuid('invitee_id');
            $table->text('message')->nullable();
            $table->dateTime('accepted')->nullable();
            $table->text('acceptance_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_invites');
    }
}
