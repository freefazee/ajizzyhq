<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCountryProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('country_project', function (Blueprint $table) {
            $table->string('venue', 255)->nullable();
            $table->string('street', 255)->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('location_type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('country_project', function (Blueprint $table) {
            //
        });
    }
}
