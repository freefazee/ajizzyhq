<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCastingRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_casting_roles', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('project_id');
            $table->string('name')->nullable();
            $table->string('gender')->nullable();
            $table->boolean('is_transgender')->default(false);
            $table->unsignedInteger('min_age')->default(16);
            $table->unsignedInteger('max_age')->default(100);
            $table->text('description')->nullable();
            $table->boolean('requires_nudity')->default(false);
            $table->uuid('user_id')->nullable();
            $table->integer('role_type_id')->nullable();
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_casting_roles');
    }
}
