<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appearances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('user_id')->unique();
            $table->boolean('height_unit_us')->default(false);
            $table->boolean('weight_unit_us')->default(false);
            $table->string('height');
            $table->string('weight');
            $table->integer('body_size_id')->nullable();
            $table->integer('eye_color')->nullable();
            $table->integer('hair_color')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appearances');
    }
}
