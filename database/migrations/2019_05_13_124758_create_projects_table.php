<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('title')->index();
            $table->integer('category_id')->nullable();
            $table->uuid('user_id')->nullable();
            $table->string('company')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_paid')->default(true)->nullable();
            $table->text('compensation_detail')->nullable();
            $table->dateTimeTz('expires_at')->nullable();
            $table->bigInteger('country_id')->nullable()->comment('This is for local projects');
            $table->string('union_type')->nullable()->default('N/A');
            $table->enum('location', ['local', 'nationwide', 'worldwide'])->nullable();
            $table->boolean('accepting_submission')->default(true);
            $table->enum('status', ['published', 'review', 'suspended', 'drafted'])->default('drafted');
            $table->dateTime('featured')->nullable();
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
