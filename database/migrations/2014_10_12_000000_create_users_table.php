<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email', 255)->unique();
            $table->unsignedInteger('country_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('terms_and_condition')->nullable();
            $table->boolean('age_verification')->nullable();
            $table->string('plan')->nullable();
            $table->string('password');
            $table->string('api_token')->nullable();
            $table->string('phone')->unique();
            $table->string('account_type')->nullable();
            $table->string('postal_code')->nullable();
            $table->primary('id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
