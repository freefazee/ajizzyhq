<?php

use Illuminate\Database\Seeder;
use \App\Ethnicity;

class EthnicitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Ethnicity::truncate();

        $data = [
            'Asian', 'Black / African Descent', 'Ethnically Ambiguous / Multiracial', 'Indigenous Peoples', 'Latino / Hispanic', 'Middle Eastern',
            'South Asian / Indian', 'Southeast Asian / Pacific Islander', 'White / European Descent'
        ];

        foreach($data as $item) {
            Ethnicity::create(['name' => trim($item)]);
        }
    }
}
