<?php

use Illuminate\Database\Seeder;

class HairColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\HairColor::truncate();

        $data = ['Black', 'Brown', 'Blond', 'Auburn', 'Chestnut', 'Red', 'Gray', 'White', 'Bald', 'Salt & Pepper', 'Strawberry Blond', 'Multicolored/Dyed'];

        foreach($data as $item)
        {
            \App\HairColor::create(['name' => $item]);
        }
    }
}
