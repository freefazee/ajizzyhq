<?php

use App\Skill;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BodySizeSeeder::class);
        $this->call(EyeColorSeeder::class);
        $this->call(HairColorSeeder::class);
        $this->call(OccupationSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(EthnicitySeeder::class);
        $this->call(RoleTypeSeeder::class);
        $this->call(MediaTypeSeeder::class);
        $this->call(SkillSeeder::class);
        Artisan::call('country:load');

        $this->call(PriceSeeder::class);
        $this->call(PriceRangeSeeder::class);
        Artisan::call('geoip:update');

        $this->call(MessageTemplateSeeder::class);


    }
}
