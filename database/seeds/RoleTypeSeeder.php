<?php

use Illuminate\Database\Seeder;
use \App\RoleType;

class RoleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleType::truncate();
        $data = ['Lead', 'Supporting', 'Day Player', 'Background / Extra', 'Chorus / Ensemble', 'Staff / Crew', 'Unspecified'];
        foreach($data as $item)
        {
            RoleType::create(['name' => trim($item)]);
        }
    }
}
