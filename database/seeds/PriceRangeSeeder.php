<?php

use Illuminate\Database\Seeder;
use \App\PriceRange;

class PriceRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PriceRange::truncate();
        $data = [
            [
                'range' => '1-2',
                'label' => 'Monthly',
                'discount' => 0
            ],
            [
                'range' => '3-5',
                'label' => 'Quarterly',
                'discount' => 5
            ],
            [
                'range' => '6-11',
                'label' => 'Every 6 months',
                'discount' => 10
            ],
            [
                'range' => '12-10000000',
                'label' => 'Yearly',
                'discount' => 40
            ],
        ];

        foreach($data as $item)
            PriceRange::create($item);
    }
}
