<?php

use Illuminate\Database\Seeder;

class EyeColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\EyeColor::truncate();

        $data = ['Amber', 'Blue', 'Brown', 'Gray', 'Green', 'Hazel', 'Red', 'Violet'];

        foreach($data as $item)
        {
            \App\EyeColor::create(['name' => $item]);
        }
    }
}
