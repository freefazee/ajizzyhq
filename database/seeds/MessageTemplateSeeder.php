<?php

use Illuminate\Database\Seeder;

use App\MessageTemplate;
use App\Ajizzy\Message\Template;
use App\Ajizzy\Message\TemplateFactory;

class MessageTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MessageTemplate::truncate();

        TemplateFactory::create([
            'key' => 'email-verification',
            'channel' => Template::CHANNEL_EMAIL,
            'title' => 'Welcome to Ajizzy.com - Verify Email',
            'message' => 'Dear #USER_NAME#, kindly verify your email.',
        ]);
    }
}
