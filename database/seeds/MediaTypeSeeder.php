<?php

use Illuminate\Database\Seeder;
use \App\MediaType;

class MediaTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MediaType::truncate();

        $records = [
            [
                'name' => 'Headshot/Photo',
                'description' => 'You are required to upload a nice photo.',
                'valid_mime_types' => 'image/jpeg,image/pipeg,image/png',
                'input_type' => 'upload'
            ],
            [
                'name' => 'Video Reel',
                'description' => 'Short video clip',
                'valid_mime_types' => 'video/mp4,video/quicktime,video/3gpp,video/x-msvideo',
                'input_type' => 'upload'
            ],
            [
                'name' => 'Audio Reel',
                'description' => 'Short audio clip',
                'valid_mime_types' => 'audio/mpeg,audio/mid,audio/x-wav',
                'input_type' => 'upload'
            ],
            [
                'name' => 'Cover Letter',
                'description' => 'Type your cover letter',
                'valid_mime_types' => 'text/plain',
                'input_type' => 'text'
            ],
            [
                'name' => 'Resume',
                'description' => 'Upload resume',
                'valid_mime_types' => 'application/msword,application/pdf,application/rtf',
                'input_type' => 'upload'
            ]
        ];

        foreach($records as $item)
        {
            MediaType::create($item);
        }
    }
}
