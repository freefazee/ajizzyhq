<?php

use Illuminate\Database\Seeder;

class BodySizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\BodySize::truncate();
        $data = ['Average', 'Slim', 'Athletic / Toned', 'Muscular', 'Curvy', 'Heavyset / Stocky', 'Plus-Sized / Full-Figured'];
        foreach($data as $item)
        {
            \App\BodySize::create(['name' => $item]);
        }
    }
}
