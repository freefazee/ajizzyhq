<?php

use App\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::truncate();

        $data = [
            ['name' => 'Musician'],
            ['name' => 'Dancer'],
            ['name' => 'Voice Artist'],
            ['name' => 'Actor'],
            ['name' => 'Photographer']
        ];

        Skill::insert($data);
    }
}
