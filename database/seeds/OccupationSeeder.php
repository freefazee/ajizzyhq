<?php

use Illuminate\Database\Seeder;
use \App\Occupation;

class OccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Occupation::truncate();
        $data = 'Casting Director,Director,Producer,Production Company,School / Student,Agent / Manager,Actor / Performer,Content Development,Marketing / Advertising,Other';
        foreach(explode(',', $data) as $item)
        {
            Occupation::create(['name' => trim($item)]);
        }

    }
}
