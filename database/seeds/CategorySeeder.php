<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::truncate();

        $data = [

            [
                'name' => 'Theater',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Plays'],
                    ['name' => 'Musicals'],
                    ['name' => 'Chorus Calls'],
                ]
            ],

            [
                'name' => 'Film',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Feature Films'],
                    ['name' => 'Short Films'],
                    ['name' => 'Student Films'],
                ]
            ],


            [
                'name' => 'TV & Video',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Scripted TV & Video'],
                    ['name' => 'Reality TV & Documentary'],
                    ['name' => 'Music Videos'],
                    ['name' => 'Demo & Instructional Videos'],
                    ['name' => 'Multimedia'],
                ]
            ],


            [
                'name' => 'Commercials',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'National Commercials'],
                    ['name' => 'Local Commercials'],
                    ['name' => 'Spec Commercials'],
                    ['name' => 'Online Commercials & Promos'],
                    ['name' => 'Social Media Skit']
                ]
            ],


            [
                'name' => 'Modeling',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Print Modeling'],
                    ['name' => 'Fashion & Runway Modeling'],
                    ['name' => 'Commercial & Fit Modeling'],
                    ['name' => 'Promotional & Event Modeling'],
                ]
            ],

            [
                'name' => 'Performing Arts',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Dancers & Choreographers'],
                    ['name' => 'Cabaret & Variety'],
                    ['name' => 'Singers'],
                    ['name' => 'Musicians & Composers'],
                    ['name' => 'Comedians'],
                    ['name' => 'Theme Parks'],
                    ['name' => 'Cruise Lines'],
                ]
            ],

            [
                'name' => 'Voiceover',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Commercial & Film Voiceover'],
                    ['name' => 'Animation & Videogame Voiceover'],
                    ['name' => 'Audio Books'],
                    ['name' => 'Radio & Podcasts'],
                ]
            ],

            [
                'name' => 'Entertainment Jobs & Crew',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Stage Staff & Tech'],
                    ['name' => 'Film & TV Crew'],
                    ['name' => 'Writers'],
                    ['name' => 'Gigs'],
                ]
            ],

            [
                'name' => 'Events & Representation',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Agents & Managers'],
                    ['name' => 'Workshops'],
                    ['name' => 'Competitions'],
                    ['name' => 'Festivals & Events'],
                    ['name' => '"Groups & Membership Companies'],
                ]
            ],

            [
                'name' => 'Musical',
                'sort' => 1,
                'expand' => true,
                'children' => [
                    ['name' => 'Video vixen'],
                    ['name' => 'Producer'],
                ]
            ],

        ];

        $this->processData($data);
    }

    private function processData($data, $parent_id=null)
    {
        foreach($data as $item)
        {

            $parent = \App\Category::create([
                'name' => $item['name'],
                'sort' => isset($item['sort']) ? $item['sort'] : 0,
                'expand' => isset($item['expand']) ? $item['expand'] : 0,
                'parent_id' => $parent_id
            ]);
            $children = isset($item['children']) ? $item['children'] : [];
            if(count($children)) {

                $this->processData($children, $parent->id);
            }
        }
    }
}
