<?php

use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Ajizzy\Price::empty();

        $records = [
            [
                'name' => 'Premium Pack',
                'price' => 150000,
                'country_id' => \App\Ajizzy\Country::getByCode('NG')->id,
                'short_desc' => 'Get a 40% discount when you pay for one year.',
                'description' => 'More info',

            ],

            [
                'name' => 'Premium Pack',
                'price' => 499,
                'country_id' => \App\Ajizzy\Country::getByCode('US')->id,
                'short_desc' => 'Get a 40% discount when you pay for one year.',
                'description' => 'More info',

            ],
        ];

        foreach($records as $record)
        {
            \App\Ajizzy\Price::create($record);
        }
    }
}
