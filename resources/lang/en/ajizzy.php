<?php
/**
 * Ajizzy language file
 *
 */

return [

    'about' => 'Welcome to Ajizzy',
    'Profile' => 'Update Your Profile',
    'Pay now' => 'Pay Now',
    'accept terms and condition' => 'You must accept our terms and condition first',
    'phone not valid' => 'The phone number is not valid. Ensure you pick the right country.',
    'phone already taken' => 'The phone number already taken',
    'age verification' => 'Please confirm you are at least 16 years',
    'unable to login' => 'Login Error: Make sure your email and password is correct',
    'password changed' => 'Password changed'
];
