import ProfileForm from '../account/ProfileForm';
import UserProfilePhotos from '../account/UserProfileAvatar';
import UserLocation from './UserLocation';
import Appearance  from './Appearance';
import Education  from "./Education";
import UserSkills from "./UserSkills";

const routes = [
    { path: '/', component: ProfileForm,  props: {goto: 'appearance'}, exact: true },
    { path: '/photos', component: UserProfilePhotos, props: {goto: 'location'}},
    {path: '/location', component: UserLocation, props: {goto: 'education'}},
    {path: '/appearance', component: Appearance, props: {goto: 'photos'}},
    {path: '/education', component: Education, props: {goto: 'skills'}},
    {path: '/skills', component: UserSkills, props: {goto: 'profile-done'}}
];

export  default routes;
