window.Vue = require('vue');
import VueRouter from 'vue-router'


window.Aj = require('../ajizzy');

import routes  from './routers';

const router = new VueRouter({
    routes,
    mode: 'history',
    base: '/builders/profile/'
});

Vue.use(VueRouter);

const app = new Vue({
    router,
    data() {
        return {
            profile: {}
        }
    }
}).$mount('#app');

