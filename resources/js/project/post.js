window.Vue = require('vue');
window.Aj = require('../ajizzy');

import CreatorForm from './components/Form';
import BModal from 'bootstrap-vue/es/components/modal/modal'
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal'

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);


import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css'

Vue.component('datetime', Datetime);


new Vue({
    el: '#app',
    components: {
        'creator-form': CreatorForm
    }
});

