window.Vue = require('vue');
window.Aj = require('../../ajizzy');


import BModal from 'bootstrap-vue/es/components/modal/modal'
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal'

import ReadyToApplyBox from './ReadyToApplyBox';
import applyForRole from './ApplyForRole';

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);


Vue.component('applyForRole', applyForRole);
Vue.component('ReadyToApplyBox', ReadyToApplyBox);



new Vue({
    el: '#single'
});

