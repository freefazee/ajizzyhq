window.Vue = require('vue');
window.Aj = require('../../ajizzy');

import CastingCallWrappers from './CastingCallsWrapper';

import BModal from 'bootstrap-vue/es/components/modal/modal';
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal';

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);

/**import PortalVue from 'portal-vue';
Vue.use(PortalVue);*/

new Vue({
    el: '#calls',
    components: {
        'casting-calls-wrapper': CastingCallWrappers,
    }
});

