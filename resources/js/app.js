

window.Vue = require('vue');
import BModal from 'bootstrap-vue/es/components/modal/modal';
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal';
import BSpinner from 'bootstrap-vue/es/components/spinner/spinner';

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);
Vue.component('b-spinner', BSpinner);

const app = new Vue({
    el: '#app',
    data() {
        return {
            post: {}
        }
    },
    mounted() {
        this.$root.$on('bv::modal::show', (bvEvent, modalId) => {
            console.log('Modal is about to be shown', bvEvent, modalId)
        });
    },
    methods: {
        showModal(e) {
            let vm = this;
            this.post = {t: new Date().toDateString(), title: 'This is a new post'};
            e.target.innerText = "Loading..";
            setTimeout(function(){
                vm.$refs['modal-1'].show();
                e.target.innerText = "LAUNCH DEMO MODAL";
            }, 1000);
        },
        handleHiddenState(bvModalEvt) {
            this.post = {};
        }
    }
});

console.log(app);
