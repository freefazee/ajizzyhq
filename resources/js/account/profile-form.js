window.Vue = require('vue');

window.Aj = require('../ajizzy');

// Local components
import ProfileForm from './ProfileForm';
import UserProfileAvatar from './UserProfileAvatar';

new Vue({
    el: '#app',
    components: {
        'account-profile-form': ProfileForm,
        'account-avatar': UserProfileAvatar
    }
});

