
//change-password-form
window.Aj = require('../ajizzy');

$(document).ready(function () {

    $('#change-password-form').submit(function(e){
        e.preventDefault();

        let data = {
            current_password: $('#current_password').val(),
            password: $('#new_password').val(),
            confirm_password: $('#new_password_confirm').val()
        };

        Aj.Api.post('/account/profile/password', data).then(res => {
            if(res.status === 'ok') {
                Aj.Notify.InfoSuccess({
                    title: 'Password Changed!',
                    message: 'You are required to to re-login.'}, () => {
                   document.location = res.goto;
                });
            } else {
                Aj.Notify.Danger('Unable to change password');
            }
        }).catch(err => {
             let msg = '';
             for(let prop in err.payload) {
                 if(err.payload.hasOwnProperty(prop)) {
                     msg += err.payload[prop] + '<br />';
                 }
             }
            Aj.Notify.InfoDanger({
                title: err.message,
                message: msg
            });
        });

    });
});
