window.Vue = require('vue');

window.Aj = require('../ajizzy');

require('../google-autocomplete/bootstrap');

new Vue({
    el: '#app',
    data( ) {
        return {
            networkRequest: false,
            buttonText: 'SAVE & CONTINUE <i class="fas fa-arrow-right"></i>',
            form: {}
        }
    },
    mounted() {
        this.form = JSON.parse(window.Profile);
        Vuemit.listen('setAddress', this.onAddressChanged);
    },
    methods: {
        onAddressChanged(payload) {
            if (Object.keys(payload.place).length > 0 && !this.networkRequest) {
               this.form.place_id = payload.response.place_id;
            }
        },

        handleSave() {
            let vm = this;
            this.networkRequest = true;
            let btnContent  = this.buttonText;
            this.buttonText = 'Saving <i class="fas fa-spinner fa-spin"></i>';

            Aj.Api.post('/creator/company-profile-update', this.form).then(res => {
                if(res.status === 'ok') {
                    Aj.Notify.InfoSuccess({title: 'Profile Updated!', message: '<h2 class="text-center">Click Ok to continue.</h2>'}, () => {
                            document.location  = '/account/home';
                    });
                } else {
                    Aj.Notify.Danger(res.message);
                }
            }).catch(err => Aj.Notify.Danger(err.message))
                .finally(() => {
                    vm.networkRequest = false;
                    this.buttonText = btnContent;
                });

        }

    }
});

