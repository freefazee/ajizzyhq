window.Vue = require('vue');

window.Aj = require('../ajizzy');

// Local components
import UserProfileAvatar from './UserProfileAvatar';

new Vue({
    el: '#app',
    components: {
        'account-avatar': UserProfileAvatar
    }
});

