import axios from 'axios';

const CancelToken = axios.CancelToken;

export default {

    /**
     * Get abstraction.
     * @return {Promise}
     */
    get(_endpoint, config) {
        if(!config || !config.noBlock) {
            Aj.Notify.Blocker();
        }
        return axios.get(_endpoint, config)
                    .then(res => res.data )
                    .catch(err => {
                        return Promise.reject({message: err.response.statusText, payload: {}});
                    })
                    .finally(() => {
                            Aj.Notify.CloseBlocker()
                     });
    },

    post(_endpoint, body, options) {
        if(options=== undefined || options.noLoading === undefined) {
            Aj.Notify.Blocker()
        }
        return axios.post(_endpoint, body, options ? options.config : null)
                .then(res  => res.data)
                .catch((err) => {
                    let errorMessage = err.response ? err.response.data.message : err.response.statusText;
                    if (typeof errorMessage !== 'string' && err.response.message) {
                        errorMessage = err.response.message;
                    }
                    return Promise.reject({message: errorMessage, payload:err.response.data.errors })
                }).finally(Aj.Notify.CloseBlocker);
    },

    delete(_endpoint, options) {
        if(options=== undefined || options.noLoading === undefined) {
            Aj.Notify.Blocker()
        }
        return axios.delete(_endpoint).then(res => res.data).finally(Aj.Notify.CloseBlocker);
    },

    CancelToken
};
