closeBlocker = {
        close() {
            Notiflix.Loading.Remove();
        }
};

const initNotince = () => {
    Notiflix.Notify.Init({
        width:'280px',
        position:'right-top',
        distance:'10px',
        opacity:1,
        plainText: false,
        useGoogleFont: false,
    });
};

module.exports = {
    Blocker(cb) {
        Notiflix.Loading.Init({
            className: 'notiflix-loading',
            zindex: 4000,
            backgroundColor: 'rgba(0,0,0,0.8)',
            rtl: false,
            useGoogleFont: false,
            fontFamily: 'Quicksand',
            cssAnimation: true,
            cssAnimationDuration: 400,
            clickToClose: false,
            customSvgUrl: null,
            svgSize: '80px',
            svgColor: '#F5A623',
            messageID: 'AjizzyLoadingMessage',
            messageFontSize: '15px',
            messageMaxLength: 34,
            messageColor: '#dcdcdc',
        });

        Notiflix.Loading.Pulse();

        if(cb !== undefined) cb(closeBlocker);
    },

    CloseBlocker() {
        Notiflix.Loading.Remove();
    },

    Success(message) {
        initNotince();
        Notiflix.Notify.Success(message);
    },

    Danger(message) {
        initNotince();
        Notiflix.Notify.Failure(message);
    },

    InfoSuccess(option, cb) {
        Notiflix.Report.Init({
            useGoogleFont: true,
            plainText: false,
            borderRadius: '10px',
        });

        Notiflix.Report.Success(
            option.title || 'Success',
            option.message || 'Operation was successful',
             option.btn || 'Ok',
                cb
        );
    },

    InfoDanger(option, cb) {
        Notiflix.Report.Init({
            useGoogleFont: true,
            plainText: false,
            borderRadius: '10px',
        });

        Notiflix.Report.Failure(
            option.title || 'Failed',
            option.message || 'Operation was not successful',
            option.btn || 'Ok',
            cb
        );
    },
    Confirm(option, cb) {
        Notiflix.Confirm.Init({
            titleColor: '#F5A623',
            okButtonColor: '#000',
            okButtonBackground: '#F5A623',
            position: 'center-top',
            borderRadius: '0px',
            plainText: false,
            width: '350px',
            useGoogleFont: true,

        });

        Notiflix.Confirm.Show(
            option.title || 'Confirmation',
            option.message || '<h3>Are you sure you want to perform this action?</h3>',
            option.yes || 'Yes',
            option.no || 'No',
            cb
            );
    }
};
