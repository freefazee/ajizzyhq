window.Vue = require('vue');
window.Aj = require('../ajizzy');

import TalentList from './TalentList';

import BModal from 'bootstrap-vue/es/components/modal/modal';
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal';
import BCarousel from 'bootstrap-vue/es/components/carousel/carousel';
import BCarouselSlide from 'bootstrap-vue/es/components/carousel/carousel-slide';

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);


Vue.component('b-carousel', BCarousel);
Vue.component('b-carousel-slide', BCarouselSlide);

let VueTruncate = require('vue-truncate-filter');
Vue.use(VueTruncate);

import { VueHammer } from 'vue2-hammer';
Vue.use(VueHammer);


new Vue({
    el: '#talents',
    components: {
        TalentList,
    }
});

