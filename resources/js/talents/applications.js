window.Vue = require('vue');
window.Aj = require('../ajizzy');

import TalentList from './ApplicationList';

import BModal from 'bootstrap-vue/es/components/modal/modal';
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal';
import BCarousel from 'bootstrap-vue/es/components/carousel/carousel';
import BCarouselSlide from 'bootstrap-vue/es/components/carousel/carousel-slide';

Vue.component('b-modal', BModal);
Vue.directive('b-modal', BModalDirective);


Vue.component('b-carousel', BCarousel);
Vue.component('b-carousel-slide', BCarouselSlide);

var VueTruncate = require('vue-truncate-filter');
Vue.use(VueTruncate);

import { VueHammer } from 'vue2-hammer';
Vue.use(VueHammer);

import TalentBasinFilter from './TalentBasinFilter';


new Vue({
    el: '#talents',
    data() {
            return {
                filters: {
                    basin: ''
                }
            }
    },

    methods: {
        basinFilter(basin) {
           this.filters.basin = basin;
        }
    },
    components: {
        TalentList,
        TalentBasinFilter
    }
});

