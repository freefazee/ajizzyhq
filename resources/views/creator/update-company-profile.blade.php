@extends('layouts.main')
@section('content')
    <div class="mid-section" role="body" id="app">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-wrapper">
                            <!-- Form controls -->
                            <div class="card">
                                <!-- Card header -->
                                <div class="card-header">
                                    <h3 class="mb-0">Hello {{auth()->user()->first_name}}, just a quick profile update</h3>
                                </div>
                                <!-- Card body -->
                                <div class="card-body">
                                    <p class="mb-0">
                                        When you post a project, we display your company details like name and description alongside other information.
                                        <br><br>

                                    </p>

                                    <form>
                                        <div class="form-group">
                                            <label class="form-control-label" for="company">What is your company name?</label>
                                            <input type="text" v-model="form.company" class="form-control" id="company" placeholder="">
                                        </div>


                                        <div class="form-group">
                                            <label class="form-control-label" for="about_company">Tell us a little about your company.</label>
                                            <textarea class="form-control" v-model="form.about_company" id="about_company" rows="3"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-control-label" for="company_location">Where is your company located in <strong>{{$country->name}}</strong>?</label>
                                            <google-autocomplete v-show="!networkRequest"
                                                                 class = "form-control icon-search"
                                                                 input_id = "txtAutocomplete"
                                                                 autocomplete="off"
                                                                 name = "s"
                                                                 placeholder = "Type here for city autocompletion"
                                                                 :config="{ type: ['geocode'], componentRestrictions: {country: '{{$country->getShortName()}}'}}"
                                            >
                                            </google-autocomplete>
                                            <p><strong v-html="form.location"></strong></p>
                                        </div>

                                        <div class="form-group">
                                            <button type="button"
                                                    class="btn float-right btn-black btn__custom mb-3"
                                                    v-html="buttonText"
                                                    :disabled="networkRequest"
                                                    v-on:click.prevent="handleSave">
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- HTML5 inputs -->

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-wrapper">


                            <!-- Selects -->
                            <div class="card">
                                <!-- Card header -->
                                <div class="card-header">
                                    <h3 class="mb-0">Need some help?</h3>
                                </div>
                                <!-- Card body -->
                                <div class="card-body">
                                    <p>Watch this short video to understand how Ajizzy works for creators.</p>

                                   <div> <iframe width="100%" height="315" src="https://www.youtube.com/embed/j4aJIcgt6QE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection



@section('footer_script')
    <script>
        window.Profile = '{!! json_encode($profile) !!}';
    </script>
    <script src="{{asset('/assets/ajizzy/js/account/creator-company-profile-update.js')}}"></script>
@endsection
