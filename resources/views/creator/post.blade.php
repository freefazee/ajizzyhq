@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body" id="app">
       <creator-form :project="{{$project}}" :categories="{{$categories}}" :meta="{{$meta}}"></creator-form>
    </div>
@endsection

@section('footer_script')
    <script src="{{asset('/assets/ajizzy/js/project/post.js ')}}"></script>
@endsection
