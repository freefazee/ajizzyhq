@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body"  id="talents">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section--header">
                    <div class="row d-flex align-items-center">
                        <div class="col-lg-10 col-md-12 col-12">
                            <div class="production-name">
                                <span>Manage applicants for:</span>
                                <div class="role-select">
                                    <h1>{{$project->title}}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 text-md-right mb-4 mb-md-0 d-none d-lg-block">
                            <a href="{{route('casting.projects.index')}}" class="btn btn-sm btn-outline-darker"><i class="fas fa-tasks"></i> Projects</a>
                            <a href="#" class="btn btn-sm btn-outline-darker"><i class="far fa-question-circle"></i> Help</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="action-bar">
                        <div class="action-bar__main">
                            <div class="action-bar__main__item">
                                <span class="icon"></span>
                                <strong>Submissions:</strong><a class="">0 unfiled</a>
                            </div>
                            <div class="action-bar__main__item">
                                <span class="icon"></span>
                                <strong>Invites:</strong><a class="">0 pending</a>
                            </div>
                            <div class="action-bar__main__item">
                                <span class="icon"></span>
                                <strong>Auditions:</strong><a class="">0 requested</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="sidebar__grid" class="col-12 col-lg-3 sidebar__col roles">
                    <div id="sidebar-nav" class="sidebar__col--roles">
                        <div class="panel-group" id="accordion-roles">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a href="#role-849526" data-toggle="collapse" data-target="#role-849526" aria-expanded="true" aria-controls="role-849526" class="sidebar__title--role">
                                        <span class="applicants--old"></span>
                                        <span class="sidebar__title--text">Tunde (0)</span>
                                        <span class="sidebar__title--chevron"><i class="fas fa-chevron-down pull-right"></i></span>
                                    </a>
                                    <div id="role-849526" data-parent="#accordion-roles" class="panel-collapse collapse" style="">
                                        <div class="panel-body font-md">
                                            <a class="sidebar__role--edit edit-role icon "></a>
                                            <ul>
                                                <li class="role_sidebar_gender "><span class="role-attribute">Gender</span>: Male</li>
                                                <li class="role_sidebar_minage "><span class="role-attribute">Min Age</span>: 18</li>
                                                <li class="role_sidebar_maxage "><span class="role-attribute">Max Age</span>: 35</li>
                                                <li class="role_sidebar_ethnicities "><span class="role-attribute">Ethnicities</span>: Black / African Descent</li>
                                                <li class="role_sidebar_type"><span class="role-attribute">Type</span>: Lead</li>
                                                <li class="select-role">
                                                    <a>Filter by this role</a>
                                                </li>
                                                <li class="toggle-role">
                                                    <a>
                                                        <span class="toggle-role__label">Close Role</span>&nbsp;
                                                        <span class="toggle-role__icon open"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a href="#" data-toggle="collapse" data-target="#role-849527" aria-expanded="true" aria-controls="role-849527" class="sidebar__title--role">
                                        <span class="applicants--old"></span>
                                        <span class="sidebar__title--text">Vanessa (0)</span>
                                        <span class="sidebar__title--chevron"><i class="fas fa-chevron-down pull-right"></i></span>
                                    </a>
                                    <div id="role-849527" data-parent="#accordion-roles" class="panel-collapse collapse" style="">
                                        <div class="panel-body font-md">
                                            <a class="sidebar__role--edit edit-role icon "></a>
                                            <ul>
                                                <li class="role_sidebar_gender "><span class="role-attribute">Gender</span>: Female</li>
                                                <li class="role_sidebar_minage "><span class="role-attribute">Min Age</span>: 18</li>
                                                <li class="role_sidebar_maxage "><span class="role-attribute">Max Age</span>: 35</li>
                                                <li class="role_sidebar_ethnicities "><span class="role-attribute">Ethnicities</span>: Black / African Descent</li>
                                                <li class="role_sidebar_type"><span class="role-attribute">Type</span>: Lead</li>
                                                <li class="select-role">
                                                    <a>Filter by this role</a>
                                                </li>
                                                <li class="toggle-role">
                                                    <a>
                                                        <span class="toggle-role__label">Close Role</span>&nbsp;
                                                        <span class="toggle-role__icon open"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar__roles--bottom d-none d-lg-block">
                            <button class="btn btn__custom  btn-sm btn-system">Add a New Role</button>
                            <button class="btn btn__custom btn-sm btn-system">Expire Your Listing</button>
                            <button class="btn btn__custom btn-sm btn-system">Icon Legend</button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-9">
                    <div class="row">

                        <div class="col-12 col-sm-12 col-md-12 main-wrapper">
                            <div class="row sort-options">
                                <div class="col-md-12">
                                    <div class="bulk-actions">
                                        <div id="select-all" class="checkbox sm">
                                            <input type="checkbox" class="checkbox__bulk" id="checkbox-ba">
                                            <label for="checkbox-ba" class="checkbox__bulk--label">Select All</label>
                                        </div>
                                        <talent-basin-filter @select="basinFilter"></talent-basin-filter>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="applicants__col col-12">
                            <talent-list endpoint="/creator/projects/{{$project->id}}/applications-feeds" :filters="filters">

                            </talent-list>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/talents/applications.js')}}"></script>
@endsection

