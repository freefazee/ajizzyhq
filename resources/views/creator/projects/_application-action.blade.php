<div class="dropdown">
    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-ellipsis-v"></i> action
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
        <a class="dropdown-item" href="?basin=green&application={{$talent->id}}">Approve</a>
        <a class="dropdown-item" href="?basin=yellow&application={{$talent->id}}">Put in review</a>
        <a class="dropdown-item" href="?basin=red&application={{$talent->id}}">Reject</a>
    </div>
</div>
