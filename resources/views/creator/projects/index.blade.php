@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section--header">
                    <div class="row">
                        <div class="col-sm-6 col-xs-7 col-12">
                            <h1 class="section-header--title border-0">Your Projects</h1>
                            <p class="pb-3">To manage applications, click on <strong>"Applications"</strong></p>
                        </div>
                        <div class="col-sm-6 col-xs-5 col-12 text-md-right mb-4 mb-md-0">
                            <a href="{{route('creator.projects.new')}}" class="btn btn-sm btn-outline-darker"><i class="far fa-plus-square"></i> Post a Job</a>
                            <a href="#" class="btn btn-sm btn-outline-darker"><i class="fas fa-question"></i> Help</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">


                    <div id="productions" class="card">
                        <div class="card-header">
                            <h5 class="h3 mb-0">My Productions (<span class="post-item-total">{{$projectsCount}}</span>)</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush list">
                               @foreach($projects as $project)
                                    <li class="list-group-item p-0">
                                        <div class="row mx-0 align-items-center panel__content--production">
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#production{{$project->id}}" aria-expanded="false" aria-controls="productionExample2"><span class="status draft">{{$project->status === 'drafted' ? '(Drafted)': ''}}</span>{{$project->title}}
                                                   </h4>
                                                <p class="text-sm mb-0">
                                                    <a class="menu-help-text"
                                                       href="{{route('creator.projects.applications', $project->id)}}">{{$project->applications_count}} application{{$project->applications_count > 1 ?  's' :  ''}}</a>
                                                </p>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12 text-lg-right">
                                                <a
                                                    class="btn btn-link btn-sm"
                                                    data-toggle="tooltip"
                                                    title="Manage applications"
                                                    href="{{route('creator.projects.applications', $project->id)}}"><i class="fas fa-tasks"></i> Applications</a>

                                                <a
                                                    href="{{route('creator.projects.show', ['id' => $project->id, 'status' => $project->status])}}"
                                                    class="btn  btn-link btn-sm"
                                                    data-toggle="tooltip"
                                                    title="Modify the project"> <i class="far fa-edit"></i> Edit</a>
                                                <a
                                                    class="btn  btn-link btn-sm"
                                                    data-toggle="tooltip"
                                                    title="Open project page"
                                                    href="{{$project->public_link}}"><i class="far fa-eye"></i> View</a>
                                            </div>
                                        </div>
                                        <div class="collapse" id="production{{$project->id}}" style="">
                                            <div class="prod__status">
                                                Status: <span class="prod__status--info">{{ucfirst($project->status)}}</span>
                                                Date Created: <span class="prod__status--info">{{$project->created_at->toDateString()}}</span>
                                                Date Expires: <span class="prod__status--info">{{$project->expires_at}}</span>
                                                Email Notification: <span class="prod__status--info">Daily Digest</span>
                                            </div>
                                        </div>
                                    </li>
                               @endforeach
                            </ul>
                        </div>
                    </div>
                    {!! $projects->links() !!}
                </div>
                <div class="col-md-4">
                   @include('account.latest-notes')
                </div>
            </div>
        </div>
    </div>
@endsection
