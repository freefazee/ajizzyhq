@extends('layouts.main')

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <div class="row">
                                <div class="col-6">
                                    <h3 class="mb-0">{{$media->mediaType->name}}</h3>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{route('creator.projects.applications', $application->laravel_through_key)}}" class="btn btn-sm btn-dark btn-round btn-icon" data-toggle="tooltip" data-original-title="Go to dashboard">
                                        <span class="btn-inner--icon"><i class="fas fa-film"></i></span>
                                        <span class="btn-inner--text">Back</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <!-- Light table -->

                        <div class="card-body">
                            <p class="card-text mb-4">{{$media->filename}}</p>
                            <a href="{{route('creator.projects.applications', $application->laravel_through_key)}}" class="btn btn-darker">Go back</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
