<form class="form-inline" action="" method="get">
    <div class="form-group mb-2">
        <label for="Role" class="sr-only">Role</label>
        <select name="role" id="Role" class="form-control form-control-sm">
            <option value="">Select Role</option>
            @foreach($project->roles as $r)
                <option value="{{$r->id}}" {{ app('request')->input('role') === $r->id ? 'selected="selected"': '' }}>{{$r->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group mb-2 mx-sm-3">
        <label for="status" class="sr-only">Status</label>
        <select name="status" id="status" class="form-control form-control-sm">
            <option value="">Status</option>
            @foreach($status as $key=>$s)
                <option value="{{$key}}" {{ app('request')->input('status') === $key ? 'selected="selected"': '' }}>{{$s}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group mx-sm-3 mb-2">
        <label for="q" class="sr-only">Query</label>
        <input type="text" class="form-control form-control-sm" name="q" value="{{app('request')->input('q')}}" id="q" placeholder="Search">
    </div>
    <button type="submit" class="btn btn-darker mb-2 btn-sm" name="action" value="filter">Filter</button>
    <button type="submit" class="btn btn-darker mb-2 btn-sm" name="action" value="download"><i class="fas fa-download"></i> Download</button>
    <a href="{{route('creator.projects.applications', $project->id)}}" class="btn btn-secondary mb-2 btn-sm" name="filter">Reset</a>
</form>
