@extends('layouts.main')

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <div class="row">
                                <div class="col-6">
                                    <h3 class="mb-0">Applications: {{$project->title}}</h3>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{route('casting.projects.index')}}" class="btn btn-sm btn-dark btn-round btn-icon" data-toggle="tooltip" data-original-title="Go to dashboard">
                                        <span class="btn-inner--icon"><i class="fas fa-film"></i></span>
                                        <span class="btn-inner--text">Projects</span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    To approve/reject an application, click on "action".
                                </div>

                                <div class="col-12">
                                    <br />
                                    @include('creator.projects._application-filter-form')
                                </div>
                            </div>

                        </div>
                        <!-- Light table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush table-striped">
                                <thead class="thead-light">
                                <tr>
                                    <th>Talent Details</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th>Media</th>
                                    <th>Applied On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                @foreach($applications as $talent)
                                <tr>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <a href="#" class="avatar rounded-circle mr-3">
                                                @if($talent->owner->profile->medias)
                                                    <img alt="{{$talent->owner->profile->stage_name}}" src="{{$talent->owner->profile->medias[0]->url}}">
                                                @else
                                                    no avatar
                                                @endif
                                            </a>
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm"><a href="{{route('public.profile', $talent->owner->profile->profile_url)}}" target="_blank">{{$talent->owner->profile->stage_name}}</a></span>
                                                <br />
                                                Role: {{$talent->role->name}}
                                            </div>
                                        </div>
                                    </th>
                                    <td class="budget">
                                        <a href="mailto:{{$talent->owner->email}}">{{$talent->owner->email}}</a><br />
                                        <a href="tel:{{$talent->owner->phone}}">{{$talent->owner->phone}}</a>
                                    </td>
                                    <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-{{$talent->getBasinClass()}}"></i>
                                        <span class="status">{{$talent->getBasinMessage()}}</span>
                                      </span> @include('creator.projects._application-action')
                                    </td>
                                    <td>
                                        @if($talent->medias)
                                        <div class="avatar-group">
                                            @foreach($talent->medias as $m)
                                            <a href="?media={{$m->id}}&application={{$talent->id}}" target="{{$m->mediaType->input_type === 'upload' ? '_blank' : '_self'}}" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="{{$m->mediaType->name}}">
                                                @if($m->mediaType->input_type === 'upload')
                                                    <i class="fas fa-download"></i>
                                                @else
                                                    <i class="fas fa-file-alt"></i>
                                                @endif
                                            </a>
                                            @endforeach
                                        </div>
                                         @else
                                            <strong>No Media</strong>
                                       @endif
                                    </td>

                                    <td>
                                        <div class="d-flex align-items-center">{{$talent->formatted_created}}</div>
                                    </td>
                                    <td class="text-right">

                                    </td>
                                </tr>
                                  @endforeach
                                </tbody>
                            </table>
                            {!! $applications->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
