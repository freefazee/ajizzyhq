<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.main_meta')
    <!-- Icons -->
    <link href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <!-- Ajizzy CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/ajizzy.css')}}">
</head>
<body class="page page-auth page-login">

@yield('content')
<script type="text/javascript" src="{{asset('assets/js/ajizzy.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendor/select2/dist/js/select2.full.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.select-dd').select2();
    });
</script>
</body>
</html>
