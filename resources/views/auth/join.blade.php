@extends('layouts.main')

@section('title')Join Ajizzy Platform | @parent @endsection

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">
                        <div class="section-header text-center m-0 pt-0">
                            <h1 class="section-header--title border-0 mb-0 pb-0">
                                Upgrade to premium.
                            </h1><p>Access unlimited jobs by upgrading to premium today.</p>
                        </div>
                        <div class="section-body user-benefits">
                            <div class="inner-wrapper">

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active">


                                        <div class="row">

                                            <div class="card card-pricing border-0 text-center mb-4 col-sm-8 offset-sm-2">
                                                <div class="card-header bg-transparent plan--header">
                                                    <h4 class="text-uppercase ls-1 py-3 mb-0" style="color: #f5a623;">{{$price->name}}</h4>
                                                </div>
                                                <div class="card-body px-lg-7">
                                                    <div class="display-2">{{$price->country->getCurrency()}}{{$price->formatted_price}}</div>
                                                    <span class=" text-muted">billed monthly per user.</span>

                                                    <p class="card-text" style="text-align: left;">
                                                        <div style="margin-bottom: 10px;">
                                                        As an Ajizzy premium customer, you can apply to unlimited jobs. You will receive FREE notification for each new job posted on the platform.
                                                    </div>


                                                    <p style="margin-top: 15px; margin-bottom: 15px;">
                                                        Apply to any job, make a deal, and keep 100% of all the proceeds.
                                                    </p>


                                                    <a href="{{request('changePackage') ? route('account.change.package', $price->id) : route('talent.join-show')}}?plan={{$price->id}}" class="btn btn-darker mb-3" >Upgrade me</a>
                                                </div>
                                                <div class="card-footer">
                                                    <a href="{{route('casting-calls')}}" class=" text-muted">See job postings</a>
                                                </div>
                                            </div>

                                        </div>

                                        <h3 class="mt-4 mt-md-5 text-center">Looking to cast talents? <a href="{{route('creator.join')}}" style="text-decoration: underline;">Click here</a> to get started.</h3>
                                        <div id="why-become-member">
                                            <h3 class="text-center">Why become an Ajizzy member?</h3>
                                            <div class="row justify-content-center text-center">
                                                <div class="col-md-4 col-sm-4 mb-4 mb-sm-0">
                                                    <h5>Find your role</h5>
                                                    <p>Apply to over 10,000 jobs and find the roles you love.</p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 mb-4 mb-sm-0">
                                                    <h5>Get Discovered</h5>
                                                    <p>Build your profile and get scouted by industry pros casting now.</p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 mb-4 mb-sm-0">
                                                    <h5>Get Cast</h5>
                                                    <p>Land the role, take your place, and make your mark.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END .mid-section -->
@endsection
