@extends('auth.auth-layout')

@section('title') Join As Creator | Ajizzy @endsection

@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-lg-7">
                    <div class="signup">
                        <div class="signup--wrapper">
                            <div class="brand">
                                <a href="/">
                                    <img src="{{asset('assets/images/ajizzy--new-black.svg')}}" alt="Ajizzy.com">
                                </a>
                            </div>
                            <h4 class="section--title-small mb-5">
                                Sign Up for Creators
                                <span class="signup--title-description">Create an account to start casting. It's free!</span>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </h4>
                            <form method="post" action="{{route('creator.join.store')}}">
                                @csrf
                                <input type="hidden" name="account_type" value="creator">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-label-group">
                                            <input type="text" id="firstname" name="first_name" value="{{old('first_name')}}" class="form-control" placeholder="First Name">
                                            <label for="firstname">First Name</label>
                                            @if($errors->has('first_name'))
                                                <p class="invalid-feedback" style="display:block">{{$errors->first('first_name')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-label-group">
                                            <input type="text" id="lastname" name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="First Name">
                                            <label for="lastname">Last Name</label>
                                            @if($errors->has('last_name'))
                                                <p class="invalid-feedback" style="display:block">{{$errors->first('last_name')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-label-group">
                                    <input type="email" id="inputEmail" class="form-control" name="email" value="{{old('email')}}" placeholder="Email address" >
                                    <label for="inputEmail">Email address</label>
                                    @if($errors->has('email'))
                                        <p class="invalid-feedback" style="display:block">{{$errors->first('email')}}</p>
                                    @endif
                                </div>

                                <div class="form-label-group">
                                    <input type="password" id="inputPassword" class="form-control" name="password" value="{{old('password')}}" placeholder="Password" >
                                    <label for="inputPassword">Password</label>
                                    @if($errors->has('password'))
                                        <p class="invalid-feedback" style="display:block">{{$errors->first('password')}}</p>
                                    @endif
                                </div>

                                <div class="form-label-group">
                                    <label for="" class="label-reset">What best describe your occupation?</label>
                                    <select name="occupation_id" id="occupation_id" class="form-control">
                                        <option value="" disabled selected>Select Occupation</option>
                                        @foreach($occupations as $occupation)
                                            <option value="{{$occupation->id}}" @if(old('occupation_id') == $occupation->id) selected="selected" @endif>{{$occupation->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('occupation_id'))
                                        <p class="invalid-feedback" style="display:block">{{$errors->first('occupation_id')}}</p>
                                    @endif
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jobTitle" class="form-control" name="title" value="{{old('title')}}" placeholder="What`s your job title?" >
                                    <label for="jobTitle">What`s your job title?</label>
                                    @if($errors->has('title'))
                                        <p class="invalid-feedback" style="display:block">{{$errors->first('title')}}</p>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group">
                                            <label for="" class="label-reset">Select your country</label>
                                            <select name="country" id="country" class="form-control select-dd">
                                                <option value="" disabled selected>Select Country</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->country}}" @if(old('country') == $country->country || session('location')->code == $country->country) selected="selected" @endif>{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('country'))
                                                <p class="invalid-feedback" style="display:block">{{$errors->first('country')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-label-group">
                                            <label for="" class="label-reset">Enter your phone number</label>
                                            <input type="text" id="phone" class="form-control" name="phone" value="{{old('phone')}}" placeholder="phone number" >
                                            @if($errors->has('phone'))
                                                <p class="invalid-feedback" style="display:block">{{$errors->first('phone')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-check mt-4 mb-1">
                                    <input type="checkbox" class="form-check-input" value="1" id="savedCasting" name="terms_and_condition" @if(old('terms_and_condition') == '1') checked="checked" @endif>
                                    <label for="savedCasting" class="form-check-label">I have read and agreed to the Ajizzy Terms of Service, and I am currently at least 16 years old.</label>
                                    @if($errors->has('terms_and_condition'))
                                        <p class="invalid-feedback" style="display:block">{{$errors->first('terms_and_condition')}}</p>
                                    @endif
                                </div>

                                <button class="btn  btn__custom btn-orange btn-login text-uppercase mt-4 mr-sm-3" type="submit">Join now</button>
                                <a href="{{route('login')}}" class="btn btn__custom btn-ajizzy-outline-black btn-login text-uppercase mt-4">Login</a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="d-none d-lg-flex col-md-5 bg-image"></div>
            </div>
        </div>
    </div>
@endsection
