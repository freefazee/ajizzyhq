@extends('layouts.main')

@section('title')Select Subscription Plan @endsection

@section('content')

    <div class="mid-section" id="app">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">

                        <div class="section-body user-benefits">
                            <div class="inner-wrapper">

                                <!-- Tab panes -->
                                <order-subscription-form :ranges="{{$paymentRanges}}" :pricing-info="{{$pricingInfo}}">
                                    Loading Subscription Form  <i class="fas fa-spinner fa-spin"></i>
                                </order-subscription-form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END .mid-section -->
@endsection

@section('footer_script')
    <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
    <script src="{{asset('/assets/ajizzy/js/account/order-subscription.js')}}"></script>
@endsection
