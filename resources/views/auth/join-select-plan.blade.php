@extends('layouts.main')

@section('title')Select Payment Plan | @parent @endsection

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">
                        <div class="section-header text-center">
                            <h1 class="section-header--title border-0">
                                Select a plan to get started
                            </h1>
                        </div>

                        <div class="section-body user-benefits">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="wrap bg-white">
                                        <div class="plan--header text-center">
                                            <h3 class="plan--name">Starter</h3>
                                            <p class="plan--name-description mb-0">
                                                Ideal package for youngsters and students trying
                                                out the platform.
                                            </p>
                                        </div>
                                        <div class="plan--cta border-top text-center">
                                            <span class="price">N2000</span>
                                            <span class="duration">billed monthly per user</span>
                                            <a href="{{route('talent.join-show')}}?plan=starter" class="trigger-next btn btn-block btn-orange">Get Started</a>
                                            <a href="#" class="modal--more-link" data-toggle="modal" data-target="#starterDetails">What's included?</a>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade" id="starterDetails" tabindex="-1" role="dialog" aria-labelledby="starterDetailsLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="plan--benefits">
                                                            <h6 class="text-center small mb-4">What's included:</h6>
                                                            <ul class="benefit-list">
                                                                <li>Build professional profile</li>
                                                                <li>Upload media (photo, video, audio)</li>
                                                                <li>Receive maximum of 2 deals a month</li>
                                                                <li>Get paid directly to you bank account (getting your movey via our platform means clients can't disappoint you after work)</li>
                                                                <li>Maximum of 2 contacts monthly by brands, agencies and movie makers</li>
                                                                <li>Free pass to 2 live events/auction in a year</li>
                                                                <li>View profile detials of 2 visitors to your profile around the world monthly</li>
                                                                <li>View profile details of 2 visitors to your profile around the world monthly</li>
                                                                <li>Apply to 2 jobs monthly</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="wrap bg-orange">
                                        <div class="plan--header text-center">
                                            <h3 class="plan--name">Pro</h3>
                                            <p class="plan--name-description mb-0">
                                                Perfect solution for Elites. All features on
                                                starter plan plus more.
                                            </p>
                                        </div>
                                        <div class="plan--cta border-top text-center">
                                            <span class="price">N5000</span>
                                            <span class="duration">billed montly per user</span>
                                            <a href="{{route('talent.join-show')}}?plan=pro" class="trigger-next btn btn-block btn-black">Start with Pro</a>
                                            <a href="#" class="modal--more-link" data-toggle="modal" data-target="#proDetails">What's included?</a>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade" id="proDetails" tabindex="-1" role="dialog" aria-labelledby="proDetailsLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="plan--benefits">
                                                            <h6 class="text-center small mb-4">What's included:</h6>
                                                            <ul class="benefit-list">
                                                                <li>Build professional profile</li>
                                                                <li>Upload media (photo, video, audio)</li>
                                                                <li>Receive unlimited deals a month</li>
                                                                <li>Get paid directly to you bank account (getting your movey via our platform means clients can't disappoint you after work)</li>
                                                                <li>Unlimited contacts monthly by brands, agencies and movie makers etc.</li>
                                                                <li>Generate unlimited invoices monthly </li>
                                                                <li>Free pass to all our live events/auctions</li>
                                                                <li>View profile details of all visitors to your profile from around the world</li>
                                                                <li>Apply to unlimited jobs monthly</li>
                                                                <li>Unlimited international deals</li>
                                                                <li>Receive priority notifications on new jobs</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-5 text-center">Looking to cast talents? <a href="signup-creators.html">Click here</a> to get started.</p>
                            <div id="why-become-member">
                                <h3 class="text-center">Why become an Ajizzy member?</h3>
                                <div class="row justify-content-center text-center">
                                    <div class="col-md-4">
                                        <h5>Find your role</h5>
                                        <p>Apply to over 10,000 jobs and find the roles you love.</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>Get Discovered</h5>
                                        <p>Build your profile and get scouted by industry pros casting now.</p>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>Get Cast</h5>
                                        <p>Land the role, take your place, and make your mark.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
