@extends('auth.auth-layout')

@section('title')Reset Password @endsection

@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-5">
                    <div class="login d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-lg-8 mx-auto">
                                    <div class="brand">
                                        <a href="{{route('home')}}">
                                            <img src="{{asset('assets/images/ajizzy--new-black.svg')}}" alt="">
                                        </a>
                                    </div>
                                    <h4 class="section--title-small mb-5">Set New Password</h4>
                                         @include('partials._flash-messages')

                                    <form method="post" action="{{route('auth.reset-password-process', $token)}}">
                                            @csrf

                                            <div class="form-label-group">
                                                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" >
                                                <label for="inputPassword">Password</label>
                                                @if($errors->has('password'))
                                                    <p class="invalid-feedback" style="display:block">{{$errors->first('password')}}</p>
                                                @endif
                                            </div>


                                            <div class="form-label-group">
                                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Password Confirmation" >
                                                <label for="password_confirmation">Password Confirmation</label>
                                                @if($errors->has('password_confirmation'))
                                                    <p class="invalid-feedback" style="display:block">{{$errors->first('password_confirmation')}}</p>
                                                @endif
                                            </div>

                                            <div class="text-right pb-5"><a class="small" href="{{route('auth.forgot-password')}}">Make a new password request.</a></div>
                                            <button class="btn btn__custom btn-lg btn-orange btn-block btn-login text-uppercase font-weight-bold mb-4" type="submit"onclick="this.innerText = 'Please wait..';" o>SAVE NEW PASSWORD</button>
                                            <p>Need an Ajizzy account? <a href="{{route('talent.join-show')}}">Create an account</a>.</p>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 bg-image">
                    <div class="brand text-right mr-5 mt-5">
                        <a href="#">
                            <img src="{{asset('images/ajizzy--brand.png')}}" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
