@extends('auth.auth-layout')

@section('title')Reset Password @endsection

@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-5">
                    <div class="login d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-lg-8 mx-auto">
                                    <div class="brand">
                                        <a href="{{route('home')}}">
                                            <img src="{{asset('assets/images/ajizzy--new-black.svg')}}" alt="">
                                        </a>
                                    </div>

                                    <h4 class="section--title-small">Forgot your password?</h4>

                                       @include('partials._flash-messages')

                                        <form method="post" action="{{route('auth.forgot-password-post')}}">
                                            @csrf
                                            <div class="form-label-group">
                                                <input type="email" value="{{old('email')}}" name="email" id="inputEmail" class="form-control" placeholder="Enter your email address"  autofocus>
                                                <label for="inputEmail">Your Email Address</label>
                                                @if($errors->has('email'))
                                                    <p class="invalid-feedback" style="display:block">{{$errors->first('email')}}</p>
                                                @endif
                                            </div>


                                            <button class="btn btn__custom btn-lg btn-orange btn-block btn-login text-uppercase font-weight-bold mb-4" type="submit"onclick="this.innerText = 'Please wait..';" o>RESET MY PASSWORD</button>
                                            <p><a href="{{route('login')}}">← Back to sign in</a>.</p>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 bg-image">
                    <div class="brand text-right mr-5 mt-5">
                        <a href="#">
                            <img src="{{asset('images/ajizzy--brand.png')}}" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
