@extends('auth.auth-layout')

@section('title') Login | Ajizzy @endsection

@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-5">
                    <div class="login d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-lg-8 mx-auto">
                                    <div class="brand">
                                        <a href="{{route('home')}}">
                                            <img src="{{asset('assets/images/ajizzy--new-black.svg')}}" alt="">
                                        </a>
                                    </div>
                                    <h4 class="section--title-small mb-5">Log in to your account</h4>
                                        @include('partials._flash-messages')

                                        <form method="post" action="{{route('login-process')}}">
                                            @csrf
                                            <div class="form-label-group">
                                                <input type="email" value="{{old('email')}}" name="email" id="inputEmail" class="form-control" placeholder="Email address"  autofocus>
                                                <label for="inputEmail">Email address</label>
                                                @if($errors->has('email'))
                                                    <p class="invalid-feedback" style="display:block">{{$errors->first('email')}}</p>
                                                @endif
                                            </div>

                                            <div class="form-label-group">
                                                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" >
                                                <label for="inputPassword">Password</label>
                                                @if($errors->has('password'))
                                                    <p class="invalid-feedback" style="display:block">{{$errors->first('password')}}</p>
                                                @endif
                                            </div>

                                            <div class="text-right pb-5"><a class="small" href="{{route('auth.forgot-password')}}">Forgot password?</a></div>
                                            <button class="btn btn__custom btn-lg btn-orange btn-block btn-login text-uppercase font-weight-bold mb-4" type="submit"onclick="this.innerText = 'Please wait..';" o>Sign in</button>
                                            <p>Need an Ajizzy account? <a href="{{route('talent.join-show')}}">Create an account</a>.</p>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 bg-image">
                    <div class="brand text-right mr-5 mt-5">
                        <a href="#">
                            <img src="{{asset('images/ajizzy--brand.png')}}" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
