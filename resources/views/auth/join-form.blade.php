@extends('layouts.main')

@section('title')Join Ajizzy Platform | @parent @endsection

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">
                        <div class="section-body user-benefits">
                            <div class="inner-wrapper">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane bg-white active">
                                        <div class="row d-flex justify-content-center">

                                            <form class="col-md-10 my-4" method="post" action="{{route('talent.join')}}">
                                                @if(request('plan') !== '-1')
                                                <input type="hidden" name="plan" value="{{request('plan')}}">
                                                @endif
                                                @csrf
                                                <div class="sub-section--header">
                                                    <h4>Step 2: User Information</h4>
                                                    @if($errors->has('plan'))
                                                        <div class="alert alert-danger">{{$errors->first('plan')}}</div>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-label-group">
                                                            <input type="text" id="inputFirstName"
                                                                    name="first_name"
                                                                   value="{{old('first_name')}}"
                                                                   class="form-control  {{ $errors->has('first_name') ? 'is-invalid' : ''}}"
                                                                   placeholder="First Name"  autofocus/>
                                                            @if($errors->has('first_name'))
                                                               <p class="invalid-feedback">{{$errors->first('first_name')}}</p>
                                                           @endif

                                                            <label for="inputFirstName">First Name</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-label-group">
                                                            <input type="text" id="inputLastName"
                                                                   name="last_name"
                                                                   value="{{old('last_name')}}"
                                                                   class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}"
                                                                   placeholder="Last Name"/>
                                                            @if($errors->has('last_name'))
                                                                <p class="invalid-feedback">{{$errors->first('last_name')}}</p>
                                                            @endif
                                                            <label for="inputLastName">Last Name</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-label-group">
                                                    <input type="email"
                                                            id="inputEmail"
                                                           class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}"
                                                           value="{{old('email')}}"
                                                           name="email" placeholder="Email address"/>
                                                    @if($errors->has('email'))
                                                        <p class="invalid-feedback">{{$errors->first('email')}}</p>
                                                    @endif

                                                    <label for="inputEmail">Email address</label>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-label-group">

                                                            <select name="country"
                                                                    id="country"
                                                                    class="form-control select-dd {{ $errors->has('country') ? 'is-invalid' : ''}}">
                                                                <option value="">Select your country</option>
                                                                @foreach($countries as $country)
                                                                    <option
                                                                        value="{{$country->country}}" @if(old('country') == $country->country
                                                                                    || session('location')->code == $country->country)
                                                                                selected="selected" @endif>{{$country->name}}</option>
                                                                    @endforeach
                                                            </select>
                                                            @if($errors->has('country'))
                                                                <p class="invalid-feedback">{{$errors->first('country')}}</p>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group input-group-merge">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-globe-americas"></i></span>
                                                                </div>
                                                                <input class="form-control {{ $errors->has('phone') ? 'is-invalid' : ''}}"
                                                                       name="phone"
                                                                       placeholder="Phone number"
                                                                       value="{{old('phone')}}"
                                                                       type="text">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                                                </div>
                                                                @if($errors->has('phone'))
                                                                    <p class="invalid-feedback">{{$errors->first('phone')}}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-label-group">
                                                            <input type="password"
                                                                   id="inputPassword"
                                                                   name="password"
                                                                   class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}"
                                                                   placeholder="Password"/>
                                                            @if($errors->has('password'))
                                                                <p class="invalid-feedback">{{$errors->first('password')}}</p>
                                                            @endif
                                                            <label for="inputPassword">New Password</label>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="custom-control custom-checkbox mt-4 mb-1">
                                                    <input class="custom-control-input"
                                                           id="terms_and_condition" value="1" type="checkbox" name="terms_and_condition" @if(old('terms_and_condition'))  checked="checked" @endif>
                                                    <label class="custom-control-label" for="terms_and_condition">I have read and agree to the Terms of Use</label>
                                                    @if($errors->has('terms_and_condition'))
                                                        <p class="invalid-feedback" style="display:block">{{$errors->first('terms_and_condition')}}</p>
                                                    @endif
                                                </div>

                                                <div class="custom-control custom-checkbox mb-5">
                                                    <input type="checkbox" class="custom-control-input" value="1" id="age_verification" name="age_verification" @if(old('age_verification'))  checked="checked" @endif />

                                                    <label for="age_verification" class="custom-control-label">I am currently at least {{config('ajizzy.min_age')}} years old. How do I sign my child up for Ajizzy?</label>
                                                    @if($errors->has('age_verification'))
                                                        <p class="invalid-feedback" style="display:block">{{$errors->first('age_verification')}}</p>
                                                    @endif
                                                </div>
                                                <button type="submit" class="trigger-payment btn__custom btn btn-block btn-orange">Next</button>
                                                <span class="back-link"><a href="{{route('talent.join-show')}}" class="trigger-previous">&larr; Back to Step 1: Subscription Plan</a></span>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END .mid-section -->
@endsection
