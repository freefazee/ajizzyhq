@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">
                        <div class="section-header">
                            <h1 class="section-header--title">
                                <small>My account</small>
                                Account settings
                            </h1>
                        </div>
                        <div class="section-body">
                            <div class="sub-section sub-section--basic-information">
                                <h3 class="section-title">Basic information</h3>
                                <div class="section-content">
                                    <dl class="row">
                                        <dt class="col-md-5">Name:</dt>
                                        <dd class="col-md-7">{{$me->getFullname()}}</dd>
                                        <dt class="col-md-5">Email:</dt>
                                        <dd class="col-md-7">{{$me->email}}</dd>
                                        <dt class="col-md-5">Password:</dt>
                                        <dd class="col-md-7">
                                            ******* <a href="#" data-toggle="modal" data-target="#modal-form">(Change password)</a>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="sub-section sub-section--basic-information">
                                <h3 class="section-title">Additional information</h3>
                                <div class="section-content">
                                    <dl class="row">
                                        @if($me->profile->occupation)
                                        <dt class="col-md-5">Occupation:</dt>
                                        <dd class="col-md-7">{{$me->profile->occupation}}</dd>
                                        @endif
                                        @if($me->profile->title)
                                        <dt class="col-md-5">Professional/Working title:</dt>
                                        <dd class="col-md-7">{{$me->profile->title}}</dd>
                                        @endif

                                         @if($me->profile->company)
                                        <dt class="col-md-5">Company name:</dt>
                                        <dd class="col-md-7">{{$me->profile->company}}</dd>
                                         @endif
                                        <dt class="col-md-5">Phone:</dt>
                                        <dd class="col-md-7">{{$me->phone}}</dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="sub-section sub-section--creative_profile">
                                <h5>Profile</h5>
                                <p>
                                    Keep your profile updated at all time.
                                    <a href="/account">Update profile now!</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 aside">
                        @include('account.settings-sidebar')
                </div>
            </div>
        </div>
    </div>
    <!-- END .mid-section -->


    <div class="row">

        <div class="col-md-4">

            <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                    <div class="modal-content">

                        <div class="modal-body p-0">


                            <div class="card bg-secondary shadow border-0">
                                <div class="card-header bg-transparent pb-5">
                                    <h4 class="text-muted text-center mt-2 mb-3">Change Password</h4>
                                </div>
                                <div class="card-body px-lg-5 py-lg-5">

                                    <form role="form" id="change-password-form">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="Current Password" id="current_password" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" id="new_password" placeholder="New Password" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" id="new_password_confirm" placeholder="Re-type New Password" type="password">
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn__custom btn-orange my-4">Change Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')

    <script src="{{asset('assets/ajizzy/js/account/settings.js')}}"></script>

@endsection

