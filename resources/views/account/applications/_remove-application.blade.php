<a onclick="return confirm('Are you sure you want to remove yourself from this project?')" href="?remove={{$application->id}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Remove yourself">
    <i class="fas fa-trash"></i>
</a>
