@extends('layouts.main')

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <div class="row">
                                <div class="col-6">
                                    <h3 class="mb-0">Applications</h3>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{route('account.dashboard')}}" class="btn btn-sm btn-dark btn-round btn-icon" data-toggle="tooltip" data-original-title="Go to dashboard">
                                        <span class="btn-inner--icon"><i class="fas fa-tachometer-alt"></i></span>
                                        <span class="btn-inner--text">Dashboard</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Light table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush table-striped">
                                <thead class="thead-light">
                                <tr>
                                    <th>Role Info</th>
                                    <th>Status</th>
                                    <th>Applied On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($applications as $application)
                                <tr>
                                    <td class="table-user">
                                        <strong>{{$application->role->name}} ({{$application->role->gender}}) {{$application->role->min_age}} - {{$application->role->max_age}}</strong> @include('account.applications._remove-application') <br /> <a href="{{route('single.casting-call', $application->role->project->id)}}">{{$application->role->project->title}}</a> <small>{{$application->role->project->company}}</small>
                                    </td>
                                    <td>
                                        <span class="text-muted">
                                            <label for="" class="badge badge-{{$application->getBasinClass()}}">{{$application->getBasinMessage()}}</label>
                                        </span>
                                    </td>
                                    <td>
                                        {{$application->formatted_created}}
                                    </td>
                                    <td class="table-actions">
                                        @include('account.applications._remove-application')
                                    </td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
