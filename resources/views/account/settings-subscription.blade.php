@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 main-wrapper">
                    <div class="entry">
                        <div class="section-header">
                            <h1 class="section-header--title">
                                <small>My account</small>
                                Subscription Info
                            </h1>
                        </div>
                        <div class="section-body">
                            @if(!auth()->user()->profile->subscriptionActive())
                            <p>
                                Oops! It seems your subscription has expired. It looks like you
                                don’t currently have an active Ajizzy subscription. Click
                                the button below to upgrade to premium.
                            </p>
                            <a href="{{route('join.plan-selection')}}" class="btn btn__custom btn-orange mt-1">Renew subscription</a>
                            @else
                                <p>Your Ajizzy subscription expires on <strong class="font-weight-bold">{{auth()->user()->profile->subscription_expires}}</strong>.
                                    <br /> <br />Do you wish to extend the expiry date? <a href="{{route('join.plan-selection')}}">click here</a>.</p>
                            @endif

                            <h1 class="section--header--title mb-4" style="margin-top: 20px;">Latest Transactions</h1>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Ref</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($transactions as $trans)
                                        <tr>
                                            <td>{{$trans->formattedAmount}}</td>
                                            <td>{{$trans->created_at}}</td>
                                            <td>{{$trans->txf}}</td>
                                            <td>{{$trans->qty}}</td>
                                            <td><a href="?action=check&ref={{$trans->txf}}&method={{$trans->method}}">{{$trans->status}}</a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {!! $transactions->links() !!}
                                </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 aside">
                    @include('account.settings-sidebar')
                </div>
            </div>
        </div>
    </div>
    <!-- END .mid-section -->


    <div class="row">

        <div class="col-md-4">

            <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                    <div class="modal-content">

                        <div class="modal-body p-0">


                            <div class="card bg-secondary shadow border-0">
                                <div class="card-header bg-transparent pb-5">
                                    <h4 class="text-muted text-center mt-2 mb-3">Change Password</h4>
                                </div>
                                <div class="card-body px-lg-5 py-lg-5">

                                    <form role="form" id="change-password-form">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="Current Password" id="current_password" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" id="new_password" placeholder="New Password" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                                </div>
                                                <input class="form-control" id="new_password_confirm" placeholder="Re-type New Password" type="password">
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn__custom btn-orange my-4">Change Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')

    <script src="{{asset('assets/ajizzy/js/account/settings.js')}}"></script>

@endsection

