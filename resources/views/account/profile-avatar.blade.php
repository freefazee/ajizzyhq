@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body" id="app">
        <account-avatar></account-avatar>
    </div>
    <!-- END .mid-section -->
@endsection

@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/account/photo-avatars.js')}}"></script>

@endsection

