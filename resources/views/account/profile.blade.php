@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body" id="app">
        <account-profile-form>
            Loading..
        </account-profile-form>
    </div>
    <!-- END .mid-section -->
@endsection

@section('footer_script')
    <script src="{{asset('assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/ajizzy/js/account/profile-form.js')}}"></script>

@endsection

