@extends('layouts.main')

@section('content')

    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section--header" style="margin-bottom: 25px;">
                    <div class="row">
                        <div class="col-sm-12 col-xs-7 col-12">
                            <h3 class="section-header--title border-0">Verify you`re a human to continue.</h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="h3 mb-0">Email Verification</h5>
                        </div>
                        <div class="card-body">
                            <div class="card-text mb-4">

                                <p> <button type="button" class="btn btn-{{auth()->user()->emailVerified() ? 'success' : 'danger'}} btn-icon-only rounded-circle">
                                        <span class="btn-inner--icon"><i class="fas fa-{{auth()->user()->emailVerified() ? 'check' : 'ban'}}"></i></span>
                                    </button>
                                    Your email <strong>{{auth()->user()->email}}</strong> has {{auth()->user()->emailVerified() ? 'been verified' : 'not been verified'}}.
                                    @if(!auth()->user()->emailVerified())
                                    <a href="?resend=true" class="text-sm">Resend</a>
                                        <br><br>
                                        <small class="bg-warning text-white p-1">
                                            <strong>You may want to check your junk/spam folder. Remember to mark all our emails as 'Not Spam'.</strong></small>
                                    @endif
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
