@if(auth()->user()->isTalent())
    <div id="viewed-applications">
        <div class="card">
            <div class="card-header">
                <h5 class="h3">Latest Jobs</h5>
            </div>
            <div class="card-body">
                @foreach($latestProjects as $project)
                    <div class="col-md-12 mb-12 mb-md-0">
                        <div class="column-wrapper">
                            <h6 class="h6--small-header category-title">
                                <a href="{{route('casting-calls')}}?category={{$project->category->id}}">{{$project->category->name}}</a>
                            </h6>
                            <h3 class="h3--smaller-header post-title">
                                <a href="{{route('single.casting-call', $project->id)}}">{{$project->title}}</a>
                            </h3>
                            <div class="meta">
                                <span class="location">{{$project->loc}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
