
@if(!auth()->user()->isTalent() && isset($notedTalents))
    <div id="notes">
    <div class="card">
        <div class="card-header">
            <h5 class="h3">Your Latest Notes</h5>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush list my--3">

                @foreach($notedTalents as $notedTalent)
                <li class="list-group-item px-0">
                    <div class="row align-items-center">
                        <div class="col-auto">
                            <a href="{{route('public.profile', $notedTalent->owner->id)}}" class="avatar">
                                <img alt="{{$notedTalent->owner->profile->stage_name}}" src="{{$notedTalent->owner->getAvatarUrl()}}">
                            </a>
                        </div>
                        <div class="col ml--2 notes-details">
                            <div class="mb-0 notes-details--name">
                                <a href="{{route('public.profile', $notedTalent->owner->id)}}">{{$notedTalent->owner->profile->stage_name}}</a>
                            </div>
                            <div class="notes-details--activity">You wrote on {{$notedTalent->created_at->format('Y/m/d')}}</div>
                            <div class="notes-details--message">{{str_limit($notedTalent->body,35)}}</div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endif
