@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.0.2/dist/style.css">
@endsection

@section('content')

    <div class="main__container staticpages" id="app">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-sm-12">
                   <div class="card">
                       <div class="card-header" id="builder-holder">
                           <h5 class="h3 mb-0">Profile Builder</h5>
                       </div>
                       <div class="card-body">
                           <router-view></router-view>
                       </div>
                   </div>
                </div>

                <div class="col-md-4 aside">
                    <div class="widget widget--no-padding widget-similar-links">
                        <div class="list-group">
                            <router-link to="/" class="list-group-item list-group-item-action">Basic Information</router-link>
                            <router-link to="/appearance" class="list-group-item list-group-item-action">Appearance</router-link>
                            <router-link to="/photos" class="list-group-item list-group-item-action">Profile Photos</router-link>
                            <router-link to="/location" class="list-group-item list-group-item-action">Location</router-link>
                            <router-link to="/education" class="list-group-item list-group-item-action">Education</router-link>

                            <router-link to="/skills" class="list-group-item list-group-item-action">Skills</router-link>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_script')

    <script src="{{asset('assets/ajizzy/js/account/builder.js')}}"></script>

@endsection

