@extends('layouts.main')

@section('content')
    <div class="mid-section" role="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section--header" style="margin-bottom: 25px;">
                    <div class="row">
                        <div class="col-sm-6 col-xs-7 col-12">
                            <h3 class="section-header--title border-0">Welcome {{auth()->user()->name}}</h3>
                        </div>
                        <div class="col-sm-6 col-xs-5 col-12 text-md-right mb-4 mb-md-0">
                            <a href="{{route('creator.projects.new')}}" class="btn btn-sm btn-outline-dark"><i class="far fa-plus-square"></i> Post a Job</a>
                            <a href="{{route('casting-calls')}}" class="btn btn-sm btn-outline-dark"><i class="fas fa-search"></i> Find Job</a>
                            <a href="/account/profile" class="btn btn-sm btn-outline-dark">@if(auth()->user()->profile->built()) Update Profile <i class="fas fa-check-square"></i> @else Build Your Profile @endif</a>
                            <a href="#" class="btn btn-sm btn-dark"><i class="fas fa-question"></i> Help</a>
                        </div>
                    </div>
                    @if(auth()->user()->showProfileInvisibilityWarning())
                       <div class="row">
                           <div class="col-md-12">
                               <div class="card bg-danger">
                                   <div class="card-body">
                                       <h3 class="card-title text-white">Account Not Visible</h3>
                                       <blockquote class="blockquote text-white mb-0">
                                           <p>Your account is currently not visible. You can start by <a href="{{route('account.profile.avatar')}}" class="btn btn-secondary btn-sm">adding some photos</a> and ensure it is
                                               <a href="{{route('account.profile')}}" class="btn btn-secondary btn-sm">set to public</a>.</p>
                                           <footer class="blockquote-footer text-danger">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                                       </blockquote>
                                   </div>
                               </div>
                           </div>
                       </div>
                    @endif
                </div>
            </div>


             <!-- end of stat -->
            <div class="row">
                <div class="col-md-8">
                    <div id="productions" class="card">
                        <div class="card-header">
                            <h5 class="h3 mb-0">Quick Links</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush list">
                                <li class="list-group-item">
                                    <a href="{{route('casting.projects.index')}}">Manage Projects</a>
                                    <p class="menu-help-text">Manage the projects you have posted.</p>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{route('account.applications')}}">My Applications</a>
                                    <p class="menu-help-text">Have you applied for any role? Check the status here.</p>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{route('creator.company.profile-update')}}">Company Profile</a>
                                    <p class="menu-help-text">Are you a creator? Manage your company information here.</p>
                                </li>

                                <li class="list-group-item">
                                    <a href="{{route('account.profile.settings.subscriptions')}}">Billing & Subscription</a>
                                    <p class="menu-help-text">Manage your financial details like cards, subscription and more.</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    @include('account.latest-notes')

                    @include('account.latest-projects')

                </div>
            </div>
        </div>
    </div>
@endsection
