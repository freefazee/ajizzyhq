<div class="widget widget--no-padding widget-similar-links">
    <div class="list-group">
        <a href="{{route('account.profile.settings')}}" class="list-group-item list-group-item-action">Account settings</a>
        <a href="{{route('account.profile.settings.subscriptions')}}" class="list-group-item list-group-item-action">
            Subscription info
            <span class="badge badge-{{auth()->user()->profile->subscriptionActive() ? 'success' : 'danger'}}">{{auth()->user()->profile->subscriptionActive() ? 'Active' : 'Expired' }}</span>
        </a>

    </div>
</div>
<div class="widget widget-quick-help">
    <h3 class="section-title">Still need help?</h3>
    <p>
        You can send an email to <a href="mailto:talk@ajizzy.com">talk@ajizzy.com</a>
    </p>
</div>
