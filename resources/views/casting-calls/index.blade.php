@extends('layouts.post')

@section('title')Open Casting Calls & Auditions Near You | Ajizzy @endsection
@section('keywords')Acting Jobs, Modeling Jobs, Entertainment jobs, latest modeling jobs, latest casting calls, @parent @endsection

@section('content')
    <div class="mid-section" id="calls">

        <casting-calls-wrapper :categories="{{$categories}}" :ethnicities="{{$ethnicities}}">
            <div class="container">loading..<i class="fas fa-spinner fa-spin"></i></div>
        </casting-calls-wrapper>
    </div>

@endsection

@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/project/casting-calls.js')}}"></script>
@endsection
