@extends('layouts.post')

@section('title'){{$project->title}} - Ajizzy @endsection

@section('description'){{Str::limit($project->description)}}@endsection

@section('content')
    <div class="mid-section" role="body" id="single">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="section-header">
                        <h1 class="section-header--title">{{$project->title}}</h1>
                    </div>
                    <div class="prod-listing__info">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="prod-listing__info__section">
                                    <span class="category-label">CATEGORY:</span>
                                    <a href="/casting/?pt=56&amp;pt=107" class="tag category">{{$project->category->parent->name}}</a>
                                    <a href="/casting?pt=56&amp;pt=76" class="tag sub-category">{{$project->category->name}}</a>
                                </div>
                                <div class="prod-listing__info__section">
                                    <span class="tag-label">TAGS:</span>
                                    <a class="tag paid" href="/casting-calls/?compensation=true">Paid</a>
                                </div>
                                <div class="prod-listing__info__section">
                                    <span class="expires-text">
                                        <span class="expires-text--label">EXPIRES:</span>
                                        <span class="expires-text--date">{{$project->formatted_expiry_date}}</span>
                                    </span>
                                </div>
                                <div class="prod-listing__info__section share">
                                    <span class="share-label">SHARE:</span>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{route('single.casting-call', $project->id)}}" target="_blank" class="share-icon facebook"></a>
                                    <a
                                        href="https://twitter.com/intent/tweet?text=Open Casting Call: {{$project->title}}&url={{route('single.casting-call', $project->id)}}&hashtags=ajizzy,african talents,creators,actors,models" target="_blank" class="share-icon twitter"></a>
                                    <a href="mailto:?subject=Open Casting Call: {{$project->title}}&body=A new role has just been posted on ajizzy.com. Hurry now and reserve your space at {{route('single.casting-call', $project->id)}}" target="_blank" class="share-icon email"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-body">
                        <div class="casting--primary-information">
                            <div class="cast-author-category">Company / Agency</div>
                            <div class="cast-author-name">{{$project->company}}</div>
                            <div class="cast-crew">
                                {{str_limit($project->about_company,1000)}}
                            </div>
                            <div class="cast-description">
                                <h5>Production description</h5>
                                <p>{{$project->description}}</p>

                            </div>
                        </div>
                        <div class="casting--secondary-information">
                            <div class="casting--roles">
                                <h3>
                                    Roles
                                    <span class="expand-roles float-right">
                                        <a href="#">Expand all roles</a>
                                    </span>
                                </h3>

                                <div class="sub--section-content">
                                    <div class="accordion" id="available-roles">
                                        <div class="card pt-2 mb-0">
                                            @foreach($project->roles as $role)
                                            <div class="card-header" id="role{{$role->id}}">
                                                <h4 class="mb-0 card-header-title">
                                                    <a class="" data-toggle="collapse" href="#role{{$role->id}}Details" role="button" aria-expanded="false" aria-controls="roleOneDetails">{{$role->name}} ({{$role->roleType->name}}): <span class="role-meta">{{$role->gender}}, {{$role->min_age}}-{{$role->max_age}}</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="role{{$role->id}}Details" class="collapse" aria-labelledby="role{{$role->id}}" data-parent="#available-roles">
                                                <div class="card-body">
                                                    @if($role->description)
                                                        <p>{{$role->description}}</p>
                                                    @else
                                                        <p>No specific description for this role. Just hit apply.</p>
                                                    @endif

                                                        @if($role->requires_nudity) <p>
                                                            <span class="badge badge-danger">This role requires nudity</span>
                                                        </p> @endif


                                                    @if(!in_array($role->id, $rolesApplied->toArray()))
                                                    <apply-for-role
                                                        :role="{{$role}}"
                                                        input-class="btn btn-sm btn-black btn__custom mt-3"
                                                        btn-text="Apply for this role"
                                                        key="{{$role->id}}"
                                                        :roles-applied="{{$rolesApplied}}"
                                                        :auth-check="{{auth()->user() ? 0 : 1}}">Loading..</apply-for-role>
                                                   @else
                                                       <p><button class="btn btn-success">Applied <i class="fas fa-check"></i></button></p>
                                                    @endif
                                                </div>
                                            </div>
                                           @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="casting--dates">
                                <h3>Rehearsal / Production Dates &amp; Locations</h3>
                                <div class="sub--section-content">
                                    <p>{{$project->loc}}</p>
                                </div>
                            </div>
                            <div class="casting--contracts">
                                <h3>Compensation & Union Contract Details</h3>
                                <div class="sub--section-content">
                                    <p>{{$project->compensation_detail}}</p>
                                </div>
                            </div>
                            <div class="casting--key-details">
                                <h3>Key Details</h3>
                                <div class="sub--section-content">
                                    <p>
                                        <span class="font-weight-bold text-capitalize">Seeking talent:</span>
                                        <br />
                                        @if($project->location == 'worldwide')
                                            Seeking talents worldwide.
                                        @else
                                        {{ucfirst($project->location)}}
                                            ({{$project->location === 'local' ? $project->country->name : 'Multiple locations'}})
                                            @endif
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 aside">
                    <div class="widget widget--apply widget--bg-ajizzy">
                        <h3>Ready to apply?</h3>
                        <ready-to-apply-box :roles="{{$project->roles}}"></ready-to-apply-box>
                    </div>

                     @include('casting-calls._similar')


                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/project/casting-call-single.js')}}"></script>

@endsection
