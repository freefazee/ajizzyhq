<div class="widget widget--similar-calls widget-transparent border-0 p-0 py-2">
    <h3 class="section-title--bordered">
        Similar calls
    </h3>
    <ul class="list-unstyled widget-list-items">

        @foreach($similar as $proj)
        <li>
            <h6 class="category-title">{{$proj->category->name}}</h6>
            <h4><a href="{{route('single.casting-call', $proj->id)}}">{{$proj->title}}</a></h4>
        </li>
        @endforeach

    </ul>
</div>
