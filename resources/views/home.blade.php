@extends('layouts.main')

@section('title')Welcome to Ajizzy  - @parent @endsection

@section('content')
    <div class="home-hero" id="top-page">
        <div class="ajizzy--caption d-flex justify-content-center">
            <div class="text-center">
                <h1 class="title">
                    Get Discovered Today!
                </h1>
                <h4 class="desc">
                    We help Africa’s most creative talent find work and enable industry professionals to move their projects forward.
                </h4>

                @if(auth()->user())
                <a href="{{route('creator.projects.new')}}" class="btn btn-orange btn__custom mr-sm-3 mb-3">POST A JOB</a>
                <a href="{{route('talents')}}" class="btn btn-ajizzy-outline  btn__custom mb-3">FIND TALENTS</a>
                @else
                    <a href="{{route('talent.join-show')}}" class="btn btn-orange btn__custom mr-sm-3 mb-3">Sign up now</a>
                    <a href="{{route('login')}}" class="btn btn__custom btn-ajizzy-outline mb-3">Login</a>

                    <span><small class="cta-desc scroll-to-join">By signing up to Ajizzy.com, you confirm that you are above {{config('ajizzy.min_age')}} years old.</small></span>
                @endif


            </div>
        </div>
    </div>
    <!-- END #home-hero -->



    <section id="castings">
        <div class="container">
            <div class="row featured-calls">
                <div class="col-md-12">
                    <div class="section--header">
                        <h2 class="section--title">Latest Casting Calls & Jobs</h2>
                    </div>
                    <div class="row section--body">
                        <div class="col-md-12">
                            @include('featured-casting')


                            <div class="browse-cast-category pb-0">
                                <h4 class="text-orange mb-3">Browse by Category</h4>
                                <div class="row row-eq-heights calls-categories">
                                    @foreach($categories as $category)
                                    <div class="col-md-4 category-listings">
                                        <div class="column-wrapper">
                                            <h4 class="border-bottom">{{$category->name}}</h4>
                                            <ul class="list-unstyled">
                                                @foreach($category->children as $child)
                                                    <li>
                                                        <h6 class="h6--small-header category-title">
                                                            <a href="#">{{$child->name}}</a>
                                                        </h6>
                                                        <h3 class="h3--smaller-header">
                                                            <a href="{{$child->project->getUrl()}}">{{$child->project->title}}</a>
                                                        </h3>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="creators-job-posting">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-wrap d-md-flex align-items-center">
                        <div class="caption flex-grow-1">
                            <h2 class="section--title-smallest">
                                Need top talent for your next project?
                            </h2>
                            <p>Ajizzy will help you find the best talent in Africa with our easy-to-use tools.</p>
                        </div>
                        <a href="{{route('creator.projects.new')}}" class=" ml-auto btn__custom btn btn-ajizzy-outline-black">Post a Job</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="opportunities" class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-5 mb-5 mb-md-0">
                    <div class="section--header">
                        <h5 class="section--title-category text-left">Opportunities</h5>
                        <h2 class="section--title text-capitalize">
                            Join Today, Land Your Next Role
                        </h2>
                        <p class="section--title-description">
                            We are Africa’s #1 resource for performers to find high-quality roles to match their interests and career goals.
                        </p>
                        <a href="{{route('talent.join-show')}}" class="btn btn-orange btn__custom">Sign Up</a>
                    </div>
                </div>
                <div class="col-md-6 ml-auto">
                    <div class="video-player-wrapper">
                        <img src="{{asset('assets/images/dark-skinned-lady-wearing-eyeglasses.png')}}" alt="Video thumbnails" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="row section-body">
                <div class="col-md-4">
                    <div class="text-wrapper mb-4 mb-md-0">
                        <h4>Create Your Profile</h4>
                        <p>
                            Add all of your content, including headshots, reels, résumés and credits, to showcase your talent to casting directors.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-wrapper mb-4 mb-md-0">
                        <h4>Apply For Jobs</h4>
                        <p>
                            Explore casting notices daily and find the perfect roles to push your career forward.

                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-wrapper">
                        <h4>Land New Auditions</h4>
                        <p>
                            Apply, audition, land the role, and create your mark!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <section id="clients" class="border-top border-bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/tiffany-amber.png')}}" alt="" class="img-fluid" />
                        </li>
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/souncity.png')}}" alt="" class="img-fluid" />
                        </li>
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/chanel.png')}}" alt="" class="img-fluid" />
                        </li>
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/armani-exchange.png')}}" alt="" class="img-fluid" />
                        </li>
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/dapmod.png')}}" alt="" class="img-fluid" />
                        </li>
                        <li class="list-inline-item clientele-brands">
                            <img src="{{asset('assets/images/brands/asos.png')}}" alt="" class="img-fluid" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->

    <section id="resources" class="section">
        <div class="container">
            <div class="section--header">
                <div class="row justify-content-center">
                    <div class="col-md-10 text-md-center">
                        <h2 class="section--title">The #1 Trusted Platform for African Performers</h2>
                        <p class="section--title-description">
                            Our members represent a powerful network of professionals who make the most out of our unlimited job submissions, media uploads, and access to industry professionals.
                        </p>
                    </div>
                </div>
            </div>
            <div class="section--body">
                <div class="row no-gutters row-eq-heights">
                    <div class="col-md-4">
                        <div class="resources-feature-wrap pl-md-0">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/unlimited-applicants.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Unlimited applications</h4>
                                <p>
                                    You can apply to as many jobs as you want on Ajizzy. There’s something for everyone.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-feature-wrap">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/talent-profile.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Talent profile</h4>
                                <p>
                                    Add all of your content, including headshots, reels, résumés and credits, to showcase your talent to casting directors.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-feature-wrap pr-md-0">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/unlimited-media.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Unlimited media</h4>
                                <p>
                                    Upload as much content as you need so you always have the most up-to-date profile for castings.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-feature-wrap pl-md-0">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/instant-job-alerts.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Instant job alerts</h4>
                                <p>
                                    Stay up-to-date on your casting with instant notification alerts.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="resources-feature-wrap">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/safety-security.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Safety + security</h4>
                                <p>
                                    Every job is reviewed for legitimacy and safety. Ajizzy is a platform you can trust.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="resources-feature-wrap">
                            <span class="icons float-left"><img src="{{asset('assets/images/icns/safety-security.png')}}" alt="" class="img-fluid" /></span>
                            <div class="text-wrapper">
                                <h4>Relevant Jobs</h4>
                                <p>
                                    We’ve curated the best jobs available in Africa. Use Ajizzy to help you shape the career you want.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>




@endsection
