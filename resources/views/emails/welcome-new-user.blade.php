<h2>Hey {{$user->first_name}},</h2>

<p>We all from Ajizzy welcome you to the largest and most trusted talent platform in the world. You are about to land your next role.
</p>

<p>Get to know us in our <a href="">(Ajizzy for talents)</a> video. You’ll be guided through a few tips on how to navigate our platform and also help you make the most out of service as soon as possible.</p>

<p>Before then, kindly <a href="{{route('verify.activate')}}?hash={{$user->verification->hash}}">click on this link</a> to activate your email address and let’s get the ball rolling.
    <br>
   <code> {{route('verify.activate')}}?hash={{$user->verification->hash}}</code>

</p>

<h2>How to be successful on Ajizzy</h2>

<p><strong>A good profile is everything.</strong>  Your profile is your CV that speak to creators in your absence, hence the need to build your profile we all sense of professionalism. Ensure you follow the profile builder till the end. When uploading your photos, put very attractive photos first and crop them to make sure the show your most attractive side. Showcase your headshots, resume, reel, other photos, social media profiles and more, all in one profile. Make it lovely.</p>


<p><strong>Check your email regularly:</strong> You will be getting notifications from us on a regular basis.Kindly ensure you check your email. If for some reason our email entered your spam/junk folder, just mark it as not-spam or move it to inbox to ensure subsequent delivery to your inbox.</p>

<p><strong>Talk about us.</strong> Record a short video of yourself talking about Ajizzy and post on your social media. Tag us on <strong>#ajizzyme</strong>. The larger the community the better for all of us. We love to have your friends as well.</p>

<p>Give us some feedback if you have any issue or notice something you are not comfortable with. We are always an email away or a tweet.
</p>
