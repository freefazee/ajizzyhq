<p>Hey {{$invite->invitee->first_name}}</p>

<p>Your opportunity to land a role just arrived.   <strong>{{$invite->project->owner->profile->getCompanyDisplayName()}}</strong> just invited you to apply for a role on their project.</p>

<p>When you apply for a role, ensure you constantly check your emails for updates and audition dates. </p>

<p>As part of our transparency policy, applying for a role means you are willing to share your contact details with the creator. Contact details include phone and email that you have provided in your profile builder.</p>


@if($invite->message)
    <h2>A message from them</h2>
<p>

    {{$invite->message}}
</p>
@endif

<h3><a href="{{route('single.casting-call', $invite->project->id)}}">Click here</a> to view the project</h3>

<p>You can as well copy and paste this link to your browser. {{route('single.casting-call', $invite->project->id)}}</p>
