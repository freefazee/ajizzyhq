@extends('layouts.main')

@section('title'){{$talent->stage_name}} - {{$talent->title}}: {{$talent->gender}} | Ajizzy @endsection

@section('keywords'){{$talent->stage_name}},{{$talent->title}}, @parent @endsection


@section('content')
    <div class="mid-section" role="body">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-lg-10 col-xl-10">
                    <div class="profile-top">
                        <div class="profile-image">
                            <img src="{{$talent->medias[0]->url}}"
                                 alt="">
                        </div>

                        <div class="profile-user-settings">
                            <h1 class="profile-user-name">{{$talent->stage_name}}</h1>

                            @if(auth()->user())
                            <a class="btn profile-settings-btn" href="{{route('account.profile')}}" aria-label="profile settings">
                                <i class="fas fa-cog"  aria-hidden="true"></i>
                            </a>
                            @endif
                        </div>

                        <div class="profile-bio">
                            <p>
                                <span class="profile-real-name">{{$talent->title}}</span> @if($talent->about_company) <br /> {{$talent->about_company}} @endif</p>

                        </div>

                    </div>
                    <!-- End of profile section -->

                    <div class="nav-wrapper profile-pane">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Media Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Profile</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card shadow" id="application">
                        <div class="no-body-padding card-body">
                            <div class="tab-content" id="myTabContent">

                                <!-- home detail-->
                                <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                    <media-grid endpoint="{{$talent->url}}/media">
                                        <p>Loading.. <i class="fas fa-spinner fa-spin"></i></p>
                                    </media-grid>

                                </div>


                                <div class="tab-pane profile--main fade profile-details" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <div class="profile-section">
                                                <div class="profile-section__header">
                                                    <h4>Appearance</h4>
                                                </div>
                                                <div class="profile-section__content appearance" id="profileAppearance">
                                                    <ul>
                                                        <li>
                                                            <span class="attribute-label">Gender:</span>
                                                            <span class="attribute-value">{{ucfirst($talent->gender)}}</span>
                                                        </li>
                                                        <li>
                                                            <span class="attribute-label">Age Range:</span>
                                                            <span class="attribute-value">{{$talent->age_range}}</span>
                                                        </li>

                                                        @if($talent->appearance && $talent->appearance->height)
                                                        <li>
                                                            <span class="attribute-label">Height:</span>
                                                            <span class="attribute-value">{{$talent->appearance->height}}@if($talent->appearance->height_unit_us) Centimeters @else Feet @endif</span>
                                                        </li>
                                                        @endif

                                                        @if($talent->appearance && $talent->appearance->weight)
                                                            <li>
                                                                <span class="attribute-label">Weight:</span>
                                                                <span class="attribute-value">{{$talent->appearance->weight}}@if($talent->appearance->weight_unit_us) Pounds @else Kilograms @endif</span>
                                                            </li>
                                                        @endif



                                                        @if($talent->appearance && $talent->appearance->bodySize)
                                                            <li>
                                                                <span class="attribute-label">Body Type:</span>
                                                                <span class="attribute-value">{{$talent->appearance->bodySize->name}}</span>
                                                            </li>
                                                        @endif

                                                        @if($talent->appearance && $talent->appearance->eye)
                                                            <li>
                                                                <span class="attribute-label">Eye color:</span>
                                                                <span class="attribute-value">{{$talent->appearance->eye->name}}</span>
                                                            </li>
                                                        @endif


                                                        @if($talent->appearance && $talent->appearance->hair)
                                                            <li>
                                                                <span class="attribute-label">Hair color:</span>
                                                                <span class="attribute-value">{{$talent->appearance->hair->name}}</span>
                                                            </li>
                                                        @endif

                                                        @if($talent->user->ethnicities)
                                                           <li>
                                                               <span class="attribute-label">Ethnicities:</span>
                                                               <strong class="attribute-value">
                                                               @foreach($talent->user->ethnicities as $eth)
                                                                   {{$eth->name}} <br />
                                                               @endforeach
                                                               </strong>
                                                           </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>

                                            @if($talent->tags->count())
                                                <div class="profile-section">
                                                    <div class="profile-section__header">
                                                        <h4>Skills</h4>
                                                    </div>
                                                    <div class="profile-section__content skills" id="profileskills">
                                                        <ul>
                                                            @foreach($talent->tags as $tag)
                                                                <li class="profile-skill">
                                                                    <a href=""><span class="skill-name">{{$tag->name}}</span></a>
                                                                </li>
                                                            @endforeach

                                                        </ul>
                                                    </div>
                                                </div>

                                            @endif
                                            <div class="profile--main__education">
                                                <div class="profile-section__header">
                                                    <h4>Education &amp; Training</h4>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table align-items-center">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th scope="col">School</th>
                                                            <th scope="col">Degree/Course</th>
                                                            <th scope="col">Instructor, Location, Year Complete</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="list">
                                                        @foreach($talent->user->educations as $edu)
                                                        <tr>
                                                            <th scope="row">{{$edu->school}}</th>
                                                            <td class="role">{{$edu->degree}}</td>
                                                            <td>{{$edu->location}}, {{$edu->year_completed}}</td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End of gallery -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/talents/public.js')}}"></script>
@endsection

