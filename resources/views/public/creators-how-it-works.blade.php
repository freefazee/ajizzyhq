@extends('layouts.main')

@section('title')How Ajizzy Works For Creative Professionals @endsection

@section('content')
    <section  class="section section-mini bg--grey why-join-how">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section--header text-center mb-0">
                        <h5 class="d-none d-md-block">For Creative Professionals</h5>
                        <h2 class="section--title">
                            How Ajizzy works for Creators
                        </h2>
                        <p class="section--title-description mb-5">
                            Ajizzy is your one-stop platform to make your project come to life. We have a community of professionals including actors, film, TV, voice-over artists, extras, dancer, singers, musicians and more.
                        </p>
                        <a href="{{route('talents')}}" class="btn btn__custom btn-orange">TALENT DATABASE</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tools" class="section">
        <div class="container">
            <div class="section--body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card shadow-none">
                            <div class="card-body text-center">
                                <p class="card-text mb-4">
                                    <img src="{{asset('images/post-job-icon.jpg')}}" class="rounded-circle img-center img-fluid shadow-lg--hover" style="width: 140px;">
                                <h2>Post A Job</h2>
                                    Post a job on Ajizzy and begin to receive submissions from qualified candidates straight away.</p>

                                <p> <a href="{{route('creator.projects.new')}}" class="btn btn-outline-yellow btn-sm">TALENT DATABASE</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card shadow-none">
                            <div class="card-body text-center">
                                <p class="card-text mb-4">
                                    <img src="{{asset('images/find-talent-icon.jpg')}}" class="rounded-circle img-center img-fluid  shadow-lg--hover" style="width: 140px;">
                                    <h2>Find Talent</h2>
                                    Browse submissions from agents and talent, or search our database of talent to find the perfect fit for your role.
                                </p>
                               <p> <a href="{{route('talents')}}" class="btn btn-outline-yellow btn-sm">TALENT DATABASE</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card shadow-none">
                            <div class="card-body text-center">
                                <p class="card-text mb-4">
                                    <img src="{{asset('images/cast-project-icon.jpg')}}" class="rounded-circle img-center img-fluid shadow-lg--hover" style="width: 140px;">
                                    <h2>Cast Your Project</h2>
                                    Use our application management tool to organize applications, plan auditions, and hire the best talent for your project.
                                </p>
                                <p> <a href="{{route('casting.projects.index')}}" class="btn btn-outline-yellow btn-sm">MANAGA APPLICATIONS</a></p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="container-fluid" style="margin-top: 30px;">
            <div class="col-12">
                <div class="row align-items-center">
                    <div class="col-lg-7 col-md-6 col-xs-12 audience">
                        <div class="grey-back">
                            <div    style="padding: 70px; align-self: center;" class="bg--grey">
                                <h3>Why Us?</h3>
                                <p class="body-text-lg">
                                <h4 class="font-weight-700">Talent Database</h4>
                                Ajizzy prides itself as a database with a large pool of talented professionals. With our smooth user experience and advanced drill down filters, you can easily search, sort, and notify talents that match your casting roles quickly.

                                </p>

                                <p>
                                    Thousands of professionals use Ajizzy to facilitate their auditions yearly. You have a wide range of possible selections across the continent.
                                </p>

                                <h4 class="font-weight-700">Messaging</h4>
                                <p>Ajizzy has largely simplified the process of getting across to successful applicants. It allows you to network with applicants & schedule interviews and auditions quicker. You can communicate directly with successful applicants via email or whatsapp on the fly.
                                </p>

                                <h4 class="font-weight-700">Application Manager</h4>
                               <p> Sieve through the noise with our cutting edge platform that allows you to filter submissions that meet your specific project needs. You can send notifications to an applicant once an application is reviewed while the talent gets notified immediately. All these will happen at the comfort of your home or office.         </p>

                                <h4 class="font-weight-700">Casting Collaborators</h4>
                                <p>Ajizzy enables your team to log in and work from anywhere. You can share access to specific casting calls and projects and you can enable your team and stakeholders to collaborate in a unified and secure environment. This helps you make a unified and collaborative decision.</p>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-xs-12 audience">
                        <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div>
                                        <img src="{{asset('images/filming1.jpg')}}" alt="Filmmaker photo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="{{route('partnership')}}" class="btn btn__custom btn-orange">BECOME A PARTNER</a>

                </div>
            </div>
        </div>
    </section>
@endsection
