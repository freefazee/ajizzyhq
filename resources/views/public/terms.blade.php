@extends('layouts.main')


@section('content')
    <section  class="section section-mini">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section--header text-center mb-0">
                        <h2 class="section--title">
                            Ajizzy Terms of Service
                        </h2>

                    </div>

                    <div class="WordSection1">

                        <p class="MsoNormal" style="margin-top:10.0pt;text-align:justify;background:white"><span lang="EN-GB" style="color:#333333">These terms and conditions (“Terms of Use”),
apply to the entire contents of this Website and any affiliated website
hereunder (Websites), including any functionality and services offered on or
through each of the Websites, to any products, software, or mobile applications
provided by Ajizzy or the Websites, and to any correspondence by Ajizzy. Please
read these terms and conditions carefully before using this Website. </span></p>

                        <ol style="margin-top:0cm" start="1" type="1">
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Introduction
     and Acceptance of the Terms of Service</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">By visiting
      or using any of the Websites, including access or use through a computer,
      mobile device, mobile application or by any other means, you accept and
      agree to be bound by these Terms of Use regardless of whether or not you
      choose to register with us. If you do not accept these terms and
      conditions, do not use the Websites.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You affirm
      that you are either more than eighteen (18) years of age, or an
      emancipated minor, or possess legal parental or guardian consent, and are
      fully able and competent to enter into the terms, conditions, obligations,
      affirmations, representations, and warranties set forth in these Terms of
      Use, and to abide by and comply with these Terms of Use, and are not a
      person barred from using the Websites under the laws of the Federal
      Republic of Nigeria or other applicable jurisdiction.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><a></a><a></a><a><span lang="EN-GB" style="background:
      white">Users under the age of eighteen (18) (a "Young User")
      are expressly prohibited from using the Ajizzy Website Services, as
      defined in Section 2 below. Young Users who would like to participate in
      any of the Websites must have their parent or legal guardian (a “Parent
      Registered User”) register to use the Website, using the Parent
      Registered User’s name, email, and credit card information, and consent
      to and upload the Young User’s information to the Websites. Parent
      Registered Users are responsible for directly engaging with all Services
      on the Websites, including but not limited to submitting applications and
      messaging other users, updating the settings of and terminating their
      account and thereby use of the Websites on behalf of the applicable Young
      User.</span></a><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_1" onmouseover="msoCommentShow('_anchor_1','_com_1')" onmouseout="msoCommentHide('_com_1')" href="#_msocom_1" language="JavaScript" name="_msoanchor_1">[1]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_2" onmouseover="msoCommentShow('_anchor_2','_com_2')" onmouseout="msoCommentHide('_com_2')" href="#_msocom_2" language="JavaScript" name="_msoanchor_2">[2]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_3" onmouseover="msoCommentShow('_anchor_3','_com_3')" onmouseout="msoCommentHide('_com_3')" href="#_msocom_3" language="JavaScript" name="_msoanchor_3">[3]</a>&nbsp;</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">For purposes
      of these Terms of Use, Parent Registered Users and their Young Users are
      deemed to be Users as used in these Terms of Use. By registering as a
      Parent Registered User, you hereby represent, warrant, understand, agree
      to and accept these Terms of Use and any applicable additional terms in
      their entirety on behalf of yourself and your Young User whether or not
      you use any of the Websites. You further understand and agree that you
      will ensure your Young User’s compliance with these Terms of Use and that
      you are responsible for any noncompliance by your Young User.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">If you are a
      Parent Registered User, you also agree that you are responsible for using
      and monitoring the account on behalf of your Young User.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><a><span lang="EN-GB" style="background:white">If a
      parent or guardian believes that any Website has collected information of
      a child under the age of thirteen (13), or under the age of eighteen (18)
      please contact us at Ajizzy.com.<br>
      </span></a><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_4" onmouseover="msoCommentShow('_anchor_4','_com_4')" onmouseout="msoCommentHide('_com_4')" href="#_msocom_4" language="JavaScript" name="_msoanchor_4">[4]</a>&nbsp;</span><span lang="EN-GB" style="background:white">Ajizzy may revise and update these
      Terms of Use from time to time at our sole discretion. All changes are
      effective immediately when we post them, and apply to all access to and
      use of the Websites thereafter. However, any changes to the dispute
      resolution provisions set forth in Governing Law and Jurisdiction section
      will not apply to any disputes for which the parties have actual notice
      on or prior to the date the change is posted on the Websites. Your
      continued use of the Websites following the posting of revised Terms of
      Use means that you accept and agree to the changes. You are expected to
      check this page so you are aware of any changes, as they are binding on
      you.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Website
     Content </span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The Websites
      contain content including but not limited to text, graphics, photographs,
      images, news reports, articles, editorial and other writings, audio and
      video recordings, data, listings, and directory information
      (collectively, “Content”) that is accessible by Users. The Websites may
      also offer breakdowns, casting notices, forums, bulletin boards, wikis,
      chat rooms, blogs or other interactive areas for Users to communicate
      with each other, and the information therein is also considered Content.
      The creation of a user account may be required for the use of certain
      portions of the Websites. </span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The Websites
      provide an online venue for Users to interact and communicate with
      individuals posting casting notices. Any agreement or transaction that
      arises from these interactions or communications does not involve Ajizzy,
      and Ajizzy is not a party to any agreement between Users. Ajizzy is not
      an employer with respect to any opportunity advertised on the Websites,
      and Ajizzy is not responsible for any employment, hiring, or salary
      decisions made by you or anyone else using the Websites.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">In addition,
      certain contents and services of the Websites may only be accessed through
      purchase or paid subscription.&nbsp; We refer to the paid services available
      on or through the Websites, including the digital and print publication
      subscription services, collectively as the “<b>Paid Services</b>“.&nbsp;
      Content that is available to Users only through purchase or paid
      subscription is referred to collectively as the “<b>Paid Services Content</b>“.
      Content may include facts, views, opinions and recommendations of
      individuals and organizations not affiliated with Ajizzy.com.&nbsp; Ajizzy.com
      does not guarantee the accuracy, completeness or timeliness of, or
      otherwise endorse, these facts, views, opinions or recommendations.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Proprietary
     Rights</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><a></a><a></a><a><span lang="EN-GB" style="background:
      white">Ajizzy owns, operates, licenses, controls, and provides access to
      the Websites. You acknowledge and agree that Ajizzy, and its licensors,
      retain all right, title and interest in and to all past, present and
      future Content, excluding User Provided Content (as defined below).</span></a><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_5" onmouseover="msoCommentShow('_anchor_5','_com_5')" onmouseout="msoCommentHide('_com_5')" href="#_msocom_5" language="JavaScript" name="_msoanchor_5">[5]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_6" onmouseover="msoCommentShow('_anchor_6','_com_6')" onmouseout="msoCommentHide('_com_6')" href="#_msocom_6" language="JavaScript" name="_msoanchor_6">[6]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_7" onmouseover="msoCommentShow('_anchor_7','_com_7')" onmouseout="msoCommentHide('_com_7')" href="#_msocom_7" language="JavaScript" name="_msoanchor_7">[7]</a>&nbsp;</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Ajizzy, the
      Ajizzy logo and all other related unique identifiers of Ajizzy brand and
      its affiliates are the registered or common law or trademarks of Ajizzy,
      and all other proprietary trademarks, service marks, trade names, trade
      dress, slogans, logos, and other indicia of origin that appear on or in
      connection with the Websites are the property of Ajizzy.com unless
      otherwise noted and are protected by applicable copyright, trademark,
      intellectual property and other laws. Ajizzy's intellectual property,
      including, without limitation, its trademarks and trade dress may not be
      used in any manner that is likely to cause confusion among users, or in
      any manner that disparages Ajizzy. All rights are expressly reserved.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">All third
      party trademarks, logos, photographs, images, audio and audio-visual
      content, programming, and other intellectual property contained on or
      within the Websites are the property of the respective third parties,
      including the respective Content owners, and may be protected by
      applicable copyright, trademark or other intellectual property laws and
      treaties. Each such third party expressly reserves all rights to such
      intellectual property. Use of third party software or services is subject
      to the terms and conditions of the applicable third party license
      agreements, and you agree to look solely to the applicable third party
      and not to Ajizzy to enforce any of your rights in relation thereto.
      Except as expressly set forth in these Terms of Use or otherwise
      expressly granted to you in writing by Ajizzy, no rights (either by
      implication, estoppel or otherwise) in or to the Websites or their contents
      are granted to you.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">User
     Provided Content</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Portions of
      the Websites may allow you and other Users to upload or transmit data,
      information, text, images, software, audio, photographs, graphics, video,
      messages, tags, or other materials to or through the Websites ("User
      Provided Content"). For example, the Websites may offer forums,
      bulletin boards, wikis, chat rooms, blogs or other interactive areas. For
      User Provided Content, Ajizzy is merely hosting and providing access.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The decision
      to submit User Provided Content to the Websites is your responsibility
      and you should only submit content that belongs to you or that will not
      violate the rights of others. By submitting User Provided Content, you
      represent, acknowledge, and warrant (a) that you have the right to do so
      or that you have obtained any necessary third party consents (e.g., under
      privacy or intellectual property laws), (b) that the User Provided
      Content does not infringe on the copyrights, trademarks, moral rights,
      rights of privacy or publicity, or the intellectual property rights of
      any person or entity, and (c) that no other party has any right, title,
      claim, or interest in the User Provided Content that would be infringed
      upon as a result of uploading the User Provided Content. If you submit
      User Provided Content to the Websites on behalf of a group, organization
      or business entity, you represent that you have the right to do so and
      that you have obtained any consents from the group, organization or
      business. Upon the request of Ajizzy, you agree to furnish Ajizzy with
      any documentation, substantiation and releases necessary and reasonably
      required to verify and substantiate your compliance with this provision.
      You agree not to accept payment for User Provided Content from any third
      party, including, without limitation, accepting payment for the inclusion
      of a logo, brand advertising or other commercial content, in User
      Provided Content.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Ajizzy cannot
      vouch for the validity, accuracy or credibility of any User Provided
      Content, and does not take any responsibility or assume any liability for
      any actions Users may take as a result of viewing, reading or listening
      to User Provided Content on the Websites. Through your use of the
      Websites you may be exposed to Content that you may find offensive,
      objectionable, harmful, inaccurate or deceptive. There may also be risks
      of dealing with underage persons, people acting under false pretenses,
      international trade issues and foreign nationals. Ajizzy does not endorse
      any User Provided Content or any opinion, recommendation, or advice
      expressed therein. By using the Websites, you assume all associated
      risks, and Ajizzy expressly disclaims any and all liability in connection
      with User Provided Content.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">In addition,
      you grant to Ajizzy, without any credit or compensation to you, a
      royalty-free, non-exclusive, worldwide, perpetual, unrestricted,
      irrevocable, and fully transferable, assignable and sub-licensable
      license to host, use, modify, display, copy, reproduce, disclose, sell,
      translate, create derivative works of, distribute, and export any User
      Provided Content, in whole or in part, or to incorporate it in other
      works in any form, media, software or technology of any kind now known or
      hereafter developed or discovered for any purposes whatsoever. You agree
      that Ajizzy may publish or otherwise disclose your name in connection
      with your User Provided Content.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You
      acknowledge, consent and agree that Ajizzy may access, preserve and
      disclose account information and User Provided Content that you provide
      if Ajizzy is required to do so by law or if it believes in good faith
      that such access, preservation or disclosure is reasonably necessary to
      (i) comply with legal process, (ii) enforce these Terms of Use, (iii)
      respond to claims that any User Provided Content violates the rights of
      third parties, (iv) respond to your requests for customer service, or (v)
      protect the rights, property or personal safety of Ajizzy, its employees,
      partners and agents or members of the public.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Ajizzy
      undertakes no obligation to pre-screen User Provided Content, but
      reserves the right to, and may from time to time, monitor any and all
      information transmitted or received through the Websites. You
      acknowledge, consent and agree that Ajizzy, at its sole discretion and
      without notice to you, may review, censor, delete, move, edit, block access
      to or prohibit the transmission or receipt of any User Provided Content
      or other information, in whole or in part, for any reason whatsoever,
      including but not limited to User Provided Content that Ajizzy deems
      obscene, defamatory or libelous in nature, that invades the right of
      privacy or infringes any right of any person or entity, is unlawful, is
      offensive or otherwise inappropriate, or that Ajizzy believes to be in
      violation of these Terms of Use.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You are
      solely responsible for all User Provided Content that you make available
      via the Websites. Under no circumstances will Ajizzy be liable to you in
      any way for any User Provided Content that you upload, post, or otherwise
      make available via the Websites including, but not limited to, any errors
      or omissions in User Provided Content, or for any loss or damage of any
      kind incurred as a result of User Provided Content. In addition, you
      hereby release Ajizzy from any and all claims, liens, demands, actions or
      suits in connection with the User Provided Content, including, without
      limitation, any and all liability for any use or nonuse of your User
      Provided Content, claims for defamation, invasion of privacy, right of
      publicity, emotional distress or economic loss. Except for the rights
      granted in these Terms of Use, Ajizzy acquires no title or ownership
      rights in or to any User Provided Content you submit and nothing in these
      Terms of Use conveys any ownership rights in the User Provided Content
      you submit to Ajizzy.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Third Party
     Content and Linked Sites.</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Third
      parties, including Users, provide some of the Content of the Websites.
      Ajizzy makes no representations or warranties as to the completeness,
      accuracy, adequacy, currency or reliability of any content supplied by
      third parties and will not be liable for any lack of the foregoing or for
      any errors or omissions in any content supplied by third parties. In
      addition, third parties may offer opportunities, goods, services and
      other materials to you on the Websites. Such dealings are solely between
      you and the third party. Ajizzy will not be responsible for any loss or
      damage of any sort incurred as the result of any such dealings. Ajizzy
      makes no warranty concerning, is not responsible for and does not endorse
      any third party provided opportunities, goods, services or other
      materials, and you agree that any recourse for dissatisfaction or
      problems with those opportunities, goods, services or other materials
      will be sought from the third party provider and not from Ajizzy.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Descriptions
      of, or references to opportunities, products, services, materials or
      publications within the Websites do not imply endorsement by Ajizzy of
      that opportunity, product, service, material or publication. The Websites
      may include links to other sites that are not maintained by Ajizzy.
      Websites of those third parties are subject to terms and conditions
      different from those found here and it is your responsibility to ensure
      that you have read and understood them. Ajizzy is not responsible for the
      content of those sites, does not endorse those sites, and makes no
      representations whatsoever concerning the content or accuracy of such
      other sites. If you decide to access any third party site linked to the
      Websites, you do so entirely at your own risk, and you may be exposed to
      offensive, indecent or objectionable content. Ajizzy shall have no
      liability for any loss or damage arising from your use of any such sites.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Limited
     License</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Subject to
      your compliance with these Terms of Use, Ajizzy grants you a limited,
      personal, non-exclusive, non-commercial, revocable, non-assignable and
      non-transferable license to:</span></li>
                                <ol style="margin-top:0cm" start="1" type="i">
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">View and
       listen to the Content contained on the Websites;</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">Create
       profile pages on the Websites for non-commercial and private use;</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">Participate
       in the Websites' community areas and communicate with other Users; and</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">Download the
       Content contained on the Websites onto a computer or other personal
       electronic device for your personal, non-commercial home or archival use
       only, provided you do not delete or change any copyright, trademark, or
       other proprietary notices contained therein.</span></li>
                                </ol>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">In addition
      to the foregoing, you may not use the Content for development of
      data-related products or services, the creation of any database product,
      or for data provision services. Unless separately and specifically
      licensed to do so in writing by Ajizzy, you agree not to re-transmit,
      disclose or distribute any Content to any other person, organization or
      entity. You expressly agree that the Services and Content shall be used
      solely for your own benefit and that the Content (including print
      publications, if applicable) shall not be redistributed or republished by
      you. You understand and agree that the features of the Services are
      subject to change without notice to you.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You may not
      take any action to jeopardize, limit or interfere with Ajizzy’s ownership
      of and rights with respect to the Websites or any Content. The use,
      copying, sale, leasing, renting, lending, distribution, modification,
      downloading, creating derivative works, posting or publication by you,
      directly or indirectly, of any Content, or any other use of such Content,
      except pursuant to the foregoing express limited grant of rights, is
      strictly prohibited. Bots, crawlers, spiders, data miners, scraping and
      any other automatic access tool are expressly prohibited. You acknowledge
      that any unauthorized copying or unauthorized use of the Websites or of
      any Content is a violation of these Terms of Use and is strictly
      prohibited. Violation of this limited use license may result in immediate
      termination of your access and use of the Websites and may result in
      legal action against you.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Restrictions
     on Use.</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The Websites
      is provided for lawful purposes only. By accessing or using the Websites,
      you agree and warrant that in connection with your use of the Websites you
      will not:</span></li>
                                <ol style="margin-top:0cm" start="1" type="i">
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">alter any
       trademark, copyright and other proprietary or legal notices contained in
       the Websites; or</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">manipulate
       the Websites in any way not intended and directed by Ajizzy; or</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">copy or seek
       to copy or "rip" any audio, video or audiovisual content from
       the Websites not legally available for download; or</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">exploit any
       part of the Websites for commercial gain or undertake any commercial
       activity utilizing the contents of the Websites without the prior
       written consent of Ajizzy, including, for example, inserting your own or
       a third party's advertising, branding or promotional content into the
       Websites’ contents, materials or services; or</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">upload,
       post, email, transmit, display, copy, distribute, promote, or otherwise
       communicate to the public:</span></li>
                                    <ol style="margin-top:0cm" start="1" type="1">
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">any
        material that is false, unlawful, threatening, tortious, disparaging,
        abusive, libelous, defamatory, obscene, vulgar, offensive,
        pornographic, profane, racist, sexually explicit, ethnically or culturally
        offensive, indecent, harassing, or that promotes violence, racial
        hatred, terrorism, or illegal acts, or anything that in Ajizzy's sole
        discretion is otherwise objectionable; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">information,
        software, content or other material that violates, plagiarizes,
        misappropriates or infringes the rights of third parties including,
        without limitation, copyright, trademark, patent, trade secret, rights
        of privacy or publicity, confidential information or any other
        proprietary right; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">material of
        any kind that restricts or inhibits any other user's uninhibited use
        and enjoyment of the Websites or interferes with, overburdens, impairs
        or disrupts the Websites, including material that contains a virus,
        Trojan horse, time bomb, worm, spyware, adware, malware, bot, any
        automated system, such as scripts, or any other harmful component; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">any
        unsolicited or unauthorized advertising, promotional materials, junk
        mail, spam, chain letters, pyramid schemes, requests for money,
        petitions for signature, or any other form of solicitation; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">use or
        attempt to use another person's information, account, password, service
        or system except as expressly permitted; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">impersonate
        another person or entity; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">engage in
        any conduct that in Ajizzy's sole discretion restricts the ability of
        any other person to enjoy the use of the Websites; or</span></li>
                                        <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
        background:white"><span lang="EN-GB" style="background:white">solicit or
        collect personal data including telephone numbers, addresses, last
        names, email addresses, or any other kind of information about users,
        except where expressly authorized by Ajizzy.</span></li>
                                    </ol>
                                </ol>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Copyright
     Notices and Celebrity Material Infringement.</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Copyright
      Notices/Complaints<br>
      Ajizzy takes claims of copyright infringement seriously.&nbsp; We will respond
      to notices of alleged copyright infringement that comply with applicable
      law.&nbsp; If you believe any materials accessible on or from the Websites
      infringe your copyright, you may request removal of those materials (or
      access to them) from the Websites by submitting written notification to
      us in accordance with the provisions of the Copyright Act of Nigeria.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Right of
      Publicity.<br>
      Please be aware that celebrities, and sometimes others, may have a
      "right of publicity," which means that they may have a right to
      control commercial uses of their name, image, likeness, and other aspects
      of their identity. Although you may be a fan, you risk infringing
      celebrity rights if you use a celebrity name or likeness on the Websites
      and you don't have the celebrity's permission. Ajizzy reserves the right
      to remove any Content contained in or posted to the Websites that Ajizzy
      determines in its sole discretion does or may allegedly infringe another
      person's copyright, trademark, celebrity material or other rights.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Notices to
      Ajizzy regarding any alleged infringement on the Websites should be
      directed to Ajizzy 's General Counsel's Office at</span><span lang="EN-GB" style=""> legal@Ajizzy.com</span><span lang="EN-GB" style="background:white">. </span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">User
     Accounts</span></b><span lang="EN-GB" style="background:white">.</span></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The creation
      of a User account may be required for the use of certain portions of the
      Websites (e.g., email, newsletters, competitions, forums, content
      downloads, and promotions). As part of Ajizzy’s registration process, you
      will select a username and password and be asked to submit, among other
      things, your email address. You agree that all information you provide to
      Ajizzy for purposes of creating a user account (the “User Information”)
      will be true, accurate, current and complete and your failure to provide
      such information will constitute a breach of these Terms of Use and may
      result in the immediate termination of your account. All information
      about you including your User Information, any information obtained by
      Ajizzy as a result of your use of the Websites, and any information
      stored or transmitted in any way on or through the use of the Websites is
      subject to Ajizzy's Privacy Policy, which can be found on Ajizzy.com.
      Ajizzy 's Privacy Policy is incorporated into these Terms of Use by this
      reference.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You
      understand that you may not: (i) select or use the name of another person
      with the intent to impersonate that person; (ii) use the rights of any
      person without authorization; or (iii) use a name that Ajizzy, in its
      sole discretion, deems inappropriate. You agree that any information that
      you provide to the Websites, including but not limited to User
      Information, shall be true and accurate and current, and you are
      responsible for updating such information to keep it true, accurate and
      current. If you create a user account with the Websites, you accept
      responsibility for all activities that occur under your account or
      password and you agree you will not sell, transfer or assign your user
      account. You are responsible for maintaining the confidentiality of your
      password, if any, and for restricting access to your computer so that
      others may not access any password-protected portion of the Websites
      using your name, username or password in whole or in part.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Each User
      name permits one person to access the password-protected portion of the
      Websites, and you shall not share the username and password with any
      third party. You shall be solely responsible for any and all use of the
      Websites, including without limitation, any and all charges incurred by a
      third party, under or using your user name and password. If at any time
      you should learn or suspect that your password has been compromised, you
      shall promptly notify our Customer Service at </span><span lang="EN-GB" style="">customer@ajizzy.com</span><span lang="EN-GB" style="background:white"> and confirm such notice in writing. Upon
      receiving such telephonic and written notice, Ajizzy will assign a new
      password to you without charge.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You agree
      that any unauthorized use of the Websites (or any Content derived
      therefrom) by you or by anyone using your username or password may result
      in immediate suspension or termination of your user account by Ajizzy, in
      its sole and absolute discretion, without refund of any pre-paid fees.
      You represent and warrant that the information provided during the
      subscription process is true and accurate. Ajizzy reserves the right to
      terminate, without notice, your access to and use of the Websites in the
      event that you provide any false information to Ajizzy as part of the
      subscription process, without refund of any pre-paid fees. Ajizzy
      reserves the right to terminate, without notice, your subscription and
      access to the Websites for any conduct that Ajizzy, in its sole
      discretion, believes is in violation of these Terms of Use, any
      applicable law, or is harmful to the interests of another User, service
      provider, or Ajizzy. In the event of such termination, you will continue
      to be liable for applicable fees for the period prior to termination,
      together with such other remedies as to which Ajizzy may be entitled.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Subscription
     Terms, Fees and Payments; Automatic Renewal and Cancellation</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Ajizzy offers
      </span><span lang="EN-GB" style="">annually, quarterly, or
      monthly</span><span lang="EN-GB" style="background:white"> subscription
      services. The length of the subscription period is selected by you during
      the enrollment process. </span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">All
      Subscriptions are Automatically Renewing.</span></b><span lang="EN-GB" style="background:white"><br>
      This means that once you become a subscriber, your subscription will be
      automatically renewed and your credit or debit card will be charged based
      on the subscription program </span><span lang="EN-GB" style="background:
      white">(annually, quarterly, or monthly) </span><span lang="EN-GB" style="background:white">you have chosen unless you opt out or cancel by
      following the instructions in these Terms of Use. By subscribing to the
      Paid Services, you authorize Ajizzy to charge the applicable recurring
      subscription fees to your credit or debit card. In some cases, when you
      subscribe to a free trial of the Paid Services, you will be asked to
      authorize, and by accepting these Terms of Use you hereby expressly
      authorize, Ajizzy to charge the applicable recurring subscription fees to
      your credit or debit card upon the expiration of the free trial.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">When you
      initially subscribe to the Paid Services, your credit or debit card will
      be charged immediately for the initial term of the subscription at the
      then-current fee for the applicable subscription period. When you
      initially subscribe to a free trial of the Paid Services, you will not be
      charged for the initial term of the free trial. Unless you notify Ajizzy
      of your decision to terminate your subscription or, as applicable, your
      free trial, your subscription will automatically renew at the end of each
      subscription term or free trial (if applicable) at the then-current fee.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You will be
      charged in advance for the renewal term of the subscription service (or
      initial term, in the case of renewal following the expiration of an
      applicable free trial) on your applicable billing date (the “Billing
      Date”), which will be your the corresponding </span><span lang="EN-GB" style="">monthly, quarterly or annual </span><span lang="EN-GB" style="background:white">anniversary of the activation date of
      subscription. If, however, the activation date of your subscription (or,
      as applicable, your free trial) is on the 29th, 30th or 31st day of a
      month, and your Billing Date does not exist in a particular month because
      there are fewer than thirty-one (31) days in the month payment is due,
      your Billing Date for that particular renewal (or initial term, in the
      case of renewal following the expiration of an applicable free trial)
      will be on the last day of the month in which your Billing Date would
      otherwise be. Each charge on the applicable Billing Date applies to the
      subscription period immediately following the Billing Date (e.g., a
      charge for a monthly subscription applies to the month immediately
      following the Billing Date). You understand that any applicable sales or
      use taxes will be payable by you, and may be included in each periodic
      statement.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Ajizzy may,
      in its sole discretion, suspend access to your account or deactivate your
      account without notice to you if Ajizzy is unable to process your payment
      or if payment is past due, regardless of the amount. You agree to pay any
      outstanding balance in full within thirty (30) days of cancellation or
      termination of your subscription. You may update any of your billing
      information (including a change to your desired billing payment method)
      through your account settings on Ajizzy.com.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">by completing
      the enrollment process, you understand and agree that unless and until
      you cancel your subscription to the paid services:</span></li>
                                <ol style="margin-top:0cm" start="1" type="i">
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">your
       subscription to the paid services will automatically renew at the end of
       the subscription period and continue for another subscription period;</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">you will
       automatically be billed at the start of each new subscription period; and</span></li>
                                    <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
       background:white"><span lang="EN-GB" style="background:white">that
       periodic charges for each new subscription period will be automatically
       billed to your designated billing payment method.</span></li>
                                </ol>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">Opting Out
      Of Renewal.<br>
      </span></b><span lang="EN-GB" style="background:white">You may terminate
      enrollment in automatic renewal at any time by contacting Ajizzy online.
      Your subscription to the paid services shall be deemed terminated on the
      last day of the then in effect subscription period, unless provided
      within ten business days of an auto renewal. you agree that except as
      provided herein, the subscription fee is non-refundable once paid.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">Unauthorized
      Charges.<br>
      </span></b><span lang="EN-GB" style="background:white">You acknowledge and
      agree that you, and not Ajizzy, is responsible for paying any amounts
      billed to your credit or debit card by a third party which were not
      authorized by you.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">Billing
      Discrepancies.</span></b><span lang="EN-GB" style="background:white"> <br>
      Unless you notify Ajizzy of any discrepancies within thirty (30) days
      after they first appear on your payment statement, they will be deemed
      accepted by you and you release Ajizzy from all liabilities and claims of
      loss resulting from any such error or discrepancy.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">Fees
      Subject to Change.</span></b><span lang="EN-GB" style="background:white"> <br>
      Ajizzy may change the fees and charges then in effect by giving you
      notice in advance or by posting the changed fees online at Ajizzy.com. It
      is your responsibility to check the current fees. All fees and charges
      incurred in connection with your username(s) and password(s) will be
      billed to the credit or debit card you designate during the registration
      process. If you want to designate a different credit or debit card or
      there is a change in your credit or debit card validity or expiration date,
      you must call Customer Service or update your information through your
      account settings on Ajizzy.com.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><b><span lang="EN-GB" style="background:white">Cancellations
      and Refunds.</span></b><span lang="EN-GB" style="background:white"> <br>
      <a></a><a></a><a>You may cancel a free trial at any time during the free
      trial period and incur no charge. After the free trial is complete, you
      will be charged at the rate and schedule selected when you enrolled in
      the trial. Subscriptions are not eligible for refund, but can be
      cancelled to prevent additional future charges. </a></span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_8" onmouseover="msoCommentShow('_anchor_8','_com_8')" onmouseout="msoCommentHide('_com_8')" href="#_msocom_8" language="JavaScript" name="_msoanchor_8">[8]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_9" onmouseover="msoCommentShow('_anchor_9','_com_9')" onmouseout="msoCommentHide('_com_9')" href="#_msocom_9" language="JavaScript" name="_msoanchor_9">[9]</a>&nbsp;</span><span lang="EN-GB" style="color:black"><a class="msocomanchor" id="_anchor_10" onmouseover="msoCommentShow('_anchor_10','_com_10')" onmouseout="msoCommentHide('_com_10')" href="#_msocom_10" language="JavaScript" name="_msoanchor_10">[10]</a>&nbsp;</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Additional
     Terms.</span></b><span lang="EN-GB" style="background:white"><br>
     In some instances, additional terms or end user license agreements apply
     to some services, products, software, competitions or promotions offered
     via the Websites. Such terms are posted in connection with the applicable
     service, product, software, competition or promotion and are in addition to
     these Terms of Use. In the event of a conflict, the additional terms
     prevail over these Terms of Use. Ajizzy may change, suspend or discontinue
     any aspect of the Websites at any time and without prior notice.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Entire
     Agreement<br>
     </span></b><span lang="EN-GB" style="background:white">You acknowledge and
     agree that these Terms of Use, the Privacy Policy, any applicable end user
     license or similar agreements contain the entire agreement between the
     parties relating to the Websites; provided, however these Terms of Use do
     not alter in any way the terms or conditions of any other agreement User
     may have with Ajizzy. If any provision in these Terms of Use is invalid or
     unenforceable under applicable law, that part will be deemed severable and
     the remaining provisions will continue in full force and effect. Each
     Affiliate expressly made a third party beneficiary of this Agreement and
     may enforce this Agreement directly against you. This Agreement will be
     binding on and will inure to the benefit of the legal representatives,
     successors and assigns of the parties hereto.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Equipment
     and Internet Access<br>
     </span></b><span lang="EN-GB" style="background:white">You understand and
     agree that you are responsible for providing all telephone, modem,
     Internet connection, intranet connection, extranet connection and other
     equipment necessary for you to access the Websites (including the Paid
     Services). You are solely responsible for and shall bear the costs of any
     such equipment and any fees or charges incurred to access the Websites
     (including the Paid Services) through an Internet access provider or other
     third-party service, including any applicable taxes.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">No Oral
     Agreements or Modifications<br>
     </span></b><span lang="EN-GB" style="background:white">There shall be no
     amendment or modification of these Terms of Use unless the same is in
     writing. Oral agreements shall expressly be non-binding and unenforceable.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Dispute
     Resolution<br>
     </span></b><span lang="EN-GB" style="background:white">This Agreement shall
     be governed by, construed, and enforced in accordance with the laws of the
     Federal Republic of Nigeria.</span><span lang="EN-GB" style="color:black;
     background:white">In the event of any dispute relating to the
     interpretation, operation, default or breach of this Agreement, Parties
     shall endeavor to resolve said dispute by negotiation and/or self
     conducted mediation within 30 days of notification of a dispute by the
     aggrieved party. If negotiation or self conducted mediation fails to
     settle the dispute within 30 days of notification, the dispute shall be
     referred to and finally settled by binding Arbitration under the Rules of
     the Arbitration and Conciliation Act of Nigeria A18 2004, LFN.</span></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="color:black;background:white">The
      arbitration shall be held in Lagos, Nigeria and shall be conducted in
      English Language by a single Arbitrator appointed upon request by either
      Party to the President of the Chartered Institute of Arbitrators, UK,
      Nigeria Branch.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="color:black;background:white">The
      Arbitration Award shall be final and Parties shall hold all Arbitration
      proceedings in strict confidence.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="color:black;background:white">No
      other dispute resolution mechanism or process is permitted under this
      Agreement.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Recovery of
     Fees<br>
     </span></b><span lang="EN-GB" style="background:white">You agree that if
     Ajizzy takes action (by itself or through its representatives) to enforce
     any of the provisions of these Terms of Use against you, including
     collection of any amounts due to Ajizzy, Ajizzy shall be entitled to
     recover from you (and you agree to pay), Ajizzy’s reasonable and necessary
     attorney’s fees and any costs of any litigation, in addition to all sums
     to which Ajizzy is entitled or any other relief, at law or in equity,
     except as otherwise limited herein.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Limitation
     of Liability</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">Incidental
      Damages and Aggregate Liability.<br>
      In no event will Ajizzy or any of ITS AFFILIATES be liable for any
      indirect, special, incidental, or consequential damages, losses or
      expenses arising out of or relating to the use or inability to use the
      Websites, including without limitation damages related to any information
      received from the Websites, removal of content from the Websites,
      including profile information, any email distributed to any User or any
      linked web site or use thereof or inability to use by any party, or in
      connection with any termination of your subscription or ability to access
      the Websites, failure of performance, error, omission, interruption,
      defect, delay in operation or transmission, computer virus or line or
      system failure, even if Ajizzy, or representatives thereof, are advised
      of the possibility of such damages, losses or expenses. Under no
      circumstances will ajizzy’s aggregate liability, in any form of action whatsoever
      in connection with this agreement or the use of the websites, exceed the
      price paid by you for your account in the 12 months immediately preceding
      the event giving rise to such claim.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">No Liability
      for non-Ajizzy Actions.<br>
      To the maximum extent permitted by applicable law, in no event will
      ajizzy be liable for any damages whatsoever, whether direct, indirect,
      general, special, compensatory, and/or consequential, arising out of or
      relating to the conduct of you or anyone else in connection with the use
      of the site or the services, including without limitation, bodily injury,
      emotional distress, and/or any other damages resulting from anyone’s
      reliance on information or other content posted on the site, or
      transmitted to or by any users or any other interactions with other
      registered users of the site or services, whether online or offline. this
      includes any claims, losses or damages arising from the conduct of users
      who have registered under false pretenses or who attempt to defraud or
      harm you.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">In addition
      to the preceding paragraphs of this section and other provisions of these
      Terms of Use, any advice that may be posted on the Websites is for
      informational purposes only and is not intended to replace or substitute
      for any professional financial, medical, legal, or other advice. Ajizzy
      makes no representations or warranties and expressly disclaims any and
      all liability concerning any treatment, action, or effect on any person
      following the information offered or provided within or through the
      Websites. If you have specific concerns or a situation arises in which
      you require professional advice, you should consult with an appropriately
      trained and qualified specialist.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Indemnity.</span></b><span lang="EN-GB" style="background:white"><br>
     You agree to and shall defend, indemnify and hold harmless Ajizzy, its
     affiliates and its and its affiliates’ directors, officers, employees,
     agents, representatives, members, joint venturers, licensors, suppliers,
     vendors, distributors, advertisers and other contracting parties
     (hereinafter “Affiliates”) from and against any and all claims, losses,
     expenses, including reasonable legal fees or demands of liability,
     including reasonable attorneys’ fees and costs incurred by Ajizzy and its
     affiliates in connection with any claim by a third party (including an
     intellectual property claim) arising out of (i) materials and content you
     submit, post or transmit through the Websites, or (ii) use of the Websites
     by you in violation of these Terms of Use or in violation of any
     applicable law. Users further agree that they will cooperate as reasonably
     required in the defense of such claims. Ajizzy and its Affiliates reserve
     the right, at their own expense, to assume the exclusive defense and
     control of any matter otherwise subject to indemnification by Users, and
     Users shall not, in any event, settle any claim or matter without the
     written consent of Ajizzy. Users further agree to hold harmless Ajizzy and
     its Affiliates from any claim arising from a third party’s use of
     information or materials of any kind that Users post to the Websites.</span></li>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Disclaimers
     of Warranty</span></b></li>
                            <ol style="margin-top:0cm" start="1" type="a">
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">The websites
      (including the paid services) and all content provided through the same
      are provided on an “as is” and “as available” basis without any warranty
      or condition, express or implied. to the maximum extent permissible under
      applicable law, Ajizzy, and each of its affiliates, specifically disclaim
      all implied warranties including, without limitation, implied warranties
      of title, quality, performance, merchantability, fitness for a particular
      purpose and non-infringement. further, no warranties will be implied by
      any course of dealing or course of performance.</span></li>
                                <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
      background:white"><span lang="EN-GB" style="background:white">You
      acknowledge that all information and services provided in connection with
      the websites are compiled from and distributed by sources that are often
      beyond the control of Ajizzy. Ajizzy makes no representation or warranty
      that (i) the websites will meet your expectations or requirements, (ii)
      the websites will be available, uninterrupted, timely, secure, accurate,
      complete or error-free, (iii) any results or information that may be
      obtained from the use of the websites will be accurate, timely, complete
      or reliable, (iv) any errors or defects in the websites will be
      corrected, or (v) the websites, networks or servers that make the
      websites available are free of viruses, clocks, timers, counters, worms,
      software locks, trojan horses, trap doors, time bombs or any other
      harmful codes, instructions, programs or components. operation of the
      websites may be interfered with by numerous factors outside of Ajizzy’s
      control including, but not limited to, telecommunications network
      disruptions. ajizzy is not responsible and will have no liability for any
      failures of the internet or any data or telecommunications equipment,
      system or network used in connection with the websites. you acknowledge
      that the entire risk arising out of the use, inability to use or
      performance of any of the websites remains with you to the maximum extent
      permissible under law. you acknowledge and agree that your sole remedy
      for any problems or dissatisfaction with the websites (including the paid
      services) is to terminate your account and discontinue use of the
      websites.</span></li>
                            </ol>
                            <li class="MsoNormal" style="color:#333333;margin-top:10.0pt;text-align:justify;
     background:white"><b><span lang="EN-GB" style="background:white">Electronic
     Communications</span></b><span lang="EN-GB" style="background:white"><br>
     You consent to receive communications from Ajizzy electronically. We will
     communicate with you by email or by posting notices on the Websites. You
     agree that all agreements, notices, disclosures and other communications
     that we provide to you electronically satisfy any legal requirement that
     such communications be in “writing.” Notice will be deemed given 24 hours
     after it is posted on any of the Websites or sent via email.
     Alternatively, Ajizzy may still decide to provide notice by certified mail
     to the address provided in a User’s payment information.</span></li>
                        </ol>



                        <p>
                            For more information send an email to <a href="mailto:talk@ajizzy.com?subject=Terms of Service">talk@ajizzy.com</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
