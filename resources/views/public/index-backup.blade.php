@extends('layouts.main')

@section('sidebar')
 {{$talent->stage_name}}  - {{$talent->title}}: {{$talent->gender}} @parent
@endsection


@section('content')
    <div class="mid-section" role="body">
        <div class="container basic-details">
            <div class="row">
                <div class=" col-12 col-md-auto order-md-12">
                    <div class="profile__top--actions">
                        <ul class="list-group list-group-horizontal talents-profile--action justify-content-lg-end">
                            <li class="list-group-item"><a class="save" title="Save Profile (creates tag)"><span></span></a></li>
                            <li class="list-group-item"><a class="message" title="Send Message"><span></span></a></li>
                            <li class="list-group-item"><a class="invite" title="Invite to Apply"><span></span></a></li>
                        </ul>
                        <div class="copy-url">
                            <div id="profileURL" class="copy-url--text"><a
                                    href="{{route('public.profile', $talent->profile_url)}}">{{route('public.profile', $talent->profile_url)}}</a></div>
                        </div>
                    </div>
                </div>
                <div class="col order-1 profile__top_thumb-wrapper">
                    <div class="profile__top--thumb">
                        <div class="photo" style="background: url('{{ $talent->medias[0]->url }}') center center / cover no-repeat;"></div>
                    </div>
                    <div class="profile__top--info font-md-mobile">
                        <h1 class="name">{{$talent->stage_name}}</h1>
                        <h2 class="title">{{$talent->title}}</h2>
                        <div class="location">{{$talent->location}}, {{$talent->user->country->name}}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.basic-details -->
        <div class="profile__tabs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="profile__tabs__container d-flex justify-content-center">
                            <ul class="nav nav-tabs" id="talentDetails" role="tablist">
                                <li class="nav-item">
                                    <a href="#" class="nav-link active show" id="details-tab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">Details</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link show" id="media-tab" data-toggle="tab" role="tab" aria-controls="media" aria-selected="true">Media</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main__container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane profile--main fade show active" id="profile-details" role="tabpanel" aria-labelledby="profile-details-tab">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4">
                                        <div class="profile--main__photos">
                                            @if($talent->medias->count())
                                            <div class="profile-sidebar headshot">
                                                <a href="#" class="headshot-link">
                                                    <img src="{{$talent->medias[0]->url}}" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="profile--main__media"></div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8">
                                        <div class="profile-section">
                                            <div class="profile-section__header">
                                                <h4>Appearance</h4>
                                            </div>
                                            <div class="profile-section__content appearance" id="profileAppearance">
                                                <ul>
                                                    <li>
                                                        <span class="attribute-label">Gender:</span>
                                                        <span class="attribute-value">{{ucfirst($talent->gender)}}</span>
                                                    </li>
                                                    <li>
                                                        <span class="attribute-label">Age Range:</span>
                                                        <span class="attribute-value">{{$talent->age_range}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        @if($talent->tags->count())
                                            <div class="profile-section">
                                                <div class="profile-section__header">
                                                    <h4>Skills</h4>
                                                </div>
                                                <div class="profile-section__content skills" id="profileskills">
                                                    <ul>
                                                        @foreach($talent->tags as $tag)
                                                        <li class="profile-skill">
                                                            <span class="skill-name">{{$tag->name}}</span>
                                                        </li>
                                                        @endforeach

                                                    </ul>
                                                </div>
                                            </div>

                                        @endif

                                        @if($talent->user->educations)
                                        <div class="profile--main__education">
                                            <div class="profile-section__header">
                                                <h4>Education &amp; Training</h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table align-items-center">
                                                    <thead class="thead-light">
                                                    <tr>
                                                        <th scope="col">School</th>
                                                        <th scope="col">Degree/Course</th>
                                                        <th scope="col">Location, Year Complete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                    @foreach($talent->user->educations as $edu)
                                                    <tr>
                                                        <th scope="row">{{$edu->school}}</th>
                                                        <td class="role">{{$edu->degree}}</td>
                                                        <td>{{$edu->instructor ? $edu->instructor . ', ' : ''}}{{$edu->location}}, {{$edu->year_completed}}</td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane profile--main fade" id="media-tab" role="tabpanel" aria-labelledby="media">
                                <h1>Media</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
