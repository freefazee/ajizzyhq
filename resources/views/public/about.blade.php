@extends('layouts.main')


@section('content')
    <section  class="section section-mini bg--grey why-join-how">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section--header text-center mb-0">
                        <h5 class="d-none d-md-block">Why Ajizzy</h5>
                        <h2 class="section--title">
                            About Ajizzy
                        </h2>
                        <p class="section--title-description mb-5">
                            Ajizzy stemmed as a result of a passion for helping the spotlight find the brightest talent for the entertainment industry.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tools" class="section">


        <div class="container" style="margin-top: 30px;">
            <div class="col-12">
                <div class="row align-items-center">
                    <div class="col-lg-7 col-md-6 col-xs-12 audience">
                        <div class="grey-back">
                            <div    style="padding: 70px; align-self: center;" class="bg--grey">

                                <h2>Read more about us</h2>

                                <p class="body-text-lg">
                                Ajizzy stemmed as a result of a passion for helping the spotlight find the brightest talent for the entertainment industry. We believe that every talented individual deserves to be noticed quickly, in the same vein, agents & creators should have the best tools to match talents for their casting and production needs.
                                </p>


                                <p class="body-text-lg">The coming together of experienced minds in creative media, advertising, and technology professionals with an innovative vision to remove the struggle and heartaches on the journey to success, gave birth to Ajizzy.
                                </p>

                                <p class="body-text-lg">
                                    Ajizzy's technology matches talent attributes against multiple auditions and job postings every day. Its unique algorithm sends casting email alerts and posts auditions and jobs directly on talents' profiles, saving members countless hours of tedious searching.
                                </p>

                                <p class="body-text-lg">

                                    From talents, agents, casting directors, to content creators, film production houses, we offer cutting edge technology to find or post casting calls across the globe from the comfort of your home or office.

                                </p>

                                <a href="{{route('join.plan-selection')}}" class="btn btn__custom btn-orange">JOIN US</a>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-xs-12 audience">
                        <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div>
                                        <img src="{{asset('images/about-us-icon.jpg')}}" alt="Filmmaker photo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
@endsection
