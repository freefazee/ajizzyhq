@extends('layouts.main')


@section('content')
    <section id="why-join-today" class="section section-mini bg--grey why-join-how">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section--header text-center mb-0">
                        <h5 class="d-none d-md-block">For Talents</h5>
                        <h2 class="section--title">
                            How Ajizzy works for Performers
                        </h2>
                        <p class="section--title-description mb-5">
                            As a subscribed member of Ajizy, you can be your own agent and gain access to thousands of daily opportunities worldwide. When you pay for our subscribed service, you will get direct access to all of our audition information, and you can submit yourself for free to Casting Directors and Agents. You can also avoid paying an Agent if you book the gig, putting more money in your pocket!
                        </p>
                        <p>Ajizzy  is reviewed on a daily basis by casting directors, modeling and talent agents, production companies, and advertising agencies. <strong style="font-weight: 700;">Don't miss out on this great opportunity to get discovered.</strong> </p>
                        <a href="{{route('talent.join-show')}}" class="btn btn__custom btn-orange">Get started now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tools" class="section">
        <div class="container">
            <div class="section--body">
                <div class="row">
                    <div class="col-md-12 tool-items">
                        <div class="tool-items--head full-width">
                            <ul class="nav nav-pills nav-fill d-none d-md-flex" id="tools-lists">
                                <li class="nav-item">
                                    <a href="#item-1" class="nav-link active" id="item-1-tab" data-toggle="tab" aria-controls="item-1" aria-selected="true">
                                        <i class="ni ni-bulb-61"></i>
                                        Casting Search
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#item-2" class="nav-link" id="item-2-tab" data-toggle="tab" aria-controls="item-2" aria-selected="false">
                                        <i class="ni ni-single-02"></i>
                                        Profile
                                    </a>
                                </li>
                                <li class="nav-item d-flex align-self-stretch">
                                    <a href="#item-3" class="nav-link" id="item-3-tab" data-toggle="tab" aria-controls="item-3" aria-selected="false">
                                        <i class="ni ni-email-83"></i>
                                        Messaging
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#item-4" class="nav-link" id="item-4-tab" data-toggle="tab" aria-controls="item-4" aria-selected="false">
                                        <i class="ni ni-briefcase-24"></i>
                                        Media Locker
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#item-5" class="nav-link" id="item-5-tab" data-toggle="tab" aria-controls="item-5" aria-selected="false">
                                        <i class="fas fa-handshake"></i>
                                        Applications
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#item-6" class="nav-link" id="item-6-tab" data-toggle="tab" aria-controls="item-6" aria-selected="false">
                                        <i class="ni ni-notification-70"></i>
                                        Safety
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tool-items--body tab-content">
                            <!-- <div class="tab-pane fade" id="item-1" aria-labelledby="item-1-tab"></div>
                            <div class="tab-pane fade" id="item-2" aria-labelledby="item-2-tab"></div>
                            <div class="tab-pane fade" id="item-3" aria-labelledby="item-3-tab"></div>
                            <div class="tab-pane fade" id="item-4" aria-labelledby="item-4-tab"></div>
                            <div class="tab-pane fade" id="item-5" aria-labelledby="item-5-tab">Sample tab content 5</div> -->
                            <div class="tab-pane fade show active" id="item-1" aria-labelledby="item-6-tab">
                                <div class="row">
                                    <div class="col-md-5 d-none d-md-block">
                                        <img src="{{asset('assets/images/mosaic-ladies.png')}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-7">
                                        <h3>
                                            Apply to Ajizzy and <span class="d-md-block">audition with confidence</span>
                                            <small class="">Safety is our top priority so that giving a great performance is yours.</small>
                                        </h3>
                                        <div class="row">
                                            <div class="col-md-6 mb-5">
                                                <h4>How to find Job</h4>
                                                <p>You no longer have to hunt auditions on social media or get bombarded with fake audions. Content creators all over the world are using Ajizzy as their only platform to hire new talents.</p>
                                            </div>
                                            <div class="col-md-6">
                                                <h4>Get invites from directors</h4>
                                                <p>Creators can invite you to to apply for roles based on your profile visibility. You stand very high chances of landing your next role with us by properly building your professional profile and let industry professionals find you.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="item-2" aria-labelledby="item-2-tab">
                                <div class="row">
                                    <div class="col-mdTalentController-5 d-none d-md-block">
                                        <img src="{{asset('assets/images/mosaic-ladies.png')}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-7">
                                        <h3>
                                            Build a professional profile
                                            <small class="d-md-block">Let industry professionals find you with ease.</small>
                                        </h3>
                                        <div class="row">
                                            <div class="col-md-6 mb-5">
                                                <h4>Exposure</h4>
                                                <p>Create your <a href="{{route('account.profile')}}">profile</a> and get listed in our <a href="{{route('talents')}}">professional database</a>, used by the industry's leading professionals to connect with talent.</p>
                                            </div>
                                            <div class="col-md-6">
                                                <h4>Simple Profile creation</h4>
                                                <p>Showcase your headshots, resume, reel, other photos, social media profiles and more, all in one profile.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
