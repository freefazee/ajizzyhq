@extends('layouts.main')


@section('content')
    <section  class="section section-mini bg--grey why-join-how">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section--header text-center mb-0">
                        <h5 class="d-none d-md-block">Partnership Information</h5>
                        <h2 class="section--title">
                            Partner with Ajizzy
                        </h2>
                        <p class="section--title-description mb-5">
                            Our goal is simple: To let industry professionals find the best talents on the continent. As simple as it might sound, the success of our mission, we believe, depends on how much of collaborative efforts we can leverage.
                        </p>
                        <a href="mailto:talk@ajizzy.com?subject=Partnership" class="btn btn__custom btn-orange"><i class="fas fa-envelope"></i> SEND US AN EMAIL</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">



        <div class="container" style="margin-top: 30px;">
            <div class="col-12">
                <div class="row align-items-center">
                    <div class="col-lg-7 col-md-6 col-xs-12 audience">
                        <div class="grey-back">
                            <div    style="padding: 70px; align-self: center;" class="bg--grey">
                                <h3>Why Partner With Us?</h3>
                                <p class="body-text-lg">
                                <h4 class="font-weight-700">As a partner</h4>
                               We provide you with access to tremendous talents, and all we ask is you cast through us exclusively. What that means is, Ajizzy becomes the only platform where you post your casting calls online. We can help you generate short URLs to share on social media to enable talents link to it and apply.
                                </p>

                                <h4 class="font-weight-700">Benefits</h4>
                                <p>The benefit of this partnership is, you have a single place to assemble all your applications, download contact details, and start casting your project.
                                    <ul>
                                        <li> Access to best talents.</li>
                                        <li>Access to application management tools.</li>
                                        <li> Access to communication tools (emailing for now).</li>
                                     </ul>
                                </p>

                                <h4 class="font-weight-700">What you give to us</h4>

                                   <ul>
                                    <li>Post your casting calls exclusively on Ajizzy</li>
                                    <li>Talk about Ajizzy to friends and family.</li>
                                </ul>

                                <p class="text-center" style="margin-top: 40px;"> <a href="{{route('creator.projects.new')}}" class="btn btn__custom btn-orange">POST A JOB</a></p>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-xs-12 audience">
                        <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div>
                                        <img src="{{asset('images/partner1.jpg')}}" alt="Partnership">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
@endsection
