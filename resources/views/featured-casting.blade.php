<div class="featured-cast-listings">
    <h4 class="text-orange">Featured castings</h4>
    <div class="row">
        @foreach($featuredCastings as $featuredCasting)
        <div class="col-md-3 mb-3 mb-md-0">
            <div class="column-wrapper">
                <h6 class="h6--small-header category-title">
                    <a href="#">{{$featuredCasting->category->name}}</a>
                </h6>
                <h3 class="h3--smaller-header post-title">
                    <a href="{{route('single.casting-call', $featuredCasting->id)}}">{{str_limit($featuredCasting->title,23)}}</a>
                </h3>
                <div class="meta">
                    <span class="location">{{str_limit($featuredCasting->loc, 20)}}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
