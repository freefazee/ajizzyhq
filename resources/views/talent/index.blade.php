@extends('layouts.main')

@section('title') Find The Best African Talents: Models, Actors, Creative experts | Ajizzy @endsection


@section('content')

    <div class="section-header pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="section-header--title border-0 pb-0">Find the best talents for your project.</h1>
                    <h4>Search our talent database for your production needs. You can find actors, models, crews and many more.</h4>
                </div>
            </div>

        </div>
    </div>

    <div class="talents--wrapper">
        <div class="container" id="talents">
            <talent-list endpoint="/talents/feeds">
                <p>Loading.. <i class="fas fa-spinner fa-spin"></i></p>
            </talent-list>
      </div>
    </div>

@endsection


@section('footer_script')
    <script src="{{asset('assets/ajizzy/js/talents/index.js')}}"></script>
@endsection
