<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.header')

<body class="post post-single-cast">
@include('partials.navs')

@yield('content')

@include('partials.footer')
</body>
</html>
