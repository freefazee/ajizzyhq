<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.header')

<body class="{{Route::currentRouteName() === 'home' ? 'page-home': ''}}">
@include('partials.navs')
@include('partials._flash-messages')

@yield('content')

@include('partials.footer')
</body>
</html>
