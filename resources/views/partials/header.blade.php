<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
     @include('partials.main_meta')
    <!-- Icons -->
    <link href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/dist/css/select2.min.css')}}" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/favicon/site.webmanifest')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <!-- Ajizzy CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/ajizzy.css')}}" />

    <style>

        .profile-section__content ul{
            margin: 0;
            padding: 0;
        }
        .profile-skill{
            list-style: none;
            margin-right: 10px;
            margin-bottom: 10px;
            padding: 5px 15px;
            float: left;
            background-color: #1d2124;
            color: #FFF;
        }
        .profile-skill a{
            color: #FFF;
        }
        .profile-skill a:hover{text-decoration: underline;}
        .profile-skill:last-child{
            margin-right: 0;
        }

        .profile--main__education{
            clear: both;
            margin-top: 30px;
        }
    </style>

    @yield('css')
</head>
