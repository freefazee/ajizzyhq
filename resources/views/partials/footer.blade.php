<footer id="footer" role="footer">
    <div class="container">
        <div class="row footer-primary">
            <div class="col-md-8 footer-primary-menu">
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="footer-primary-menu-header">Performers</h6>
                        <ul class="footer-primary-menu-list list-unstyled">
                            <li class="menu-item">
                                <a href="{{route('casting-calls')}}">Casting calls</a>
                            </li>
                            <li class="menu-item"><a href="{{route('performer.how-it-works')}}">How it Works</a></li>
                            <li class="menu-item">
                                <a href="{{route('account.profile')}}">Create Talent Profile</a>
                            </li>
                            <li class="menu-item"><a href="{{route('talent.join-show')}}">Join</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h6 class="footer-primary-menu-header">Creators</h6>
                        <ul class="footer-primary-menu-list list-unstyled">
                            <li class="menu-item"><a href="{{route('talents')}}">Talent database</a></li>
                            <li class="menu-item"><a href="{{route('creator.how-it-works')}}">How it Works</a></li>
                            <li class="menu-item">
                                <a href="{{route('creator.projects.new')}}">Post a Job</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h6 class="footer-primary-menu-header">Support</h6>
                        <ul class="footer-primary-menu-list list-unstyled">
                            <li class="menu-item"><a href="#">Help</a></li>
                            <li class="menu-item"><a href="#">Contact</a></li>
                            <li class="menu-item"><a href="{{route('talent.join-show')}}">Pricing</a></li>
                            <li class="menu-item"><a href="#">Advertising</a></li>
                            <li class="menu-item"><a href="{{route('terms-of-services')}}" title="Terms of Use">Terms of Use</a></li>
                            <li class="menu-item"><a href="#" title="Privacy policy">Privacy policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END /footer-primary-menu -->

            <div class="col-md-4 footer-primary-newsletter">
                <!-- Begin Mailchimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                <div id="mc_embed_signup">
                    <form action="https://gmail.us3.list-manage.com/subscribe/post?u=3bdd10916c74d326f9847b63c&amp;id=543a39fb8d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <h2> Subscribe to our newsletter</h2>
                            <div class="mc-field-group">
                                <label for="mce-EMAIL">Email Address </label>
                                <input type="email" placeholder="Email address" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3bdd10916c74d326f9847b63c_543a39fb8d" tabindex="-1" value=""></div>
                            <div class="clear">
                                <input type="submit" style=" margin-left: 0; border: none; border-radius: 0;" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-orange btn__custom"></div>
                        </div>
                    </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->
            </div>
            <!-- END /footer-primary-newsletter -->
        </div>
        <!-- END /footer-primary -->

        <div class="row footer-secondary">
            <div class="col-md-12">
                <p>
                    &copy; 2019 by Ajizzy.com / <a href="{{route('terms-of-services')}}">Terms of service</a>
                </p>
            </div>
        </div>
        <!-- END /footer-secondary -->
    </div>
</footer>

<script type="text/javascript" src="{{asset('assets/js/ajizzy.js')}}"></script>
<script src="{{asset('js/notiflix-aio-1.5.0.min.js')}}"></script>
<script>
    window.AjConfig = {
        countries: JSON.parse('{!! $countries!!}'),
        GOOGLE_PLACE_API_KEY: '{{env('GOOGLE_PLACE_API_KEY')}}',
        LOCATION_INFO: JSON.parse('{!! session('location') !!}'),
        TIMEZONE: 'Africa/Lagos',
        RAVE_PUB_KEY: '{{env('RAVE_PUBLIC_API_KEY')}}',
        LOGGED_IN: '{{auth()->user() ? 0 : 1}}',
        USER_ID: '{{auth()->id()}}',
        MIN_AGE: '{{config('ajizzy.min_age')}}',
        BASE_URL: '{{URL::to('/')}}/'
    };

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>

<script type="text/javascript" src="{{asset('assets/vendor/select2/dist/js/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" type="text/javascript"></script>

<script src="{{asset('assets/vendor/nouislider/distribute/nouislider.min.js')}}"></script>

@yield('footer_script')
