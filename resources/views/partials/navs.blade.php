<header id="app-header">
    @if(!auth()->user())
    <nav class="top-navbar navbar logged-out">
        <ul class="top-navbar__links">
            <li><a class="header-quick-item" href="{{route('login')}}">Sign in</a></li>
            <li><a href="{{route('talent.join-show')}}" class="ga-join-link-top actor-cta header-quick-item">Join</a></li>
            <li><a href="{{route('creator.projects.new')}}" class="header-quick-item">Post a Job</a></li>
            <li><a href="#">Member Benefits</a></li>
        </ul>
    </nav>
    @else
    <nav class="top-navbar navbar logged-out">
        <ul class="top-navbar__links">

            @if(!auth()->user()->hasSubscription())
                <li><a href="{{route('join.plan-selection')}}" class="text-white font-weight-bold">Upgrade To Premium</a></li>
                @else
                <li><a href="" class="text-gray-dark"><i class="fas fa-check"></i> Premium</a></li>
            @endif

            <li class="dashboard" data-toggle="tooltip" title="Upload Media">
                <a href="{{route('account.dashboard')}}">
                    <i class="fas fa-upload"></i> Upload
                </a>
            </li>

            <li class="dashboard" data-toggle="tooltip" title="Dashboard">
                <a href="{{route('account.dashboard')}}">
                    <i class="fas fa-tachometer-alt"></i> Dashboard
                </a>
            </li>

            <li class="dashboard" data-toggle="tooltip" title="Manage Projects">
                <a href="{{route('casting.projects.index')}}">
                    <i class="fas fa-film"></i> Projects
                </a>
            </li>
            <li class="dropdown account">
                <a id="dropdownAccount" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="top-navbar__links--thumb">
                            <img src="{{auth()->user()->getAvatarUrl()}}">
                        </span>
                    <span class="top-navbar__links--text">My Account</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAccount">
                    <a href="{{route('account.profile')}}" class="dropdown-item font-md-mobile"><i class="ni ni-single-02"></i><span>Edit Profile</span></a>
                    <a href="{{route('account.profile.avatar')}}" class="dropdown-item font-md-mobile"><i class="ni ni-image"></i><span>Profile Photos</span></a>
                    <a href="{{route('account.profile.settings')}}" class="dropdown-item font-md-mobile"><i class="ni ni-settings-gear-65"></i><span>Account Settings</span></a>
                    <a href="{{route('creator.projects.new')}}" class="dropdown-item font-md-mobile"><i class="ni ni-spaceship"></i><span>Post a Job</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('public.profile', auth()->id())}}" class="dropdown-item font-md-mobile"> <i class="ni ni-user-run"></i><span>Public Profile</span></a>
                    <div class="dropdown-divider"></div>

                    <a href="{{route('account.logout')}}" class="dropdown-item font-md-mobile"> <i class="ni ni-user-run"></i><span>Log Out</span></a>
                </div>
            </li>
        </ul>
    </nav>
    @endif

    <nav class="main-navbar nav-bar navbar navbar-expand-lg sticky-shrinknav-menu" role="navigation">
        <div class="main-navbar__container">
            <div class="navbar-header">
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon icon">
                            <span class="icon"></span>
                        </span>
                </button>
                <a href="{{route('casting-calls')}}" class="find-jobs-btn ga-cc-link-mobile"><span class="text">Find Jobs</span></a>
                <a class="app-logo" href="/">
                    <img src="{{asset('assets/images/logo.svg')}}">
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbarSupportedContent" style="">
                <ul class="nav navbar-nav main-navbar--left">
                    <li>
                        <a href="{{route('casting-calls')}}" class="ga-cc-link-main header-menu-item">Casting Calls</a>
                    </li>
                    <li>
                        <a href="{{route('talents')}}" class="header-menu-item">Find Talent</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" id="creators-dropdown" href="#" role="button" data-target="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Creators</a>
                        <div class="dropdown-menu creators" role="menu" aria-labelledby="creators-dropdown">
                            <a href="{{route('talents')}}" class="dropdown-item font-md-mobile header-menu-item">
                                <div class="text">Talent Database</div>
                            </a>
                            <a href="{{route('creator.how-it-works')}}" class="dropdown-item font-md-mobile">
                                <div class="text">How it works</div>
                            </a>

                            <a href="{{route('creator.projects.new')}}" data-cta="product_home_nav_paj" class="dropdown-item font-md-mobile header-menu-item cta cd-cta">
                                <div class="btn-blue-sm">Post a Job</div>
                            </a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" id="performers-dropdown" href="#" role="button" data-target="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Performers</a>
                        <div class="dropdown-menu performers" role="menu" aria-labelledby="performers-dropdown">
                            <a href="{{route('casting-calls')}}" class="dropdown-item font-md-mobile ga-cc-link-performers header-menu-item">
                                <div class="text">Casting Calls</div>
                            </a>
                            <a href="{{route('performer.how-it-works')}}" class="dropdown-item font-md-mobile header-menu-item">
                                <div class="text">How it works</div>
                            </a>

                            <a href="{{route('talent.join-show')}}" class="dropdown-item font-md-mobile cta actor-cta header-menu-item">
                                <div class="btn-blue-sm">Join</div>
                            </a>
                        </div>
                    </li>
                    <li><a href="{{route('partnership')}}" class="header-menu-item">Partners</a></li>
                    <li><a href="{{route('about')}}" class="header-menu-item">About Us</a></li>
                </ul>
                <div classname="main-navbar--right"></div>
            </div>
        </div>
    </nav>
</header>
