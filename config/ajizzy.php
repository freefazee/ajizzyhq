<?php

return [

    'application-media-disk' => env('APPLICATION_MEDIA_DISK', 'public'),
    'profile-media-disk' =>  env('PROFILE_MEDIA_DISK', 'public'),

    'min_age' => env('MIN_AGE_ALLOWED_TO_JOIN', 18)
];
